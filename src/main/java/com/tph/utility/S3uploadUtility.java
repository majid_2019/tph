package com.tph.utility;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.tph.constants.ConstantKeys;

public class S3uploadUtility {
	private static String baseUrl = "https://s3.ap-south-1.amazonaws.com/";
	private String bucketName = ConstantKeys.FILE_UPLOAD_BUCKET;

	public String uploadToS3(InputStream inputStream, String folderName, String fileName, long size) {

		String finalFileName = null;
		try {

			AWSCredentials credentials = new BasicAWSCredentials("AKIAJTIH35WUOZY57F5Q",
					"LTua6im6t8wng4Vo4bUbtHID/EKc7313hmKeo5os");
			

			AmazonS3 s3client = new AmazonS3Client(credentials);
			//AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion("ap-south-1").withCredentials(new AWSStaticCredentialsProvider(credentials)).build();
	

			createFolder(bucketName, folderName, s3client);
			ObjectMetadata omd = new ObjectMetadata();
			//omd.setContentLength(size);

			
			finalFileName = folderName + "/" + fileName+"."+"pdf";
			

			// For Kyc Doc Content Type..
			String contentType = "pdf";
			
			omd.setContentType(contentType);
			
			PutObjectRequest putObject = new PutObjectRequest(bucketName, finalFileName, inputStream, omd)
					.withCannedAcl(CannedAccessControlList.PublicRead);

			s3client.putObject(putObject);

			//finalFileName = baseUrl + bucketName.concat("/" + finalFileName);


		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("finalFileName -- "+finalFileName);
		return finalFileName;
	}

	public static void createFolder(String bucketName, String folderName, AmazonS3 client) {
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName + "/", emptyContent, metadata);
		client.putObject(putObjectRequest);
	}

    
	/**
	 * @param file
	 * @return
	 */
	public static String getFileName(String file) {
		String fileName = null;
		fileName = file.substring(file.lastIndexOf(".") - 1);
		System.out.println("File name -- " + fileName);
		return fileName;
	}
	
	public static String getExtension(String fileName) {
		String kycExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
		return kycExtension;
	}
}
