/**
 * 
 */
package com.tph.utility;

import java.sql.Connection;
import java.sql.DriverManager;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

/**
 * @author majidkhan
 *
 */
public class DBConnectionUtility {

		
	/*  private static String QA_URL = "jdbc:mysql://tph.cwzzm606rl2y.ap-south-1.rds.amazonaws.com:7095/tph_db";
	  private static String QA_PW = "Majidkhan#$54";
	  
	  private static String PROD_URL = "jdbc:mysql://prod-tph.cwzzm606rl2y.ap-south-1.rds.amazonaws.com:7095/tph_db";
	  private static String PROD_PW = "Majidkhan#$54";*/
	 

	/**
	 * QA
	 */
	/*private static String userName = "majid";
	private static String password = "Majidkhan#$54";
	private static String url = "jdbc:mysql://tph.cwzzm606rl2y.ap-south-1.rds.amazonaws.com:7095/tph_db"; // QA
	private static String driver = "com.mysql.cj.jdbc.Driver";*/
	
	/**
	 * Prod
	 */
	private static String userName = "majid";
	private static String password = "Majidkhan#$54";
	private static String url = "jdbc:mysql://prod-tph.cwzzm606rl2y.ap-south-1.rds.amazonaws.com:7095/tph_db"; // Prod
	private static String driver = "com.mysql.cj.jdbc.Driver";
	
	 /* private static String userName = System.getenv("sqldb_userName");
	  private static String password = System.getenv("sqldb_password"); 
	  private static String url = System.getenv("sqldb_url"); 
	  private final static String driver = "com.mysql.cj.jdbc.Driver";*/
	 

	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, userName, password);
		} catch (Exception e) {
			System.out.println("DBConnectionUtility Exception Message:::" + e.getMessage());
		}

		return conn;
	}
}
