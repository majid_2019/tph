/**
 * 
 */
package com.tph.sms.utilty;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import com.tph.response.model.SmsResponseModel;

/**
 * @author majidkhan
 *
 */
public class ExternalService {

	private static String authkey = "225929Arsvdgj8MP5b4b6654";
	private static String senderId = "TPHIND";
	private static String route = "4";

	/**
	 * 
	 * @param smsBody
	 * @param mobileNumber
	 * @param responseModel 
	 * @return
	 */
	
	public SmsResponseModel sendSms(String smsBody, Long mobileNumber, SmsResponseModel smsResponseModel) {
		
		String mainUrl = "http://control.bestsms.co.in/api/sendhttp.php?";
		URLConnection myURLConnection = null;
		URL myURL = null;
		BufferedReader reader = null;
		String encoded_message = null;

		try {
			System.out.println("---->>> In sendSms ---<<<< "+mobileNumber);
			encoded_message = URLEncoder.encode(smsBody, StandardCharsets.UTF_8.toString());

			// Prepare parameter string
			StringBuilder sbPostData = new StringBuilder(mainUrl);
			sbPostData.append("authkey=" + authkey);
			sbPostData.append("&mobiles=" + mobileNumber);
			sbPostData.append("&message=" + encoded_message);
			sbPostData.append("&route=" + route);
			sbPostData.append("&sender=" + senderId);

			// final string
			mainUrl = sbPostData.toString();
			
			System.out.println("---->>> In sendSms stringbuilder---<<<< "+mainUrl);

			myURL = new URL(mainUrl);
			myURLConnection = myURL.openConnection();
			myURLConnection.connect();

			reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
			// reading response
			/*String response;
			while ((response = reader.readLine()) != null) {
				finalResponse = response;

			}*/
			// finally close connection
			reader.close();
			smsResponseModel.setCode(200);
			smsResponseModel.setMessage("Success");
		} catch (Exception e) {
			smsResponseModel.setCode(500);
			smsResponseModel.setMessage("Fail");
		}

		return smsResponseModel;
	}
}
