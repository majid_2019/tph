/**
 * 
 */
package com.tph.sms.externalservice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;
import com.tph.request.model.SmsRequestModel;
import com.tph.response.model.SmsResponseModel;

/**
 * @author majidkhan
 *
 */
public class LambdaAPICaller {

	/**
	 * 
	 * @param smsBody
	 * @param mobileNumber
	 * @param responseModel
	 * @return
	 */
	public SmsResponseModel sendSms(SmsRequestModel body, SmsResponseModel finalResponse)
			throws Exception {

		//String url = "https://s0fiwq8vaa.execute-api.ap-south-1.amazonaws.com/prod/sendsms";
		String url = "https://api.thepurchasehouse.com/sms-prod/sendsms";
		
		String POST_PARAMS = new Gson().toJson(body);
		
		System.out.println("SMS API Caller request --->>> "+POST_PARAMS);
		URL obj = new URL(url);
		HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
		postConnection.setRequestMethod("POST");
		postConnection.setRequestProperty("Content-Type", "application/json");
		postConnection.setDoOutput(true);
		OutputStream os = postConnection.getOutputStream();
		os.write(POST_PARAMS.getBytes());
		os.flush();
		os.close();
		int responseCode = postConnection.getResponseCode();
		System.out.println("responseCode -- "+postConnection.getResponseMessage());
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			System.out.println("OTP Api Response from Vendor caller --"+response);
			// print result
			finalResponse.setCode(200);
			finalResponse.setMessage("Success");
		} else {
			finalResponse.setCode(500);
			finalResponse.setMessage("Otp Fail");
		}
		
		return finalResponse;
	}
}
