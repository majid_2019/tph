package com.tph.response.model;

import lombok.Data;

@Data
public class IndustrialSubCateData {
	private Integer id;
	private String indusSubCatName;
}
