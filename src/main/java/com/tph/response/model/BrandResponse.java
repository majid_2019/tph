/**
 * 
 */
package com.tph.response.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BrandResponse {
	private String brandName;
	private Integer brandId;
}
