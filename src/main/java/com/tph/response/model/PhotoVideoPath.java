/**
 * 
 */
package com.tph.response.model;

import lombok.Data;

/**
 * @author majidkhan
 */
@Data
public class PhotoVideoPath {

	private String path;
	private Integer itemId;
	private String fileName;
}
