/**
 * 
 */
package com.tph.response.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BuyerCornerRequirementModel {
	private String productDescr;
	private String productName;
	private Integer productSize;
	private String productBrand;
	private Integer requireQuantity;
	private String companyName;
	private String companyAddress;
	private Long companyPhnNumber;
	private Integer pinCode;
	private Integer countryId;
	private String countryName;
	private Integer stateId;
	private String stateName;
	private Integer cityId;
	private String cityName;
	private String contactPerson;
	private String mobileNumber;
	private String emailId;
	private String verifiedBy;
	private String uomType;
	private String productcode;
}
