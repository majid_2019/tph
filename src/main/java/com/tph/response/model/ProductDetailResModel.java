/**
 * 
 */
package com.tph.response.model;

import java.util.List;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class ProductDetailResModel {
	private Integer productId;
	private String categoryName;
	private String productName;
	private String productDescription;
	private String productBrand;
	private String productCountry;
	private String productState;
	private String productCity;
	private Integer industryId;
	private Integer categoryId;
	private Integer subCategoryId;
	private String imagePath;
	private String productCode;
	private Integer productStatusCode;
	private String subCatName;
	private Integer expInMonth;
	private Integer expInYear;
	private Integer serviceCatId;
	private String serviceCatName;
	private String remark;
	private String postedDate;
	
	private String mfgPurchaseyear;
	private String productSubDescription;
	private Integer quantity;
	private String uom;
	private String pricePerUnit;
	private String productLoc;
	private int priceType;
	private String status;
	private String link;
	private String itemCode;
	private List<String> imageList;
	private List<String> videoList;
	private List<ProductDetailResModel> relatedProduct;
}
