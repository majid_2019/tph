package com.tph.response.model;

import lombok.Data;

@Data
public class LoginResponseModel {
	private String firstName;
	private String lastName;
	private String lastLogin;
	private String passwordExOn;
	private Integer userId;
	private String mobileNumber;
	private String componyName;
	private String email;
}
