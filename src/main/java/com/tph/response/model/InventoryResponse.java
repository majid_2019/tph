/**
 * 
 */
package com.tph.response.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class InventoryResponse {
	private String inventoryName;
	private String inventoryId;
	private String amount;
	private Double paybleAmount;
	
	private String percentage;
	private String igst;
	private String cgst;
	private String sgst;
}
