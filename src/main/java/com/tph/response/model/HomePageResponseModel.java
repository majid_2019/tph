/**
 * 
 */
package com.tph.response.model;

import java.util.List;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class HomePageResponseModel {
	private Integer totalProductView;
	private Integer totalProductSold;
	private List<ProductDetailResModel> featuredProductList;
	private List<ProductDetailResModel> recentAddedProductList;
	private List<ProductDetailResModel> stockClearanceLeadsList;
	private List<ProductDetailResModel> industrialServicesList;
	private List<BuyerCornerRequirementModel> buyerCornerReqList;
	private List<ProductDetailResModel> subCatProductList;
	private ProductDetailResModel details;
	private Long InventoryProducts;
	private Long ServiceProvider;
	private Long HappyClients;
	private SellerDetails sellerDetail;
	private List<ProductDetailResModel> relatedProductList;
	private List<IndusPlotResponseModel> industrialPlotList;
	private List<ProductDetailResModel> soldProductList;
	private List<IndusProductServiceResModel> industrialProdServiceList;
	private List<ProductDetailResModel> stockClearanceLeadList;
}
