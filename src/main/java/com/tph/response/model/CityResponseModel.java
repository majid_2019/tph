/**
 * 
 */
package com.tph.response.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */
@Data
public class CityResponseModel {
	private String cityName;
	private Integer cityId;

}
