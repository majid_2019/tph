/**
 * 
 */
package com.tph.response.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class SellerAttributesResponse {
	private String indName;
	private String catName;
	private String attribName;
	private String attriAlias;
}
