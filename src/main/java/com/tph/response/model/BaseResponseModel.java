/**
 * 
 */
package com.tph.response.model;

import java.util.List;

import com.tph.request.model.EditPostResponse;
import com.tph.request.model.PaymentDetail;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BaseResponseModel {

	private Integer status;
	private String message;
	private Integer userId;
	private String notifyType;
	private SellerDashboardResponseModel mainResponse;
	private HomePageResponseModel homePageMainResponse;
	private List<ActivePostResponseModel> activePostList;
	private ProfileViewResponseModel profileView;
	private List<IndustrialCateData> industriesCatData;
	private List<InventoryResponse> allInventoryList;
	private List<InventoryResponse> inventries;
	private List<SellerAttributesResponse> sellrAllAttributesList;
	private LoginResponseModel loginResponse;
	private PaymentDetail paymentDetail;
	private InventoryResponse paymentData;
	private EditPostResponse editPost;
	private Integer tempTotalUser;
	private Long tempTodayUser;
}
