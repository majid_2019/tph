/**
 * 
 */
package com.tph.response.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */
@Data
public class CountryResponse {
	private Integer countryId;
	private String countryName;
}
