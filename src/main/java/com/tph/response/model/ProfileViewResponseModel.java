/**
 * 
 */
package com.tph.response.model;

import com.tph.request.model.SellerBaseRequestModel;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class ProfileViewResponseModel extends SellerBaseRequestModel {
	private String companyName;
	private String contactPerson;
	private String mobileNumber;
	private String emailId;
	private String address;
	private String gstNumber;
	private String countryName;
	private String stateName;
	private String cityName;
}
