/**
 * 
 */
package com.tph.response.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class SellerDetails {
	private String sellerEmail;
	private String sellerMobileNumber;
	private String sellerFName;
	private String sellerLName;
	private String sellerCmpnyName;
	private String stateName;
	private String cityName;
	private Integer id;
}
