/**
 * 
 */
package com.tph.response.model;

import java.util.List;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class IndusPlotResponseModel {
	private Integer productId;
	private String productCode;
	private String category;
	private Integer ipTypeId;
	private String ipType;
	private String ipTitle;
	private String ipDescription;
	private Integer quantity;
	private Integer uomId;
	private Double pricePerUnit;
	private String availableYear;
	private int priceType; // is Negotiable
	private String resellerName;
	private Integer userId;
	private Integer countryId;
	private String productCountry;
	private Integer stateId;
	private String productState;
	private Integer cityId;
	private String productCity;
	private String productLoc;
	private String contactPerson;
	private String mobileNumber;
	private String altrMobileNumber;
	private String remark;
	private String image;
	
	private List<String> imageList;
	private List<String> videoList;
	private List<IndusPlotResponseModel> indPlotsList;
}
