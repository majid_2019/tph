package com.tph.response.model;

import lombok.Data;

@Data
public class NotifyResponse {
	private String body;
	private String name;
}
