package com.tph.constants;

public enum CommunicationCategory {
	MAKE_AN_OFFER("Make an offer"),
	CONTACT_SELLER("Contact seller"), 
	OTP_SMS("OTP SMS"),
	CONTACT_SELLER_TRACKING_SMS("Contact Seller Tracking SMS"),
	MAKE_AN_OFFER_TRACKING_SMS("Make an offer Tracking SMS"),
	OTP_MSG("OTP MSG"),
	POP_UP_MSG("POP UP MSG"),
	NOTIFICATION_MKN_OFFER("Notification MKN Offer"),
	NOTIFICATION_CONTACT_SELLER("Notification Contact Seller"),
	WELCOME_SMS("Welcome SMS");
	
	String communicationType;
	
	public String getCommunicationCategory() {
		return communicationType;
	}
	
	private CommunicationCategory(String catType) {
		this.communicationType = catType;
	}
	
	public static void main(String[] args) {
		System.out.println(CommunicationCategory.valueOf(CommunicationCategory.WELCOME_SMS.name()).getCommunicationCategory());
	}
}
