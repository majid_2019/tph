package com.tph.constants;

public enum PostAction {
	SOLD_POST, UPDATE_POST, FETCH_POST
}
