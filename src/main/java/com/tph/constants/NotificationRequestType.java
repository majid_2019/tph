package com.tph.constants;

public enum NotificationRequestType {

	MAKE_AN_OFFER, CONTACT_SELLER;

	
	public static void main(String[] args) {
		System.out.println(NotificationRequestType.valueOf(NotificationRequestType.MAKE_AN_OFFER.name()));
	}
}
