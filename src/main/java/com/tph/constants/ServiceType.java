package com.tph.constants;

public enum ServiceType {
	INDUSTRIAL_PLOT(1), STOCK_CLEARANCE(2), INDUSTRIAL_SERVICES(3);

	Integer type;

	public Integer getType() {
		return this.type;
	}

	private ServiceType(Integer ty) {
		this.type = ty;
	}
	
	public static void main(String[] args) {
		System.out.println(ServiceType.valueOf(ServiceType.INDUSTRIAL_SERVICES.name()).getType());
	}
}
