/**
 * 
 */
package com.tph.constants;

/**
 * @author majidkhan
 *
 */
public class ConstantKeys {
	
	/**
	 * Image/Video enum Item id
	 */
	public static Integer IMAGE_ITEM_ID = 1;
	public static Integer VIDEO_ITEM_ID = 2;
	public static Integer DOC_ITEM_ID = 3;
	
	//public static final String s3MainUrl = "https://tph-doc.s3.ap-south-1.amazonaws.com/"; // QA
	public static final String s3MainUrl = "https://tph-doc-prod.s3.ap-south-1.amazonaws.com/"; // Prod
	//public static final String s3MainUrl = System.getenv("s3base_url");
	public static final String FILE_UPLOAD_BUCKET = System.getenv("invoiceBucketName");//"tph-doc";
	public static final String FILE_UPLOAD_FOLDER = "Invoices";
	
	/**
	 * Status Code
	 */
	public static Integer SUCCESS_CODE = 200;
	public static Integer NO_CONTENT_CODE = 204;
	public static Integer BAD_REQUEST_CODE = 400;
	public static Integer RECORD_NOT_AVAIL = 404;
	public static Integer INTERNAL_SERVER_ERROR_CODE = 500;
	
	/**
	 * Status Message
	 */
	public static String INTERNAL_SERVER_ERROR_MSG = "Internal Server Error";
	public static String SUCCESS_MSG = "SUCCESS";
	public static String NOT_SAVE = "Data not saved";
	public static String BUYER_NOT_AVAIL = "This user is not available in our system";
	public static String BAD_REQUEST_MSG = "Please check request";
	public static String FAIL = "fail";
	public static String USER_NOT_EXISTS = "This user is not exists";
	public static String ALREADY_IN_WISH_LIST = "You have already added this product in your wishlist";
	public static String USER_NOT_EXIST = "This user is not exist";
	public static String INVALID_SELLER_LOGIN_PW = "Please enter valid password";
	public static String OTP_SEND = "Otp Send Successfully";
	public static String SEND_SMS_SERVICE_TYPE = "Please send ServiceType";
	public static String SELLER_NOT_EXIST = "This user is not exist with TPH, Please register.";
	public static String IN_VALID_OTP = "Please enter valid OTP ";
	public static String SELLER_ALREDY_EXISTS = "This userName or Mobile number is allready registered with us.";
	public static String SELLER_ALREDY_EXISTS_AS_BUYER = "This userName or Mobile number is allready registered with us as a buyer.";
	public static String TYPE_NOT_AVAIL = "Please provide type";
	public static String EMPTY = "";
	public static String CONTACT_SELLER_MSG_STOPPED = "Message Stopped";
	public static String OLD_PW_NOT_VALID = "Old password is incorrect";
	public static String PW_CHANGE_SUCCESSFULLY = "Password change successfully";
	
	/**
	 * Product Detail Query constant
	 */
	public static final String INVENTORY = "INVENTORY";
	public static final String INDUSTRIAL_PLOT = "INDUSTRIAL_PLOT"; 
	public static final String INDUSTRIAL_SERVICES ="INDUSTRIAL_SERVICES";
	public static final String STOCK_CLEARANCE = "STOCK_CLEARANCE";
	
	/**
	 * 
	 */
	public static final String awsAccessKey = "AKIAJTIH35WUOZY57F5Q";
	public static final String awsSecretKey = "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";
	public static final String smsServiceLambda = "messageservice";
	
	/**
	 * All Environment Variables.
	 */
	public static final String IS_MOCK_SELLER_SMS = System.getenv("isMockSellerSms");
	public static final String MOCK_MOBILE_NUMBER = System.getenv("mockMobileNumbers");
	
}
