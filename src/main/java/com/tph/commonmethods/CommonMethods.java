/**
 * 
 */
package com.tph.commonmethods;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jooq.Record;
import org.jooq.Result;

import com.google.gson.Gson;
import com.tph.jooq.tph_db.tables.ProductDetail;
import com.tph.request.model.NotificationRequest;
import com.tph.request.model.RegistraionRequestModel;
import com.tph.request.model.SmsRequestModel;

/**
 * @author majidkhan
 *
 */
public class CommonMethods {

	/**
	 * @param createdDate
	 * @return
	 * @throws Exception
	 */
	public String convertTimeStampToDate(Timestamp createdDate) throws Exception {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d MMMM yyyy");

		return simpleDateFormat.format(createdDate);
	}
	
	/**
	 * Return Int value of any booleans
	 * @param b
	 * @return
	 */
	public int boolToInt(Boolean b) {
		return b ? 1 : 0;
	}
	
	/**
	 * This function is using for create Product code with the combination of 9 zero and index value of productdetail table
	 * @param constantValue
	 * @param subCatId 
	 * @return
	 */
	public String getProductCode(int constantValue, Integer subCatId) {
		String finalString = null;
		String constValue = null;
		
		if(subCatId != 0) {
			constValue = "TPHIR";
		}
		else {
			constValue = "TPHSC";
		}

		String zeros = "000000000";
		String indexValue = String.valueOf(constantValue);
		
		String s = zeros.substring(0, zeros.length() - indexValue.length());
		finalString = constValue.concat(s.concat(indexValue));
		
		return finalString;
	}
	
	/**
	 * This function is using for create Product code with the combination of 9 zero and index value of Industrial Plot list
	 *  table is ip_product_list
	 * @param constantValue
	 * @return
	 */
	public String getProductCodeIpProductList(int constantValue) {
		String constValue = "TPHIP";
		String zeros = "000000000";
		String indexValue = String.valueOf(constantValue);
		
		String s = zeros.substring(0, zeros.length() - indexValue.length());
		String finalString = constValue.concat(s.concat(indexValue));
		return finalString;
	}
	
	/**
	 * This function is using for create Product code with the combination of 9 zero and index value of Industrial Plot list
	 *  table is ip_product_list
	 * @param constantValue
	 * @return
	 */
	public String getProductCodeIndusServiceProver(int constantValue) {
		String constValue = "TPHSP";
		String zeros = "000000000";
		String indexValue = String.valueOf(constantValue);
		
		String s = zeros.substring(0, zeros.length() - indexValue.length());
		String finalString = constValue.concat(s.concat(indexValue));
		return finalString;
	}
	
	/**
	 * This function is using for create Product code with the combination of 9 zero and index value of BuyerCorner
	 *  table is ip_product_list
	 * @param constantValue
	 * @return
	 */
	public String getProductCodeBuyerCorner(int constantValue) {
		String constValue = "TPHBR";
		String zeros = "000000000";
		String indexValue = String.valueOf(constantValue);
		
		String s = zeros.substring(0, zeros.length() - indexValue.length());
		String finalString = constValue.concat(s.concat(indexValue));
		return finalString;
	}
	
	
	/**
	 * @param imageMap
	 * @param integer 
	 * @return
	 * @throws Exception
	 */
	public String getMapValue(Map<Integer, String> imageMap, Integer productId) throws Exception{
		Iterator<Entry<Integer, String>> it = imageMap.entrySet().iterator();
		String imagePath = null;
		while (it.hasNext()) {
			Entry<Integer, String> pair = it.next();
			if(productId.equals(pair.getKey())) {
				imagePath = pair.getValue();
				break;
			}
		}
		
		return imagePath;
	}
	
	/**
	 * Add all ProductDetail Id in List for fetch Image from image table.
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public List<Integer> getProductIdIdex(Result<Record> result)
			throws Exception {
		List<Integer> idList = new ArrayList<>();

		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				idList.add(record.getValue(ProductDetail.PRODUCT_DETAIL.ID));
			}
		}
		return idList;
	}
	
	/**
	 * Add all StockClearance Id in List for fetch Image from image table.
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public List<Integer> getIdIdex(Result<?> result)
			throws Exception {
		List<Integer> idList = new ArrayList<>();

		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				idList.add(record.getValue(ProductDetail.PRODUCT_DETAIL.ID));
			}
		}
		return idList;
	}
	
	/**
	 * @return
	 */
	public Timestamp currentDate() {
		Timestamp ts = new Timestamp(new Date().getTime());
		return ts;
	}
	
	/**
	 * @return
	 */
	public Integer generateOtp() {
		Integer randomPIN = (int) (Math.random() * 9000) + 1000;
		return randomPIN;
	}

	/**
	 * @param smsContent
	 * @param otp
	 * @return
	 */
	public String getSMsBody(String smsContent, Integer otp) {
		String otpBody = smsContent.replace("xxxx", otp.toString()); 
		return otpBody;
	}
	
	/**
	 * @param smsContent
	 * @param requestModel
	 * @return
	 */
	public String getContactSellerSMSBody(String smsContent, SmsRequestModel requestModel) {
		
		String otpBody = smsContent.replace("buyername", requestModel.getBuyerName()); 
		return otpBody.replace("buyernumber", requestModel.getFromAddress());
	}

	public String getMKNOfferNotiBody(String notificationContent, NotificationRequest request) {
		notificationContent = notificationContent.replace("Buyer_Name", request.getBuyerName());
		notificationContent = notificationContent.replace("Buyer_Mobile", request.getBuyerMobile().toString());
		notificationContent = notificationContent.replace("Amount", request.getOfferPrice().toString());
		notificationContent = notificationContent.replace("product_quantity", request.getQuantity().toString().concat(" "+request.getUom()));
		notificationContent = notificationContent.replace("product_code", request.getProductCode());
		notificationContent = notificationContent.replace("product_title", request.getProductTitle());
		
		return notificationContent;
	}

	public String getContSellerNotiBody(String notificationContent, NotificationRequest request) {
		
		notificationContent = notificationContent.replace("Buyer_Name", request.getBuyerName());
		notificationContent = notificationContent.replace("Buyer_Mobile", request.getBuyerMobile().toString());
		notificationContent = notificationContent.replace("product_code", request.getProductCode());
		notificationContent = notificationContent.replace("product_title", request.getProductTitle());
		
		return notificationContent;
	}
	
	public String getContactSellerTrackingBody(String notificationContent, SmsRequestModel requestModel, boolean isContactSeller) {
		if(isContactSeller) {
			notificationContent = notificationContent.replace("Buyer_Name", requestModel.getBuyerName());
			notificationContent = notificationContent.replace("Buyer_Mobile", requestModel.getBuyerNumber());
			notificationContent = notificationContent.replace("Product_Code", requestModel.getProductCode());
			notificationContent = notificationContent.replace("Product_Title", requestModel.getProductTitle());
			notificationContent = notificationContent.replace("Seller_Name", requestModel.getSellerName());
			notificationContent = notificationContent.replace("Seller_Mobile", requestModel.getSellerMoble());
		}
		else {
			notificationContent = notificationContent.replace("Buyer_Name", requestModel.getBuyerName());
			notificationContent = notificationContent.replace("Buyer_Mobile", requestModel.getBuyerNumber());
			notificationContent = notificationContent.replace("Amount", requestModel.getOfferPrice());
			notificationContent = notificationContent.replace("product_quantity", requestModel.getQuantity().toString().concat(" "+requestModel.getUom()));
			notificationContent = notificationContent.replace("product_code", requestModel.getProductCode());
			notificationContent = notificationContent.replace("product_title", requestModel.getProductTitle());
			notificationContent = notificationContent.replace("Seller_Name", requestModel.getSellerName());
			notificationContent = notificationContent.replace("Seller_Mobile", requestModel.getSellerMoble());
		}
		
		return notificationContent;
	}

	public String getWelcomeSMSBody(String smsContent, RegistraionRequestModel requestModel) {
		
		String otpBody = smsContent.replace("xxxx", requestModel.getMobileNumber()); 
		return otpBody;
	}
}
