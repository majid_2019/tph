/**
 * 
 */
package com.tph.commonmethods;

/**
 * @author majidkhan
 *
 */
public enum ImageVideo {
	IMAGE,
	VIDEO,
	DOC
}
