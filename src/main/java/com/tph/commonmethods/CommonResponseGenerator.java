/**
 * 
 */
package com.tph.commonmethods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.Result;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.tph.constants.ConstantKeys;
import com.tph.jooq.tph_db.tables.BuyerCornerRequirement;
import com.tph.jooq.tph_db.tables.CategoriesMaster;
import com.tph.jooq.tph_db.tables.CentralCommunicationLog;
import com.tph.jooq.tph_db.tables.CityMaster;
import com.tph.jooq.tph_db.tables.CountryMaster;
import com.tph.jooq.tph_db.tables.IndustrialProductServices;
import com.tph.jooq.tph_db.tables.IndustrialServices;
import com.tph.jooq.tph_db.tables.IpProductList;
import com.tph.jooq.tph_db.tables.IpTypeMaster;
import com.tph.jooq.tph_db.tables.ProductDetail;
import com.tph.jooq.tph_db.tables.ProductImageVideo;
import com.tph.jooq.tph_db.tables.StateMaster;
import com.tph.jooq.tph_db.tables.SubCategories;
import com.tph.jooq.tph_db.tables.UomMaster;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.CityMasterRecord;
import com.tph.jooq.tph_db.tables.records.CountryMasterRecord;
import com.tph.jooq.tph_db.tables.records.IndustrialProductServicesRecord;
import com.tph.jooq.tph_db.tables.records.IndustrialTypeRecord;
import com.tph.jooq.tph_db.tables.records.IpProductListRecord;
import com.tph.jooq.tph_db.tables.records.ProductBrandMasterRecord;
import com.tph.jooq.tph_db.tables.records.ProductDetailRecord;
import com.tph.jooq.tph_db.tables.records.StateMasterRecord;
import com.tph.jooq.tph_db.tables.records.UomMasterRecord;
import com.tph.jooq.tph_db.tables.records.UsersRecord;
import com.tph.request.model.EditPostResponse;
import com.tph.response.model.ActivePostResponseModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.BrandResponse;
import com.tph.response.model.BuyerCornerRequirementModel;
import com.tph.response.model.CityResponseModel;
import com.tph.response.model.CountryResponse;
import com.tph.response.model.IndusDataModel;
import com.tph.response.model.IndusPlotResponseModel;
import com.tph.response.model.IndusProductServiceResModel;
import com.tph.response.model.IndustrialCateData;
import com.tph.response.model.IndustrialSubCateData;
import com.tph.response.model.InventoryResponse;
import com.tph.response.model.LoginResponseModel;
import com.tph.response.model.NotifyResponse;
import com.tph.response.model.ProductDetailResModel;
import com.tph.response.model.ProfileViewResponseModel;
import com.tph.response.model.SellerAttributesResponse;
import com.tph.response.model.SellerDetails;
import com.tph.response.model.StateResponseModel;
import com.tph.response.model.UomResponseModel;

/**
 * @author majidkhan
 *
 */
public class CommonResponseGenerator {

	/**
	 * @param record
	 * @return
	 * @throws Exception
	 */
	public ProfileViewResponseModel getProfileData(Record record) throws Exception {
		ProfileViewResponseModel profileViewResponseModel = new ProfileViewResponseModel();
		if (record != null) {
			profileViewResponseModel.setCompanyName(record.get(Users.USERS.COMPANY));
			profileViewResponseModel.setContactPerson(record.get(Users.USERS.FIRST_NAME) + " "
					+ (record.get(Users.USERS.LAST_NAME) != null ? record.get(Users.USERS.LAST_NAME) : ""));
			profileViewResponseModel.setMobileNumber(record.get(Users.USERS.USER_MOBILE));
			profileViewResponseModel.setEmailId(record.get(Users.USERS.EMAIL));
			profileViewResponseModel.setAddress(record.get(Users.USERS.ADDRESS_1) + " "
					+ (record.get(Users.USERS.ADDRESS_2) != null ? record.get(Users.USERS.ADDRESS_2) : "") + " "
					+ (record.get(Users.USERS.ADDRESS_3) != null ? record.get(Users.USERS.ADDRESS_3) : ""));
			profileViewResponseModel.setCountryName(record.get(CountryMaster.COUNTRY_MASTER.NAME));
			profileViewResponseModel.setStateName(record.get(StateMaster.STATE_MASTER.STATE));
			profileViewResponseModel.setCityName(record.get(CityMaster.CITY_MASTER.CITY));
			profileViewResponseModel.setGstNumber(record.get(Users.USERS.GST_NO));
		}
		return profileViewResponseModel;
	}

	/**
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel getSuccessMsgResponse(BaseResponseModel sellerBaseResponseModel) {
		sellerBaseResponseModel.setStatus(ConstantKeys.SUCCESS_CODE);
		sellerBaseResponseModel.setMessage(ConstantKeys.SUCCESS_MSG);

		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel getBasReqMsgResponse(BaseResponseModel sellerBaseResponseModel) {
		sellerBaseResponseModel.setStatus(ConstantKeys.BAD_REQUEST_CODE);
		sellerBaseResponseModel.setMessage(ConstantKeys.BAD_REQUEST_MSG);

		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel getExceptionResponse(BaseResponseModel sellerBaseResponseModel) {
		sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
		sellerBaseResponseModel.setMessage(ConstantKeys.INTERNAL_SERVER_ERROR_MSG);
		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel getLambdaFailResponse(BaseResponseModel sellerBaseResponseModel) {
		sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
		sellerBaseResponseModel.setMessage(ConstantKeys.FAIL);
		return sellerBaseResponseModel;
	}

	/**
	 * @param result
	 * @return
	 */
	public List<UomResponseModel> getUonList(Result<UomMasterRecord> uomList) throws Exception {
		List<UomResponseModel> uomModelList = new ArrayList<>();
		UomResponseModel uomResponseModel;

		if (uomList.size() > 0) {
			for (int i = 0; i < uomList.size(); i++) {
				uomResponseModel = new UomResponseModel();

				uomResponseModel.setUomId(uomList.get(i).getId());
				uomResponseModel.setUomName(uomList.get(i).getUomName());
				uomResponseModel.setUomAcronym(uomList.get(i).getAcronym());
				uomModelList.add(uomResponseModel);
			}
		}

		return uomModelList;
	}

	/**
	 * @param stateList
	 * @return
	 */
	public List<CityResponseModel> getCityList(Result<CityMasterRecord> cityList) throws Exception {
		List<CityResponseModel> list = new ArrayList<>();
		CityResponseModel cityResponseModel;

		if (cityList.size() > 0) {
			for (int i = 0; i < cityList.size(); i++) {
				cityResponseModel = new CityResponseModel();

				cityResponseModel.setCityId(cityList.get(i).getId());
				cityResponseModel.setCityName(cityList.get(i).getCity());
				list.add(cityResponseModel);
			}
		}

		return list;
	}

	/**
	 * @param stateList
	 * @return
	 */
	public List<StateResponseModel> getStateList(Result<StateMasterRecord> stateList) throws Exception {
		List<StateResponseModel> list = new ArrayList<>();
		StateResponseModel stateResponseModel;

		if (stateList.size() > 0) {
			for (int i = 0; i < stateList.size(); i++) {
				stateResponseModel = new StateResponseModel();

				stateResponseModel.setStateId(stateList.get(i).getId());
				stateResponseModel.setStateName(stateList.get(i).getState());
				list.add(stateResponseModel);
			}
		}

		return list;
	}

	/**
	 * @param countryList
	 * @return
	 */
	public List<CountryResponse> getCountryList(Result<CountryMasterRecord> countryList) {
		List<CountryResponse> list = new ArrayList<>();
		CountryResponse countryResponse;

		if (countryList.size() > 0) {
			for (int i = 0; i < countryList.size(); i++) {
				countryResponse = new CountryResponse();

				countryResponse.setCountryId(countryList.get(i).getId());
				countryResponse.setCountryName(countryList.get(i).getName());
				list.add(countryResponse);
			}
		}

		return list;
	}

	/**
	 * @param BrandList
	 * @return
	 */
	public List<BrandResponse> getBrandList(Result<ProductBrandMasterRecord> result) {
		List<BrandResponse> list = new ArrayList<>();
		BrandResponse brandResponse;

		if (result.size() > 0) {
			for (int i = 0; i < result.size(); i++) {
				brandResponse = new BrandResponse();

				brandResponse.setBrandId(result.get(i).getId());
				brandResponse.setBrandName(result.get(i).getBrandName());
				list.add(brandResponse);
			}
		}

		return list;
	}

	/**
	 * @param result
	 */
	public void fetchIndusrialList(Result<Record> result) throws Exception {

		IndusDataModel indusDataModel = null;
		IndustrialCateData industrialCateData;
		JSONArray indType = new JSONArray();
		JSONArray indCatTypeArray = new JSONArray();

		Map<Integer, String> mapIndustrial = new HashMap<Integer, String>();
		Map<Integer, String> mapIndusCategory = new HashMap<Integer, String>();

		JSONObject jsonObjCatMaster;
		JSONObject jsonObjIndType = null;
		List<String> catMster = new ArrayList<String>();

		List<String> subCatMster = new ArrayList<String>();
		List<IndustrialCateData> catList = new ArrayList<>();
		// System.out.println("Result-- "+result);

		System.out.println(result);

		for (int i = 0; i < result.size(); i++) {
			jsonObjIndType = new JSONObject();
			jsonObjCatMaster = new JSONObject();

			if (mapIndustrial.size() > 0)
				for (Map.Entry<Integer, String> entry : mapIndustrial.entrySet()) {

					// System.out.println("("+i+")"+" Key --- "+entry.getKey() +"
					// ====="+result.get(i).getValue("ind_id") + " --- equal ---
					// "+entry.getKey().equals((Integer) result.get(i).getValue("ind_id")));

					if (!entry.getKey().equals((Integer) result.get(i).getValue("ind_id"))) {
						industrialCateData = new IndustrialCateData();
						indusDataModel = new IndusDataModel();

						// Store Industries in Object
						indusDataModel.setId((Integer) result.get(i).getValue("ind_id"));
						indusDataModel.setIndusName((String) result.get(i).getValue("industrial_name"));
						indType.put(indusDataModel);

						// Store Industries Categories in Object
						industrialCateData.setId((Integer) result.get(i).getValue("cat_master_id"));
						industrialCateData.setIndusCatName((String) result.get(i).getValue("industrial_cat_name"));
						catList.add(industrialCateData);

						indusDataModel.setIndustrialCateData(catList);

						/*
						 * indType.put(indusDataModel); indusDataModel.setIndustrialCateData(catList);
						 * indCatTypeArray.put(indusDataModel);
						 */

					}
				}

			mapIndustrial.put((Integer) result.get(i).getValue("ind_id"),
					(String) result.get(i).getValue("industrial_name"));
		}
		System.out.println(new Gson().toJson(indType));
	}

	/**
	 * Store all Industrial data in List
	 * 
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public List<IndusDataModel> IndustrialTypeRecord(Result<IndustrialTypeRecord> result) throws Exception {
		IndusDataModel indusDataModel = null;
		List<IndusDataModel> list = new ArrayList<>();

		if (result.size() > 0) {
			for (int i = 0; i < result.size(); i++) {
				indusDataModel = new IndusDataModel();

				indusDataModel.setId(result.get(i).getId());
				indusDataModel.setIndusName(result.get(i).getName().replace("\u0026", "&"));

				list.add(indusDataModel);
			}
		}

		return list;

	}

	/**
	 * Create Json for Industrial category data.
	 * 
	 * @param sellerBaseResponseModel
	 * 
	 * @param fetchCategoryMaster
	 * @return
	 */
	public List<IndustrialCateData> fetchIndCategoryList(BaseResponseModel sellerBaseResponseModel,
			Result<Record> fetchCategoryMaster) throws Exception {
		IndustrialCateData industrialCateData;
		Set<IndustrialCateData> set = new HashSet<IndustrialCateData>();
		if (fetchCategoryMaster.size() > 0) {
			for (int i = 0; i < fetchCategoryMaster.size(); i++) {
				industrialCateData = new IndustrialCateData();
				industrialCateData.setId((Integer) fetchCategoryMaster.get(i).getValue("cat_master_id"));
				industrialCateData.setIndusCatName((String) fetchCategoryMaster.get(i).getValue("cat_name"));

				set.add(industrialCateData);
			}
		}

		return getSubCatData(set, fetchCategoryMaster);
	}

	// Using for fetch inner json of Category master..
	private List<IndustrialCateData> getSubCatData(Set<IndustrialCateData> set, Result<Record> fetchCategoryMaster)
			throws Exception {
		List<IndustrialCateData> catList = new ArrayList<>();
		IndustrialSubCateData industrialSubCateData = null;
		List<IndustrialSubCateData> subCatList = null;
		IndustrialCateData iterateCateData;

		if (set.size() > 0) {
			Iterator<IndustrialCateData> itr = set.iterator();
			while (itr.hasNext()) {
				iterateCateData = itr.next();
				subCatList = new ArrayList<>();

				for (int i = 0; i < fetchCategoryMaster.size(); i++) {
					if (iterateCateData.getId() == (Integer) fetchCategoryMaster.get(i).getValue("cat_master_id")) {
						industrialSubCateData = new IndustrialSubCateData();

						industrialSubCateData.setId((Integer) fetchCategoryMaster.get(i).getValue("sub_category_id"));
						industrialSubCateData
								.setIndusSubCatName((String) fetchCategoryMaster.get(i).getValue("sub_category_name"));

						subCatList.add(industrialSubCateData);
					}

					iterateCateData.setSubCategoryList(subCatList);
				}
				catList.add(iterateCateData);
			}
		}
		return catList;
	}

	/**
	 * @param imagePathMap
	 * @param dslContext
	 * @param fetchActivePost
	 * @return
	 */
	public List<ActivePostResponseModel> activePost(Result<ProductDetailRecord> record) throws Exception {
		List<ActivePostResponseModel> activePostList = new ArrayList<>();
		ActivePostResponseModel activePostResponseModel;
		if (record.size() > 0) {
			for (int i = 0; i < record.size(); i++) {
				activePostResponseModel = new ActivePostResponseModel();

				activePostResponseModel.setId(record.get(i).getId());
				activePostResponseModel.setProductTitle(record.get(i).getProductName());
				activePostResponseModel.setProductDetails(record.get(i).getProductSubDesc());
				activePostResponseModel.setPricePerUnit(record.get(i).getPricePerUnit().doubleValue());
				activePostResponseModel
						.setCreateDate(new CommonMethods().convertTimeStampToDate(record.get(i).getCreatedDate()));
				activePostResponseModel.setProductCode(record.get(i).getProductCode());
				
				activePostResponseModel.setRemark(record.get(i).getRemarks()!=null?record.get(i).getRemarks().toUpperCase():null);
				if (record.get(i).getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1) != null)
					activePostResponseModel.setImagePath(
							ConstantKeys.s3MainUrl.concat(record.get(i).getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1)));

				/*
				 * String image = new CommonMethods().getMapValue(imageMap,
				 * record.get(i).getId()); if(image != null)
				 * activePostResponseModel.setImagePath(ConstantKeys.s3MainUrl.concat(image));
				 */

				activePostList.add(activePostResponseModel);
			}
		}
		return activePostList;
	}

	public EditPostResponse inventoryEditPost(Result<Record> result) throws Exception {
		EditPostResponse editPostResponse = new EditPostResponse();
		List<String> image = new ArrayList<>();
		List<String> video = new ArrayList<>();

		for (Record record : result) {

			editPostResponse.setProductCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE));
			editPostResponse.setIndustrialTypeId(record.getValue(ProductDetail.PRODUCT_DETAIL.INDUSTRIAL_TYPE_ID));
			editPostResponse.setCatTypeId(record.getValue(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID));
			editPostResponse.setSubCatTypeId(record.getValue(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID));
			editPostResponse.setProductName(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME));
			editPostResponse.setSubsDescryption(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_SUB_DESC));
			editPostResponse.setDescryption(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION));
			editPostResponse.setBrand(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND));
			editPostResponse.setPurchaseYear(record.getValue(ProductDetail.PRODUCT_DETAIL.MFG_PURCHASE_YEAR));
			editPostResponse.setAvailableQuantity(record.getValue(ProductDetail.PRODUCT_DETAIL.QUANTITY));
			editPostResponse.setUomId(record.getValue(ProductDetail.PRODUCT_DETAIL.UOM_ID));
			editPostResponse.setUomAcronym(record.getValue(UomMaster.UOM_MASTER.ACRONYM));
			editPostResponse.setPriceType(record.getValue(ProductDetail.PRODUCT_DETAIL.PRICE_NEGOTIABLE));
			editPostResponse.setProductStatusId(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID));
			editPostResponse.setProductLocation(record.getValue(ProductDetail.PRODUCT_DETAIL.PROD_LOC));
			editPostResponse.setRemark(record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS));
			editPostResponse.setStateId(record.getValue(ProductDetail.PRODUCT_DETAIL.STATE_ID));
			editPostResponse.setCityId(record.getValue(ProductDetail.PRODUCT_DETAIL.CITY_ID));
			editPostResponse.setStateName(record.getValue(StateMaster.STATE_MASTER.STATE));
			editPostResponse.setProductCity(record.getValue(CityMaster.CITY_MASTER.CITY));
			editPostResponse.setResellerName(record.getValue(ProductDetail.PRODUCT_DETAIL.RESELLER_NAME));
			editPostResponse.setCountryName(record.getValue(CountryMaster.COUNTRY_MASTER.NAME));
			editPostResponse.setMobileNumber(record.getValue(ProductDetail.PRODUCT_DETAIL.MOBILE_NO));
			editPostResponse.setUserId(record.getValue(ProductDetail.PRODUCT_DETAIL.USER_ID));

			if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1) != null)
				image.add(ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1)));
			if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE2) != null)
				image.add(ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE2)));
			if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE3) != null)
				image.add(ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE3)));

			if (record.getValue(ProductDetail.PRODUCT_DETAIL.VIDEO_DOC) != null)
				video.add(ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.VIDEO_DOC)));
		}
		editPostResponse.setImageList(image);
		editPostResponse.setVideoList(video);

		return editPostResponse;
	}

	public EditPostResponse plotEditPost(Result<Record> result) throws Exception {
		EditPostResponse editPostResponse = new EditPostResponse();
		List<String> image = new ArrayList<>();
		List<String> video = new ArrayList<>();

		for (Record record : result) {
			editPostResponse.setProductCode(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_CODE));
			editPostResponse.setProductTitle(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_TITTLE));
			editPostResponse.setDescryption(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_DESCRIPTION));
			editPostResponse.setAvailableQuantity(record.getValue(IpProductList.IP_PRODUCT_LIST.QUANTITY));
			editPostResponse.setUomId(record.getValue(IpProductList.IP_PRODUCT_LIST.UOM_ID));
			editPostResponse.setUomAcronym(record.getValue(UomMaster.UOM_MASTER.ACRONYM));
			editPostResponse
					.setPricePerUnit(record.getValue(IpProductList.IP_PRODUCT_LIST.RATE_PER_UNIT).doubleValue());
			editPostResponse.setAvailableYear(record.getValue(IpProductList.IP_PRODUCT_LIST.AVAILABLE_YEAR));
			editPostResponse.setRemark(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_STATUS));
			editPostResponse.setIpNegotiable(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_NEGOTIABLE));
			editPostResponse.setResellerName(record.getValue(IpProductList.IP_PRODUCT_LIST.RESELLER_NAME));
			editPostResponse.setCountryId(record.getValue(IpProductList.IP_PRODUCT_LIST.COUNTRY_ID));
			editPostResponse.setCountryName(record.getValue(CountryMaster.COUNTRY_MASTER.NAME));
			editPostResponse.setStateId(record.getValue(IpProductList.IP_PRODUCT_LIST.STATE_ID));
			editPostResponse.setCityId(record.getValue(IpProductList.IP_PRODUCT_LIST.CITY_ID));
			editPostResponse.setStateName(record.getValue(StateMaster.STATE_MASTER.STATE));
			editPostResponse.setProductCity(record.getValue(CityMaster.CITY_MASTER.CITY));
			editPostResponse.setProductLocation(record.getValue(IpProductList.IP_PRODUCT_LIST.LOCATION));
			editPostResponse.setMobileNumber(record.getValue(IpProductList.IP_PRODUCT_LIST.MOBILE_NO));
			editPostResponse.setRemark(record.getValue(IpProductList.IP_PRODUCT_LIST.REMARK));
			editPostResponse.setAlterMobileNumber(record.getValue(IpProductList.IP_PRODUCT_LIST.ALTERNATE_MOBILE_NO));
			editPostResponse.setUserId(record.getValue(IpProductList.IP_PRODUCT_LIST.USER_ID));

			if (record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_1) != null)
				image.add(ConstantKeys.s3MainUrl.concat(record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_1)));
			if (record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_2) != null)
				image.add(ConstantKeys.s3MainUrl.concat(record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_2)));
			if (record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_3) != null)
				image.add(ConstantKeys.s3MainUrl.concat(record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_3)));
		}
		editPostResponse.setImageList(image);
		editPostResponse.setVideoList(video);

		return editPostResponse;
	}

	public EditPostResponse industServiceEditPost(Result<Record> result) throws Exception {
		EditPostResponse editPostResponse = new EditPostResponse();
		List<String> image = new ArrayList<>();
		List<String> video = new ArrayList<>();

		for (Record record : result) {
			editPostResponse.setProductCode(
					record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_PRODUCT_CODE));
			editPostResponse.setServiceCatId(
					record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_CATERORY_ID));
			editPostResponse.setProductTitle(
					record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_TITLE));
			editPostResponse.setDescryption(
					record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_TITLE));
			editPostResponse
					.setExpInMonth(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_MONTH));
			editPostResponse
					.setExpInYear(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_YEAR));
			editPostResponse.setAvailableYear(
					record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.AVAILABLE_FROM).toString());
			editPostResponse.setUserId(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.USER_ID));
			editPostResponse
					.setCountryId(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.COUNTRY_ID));
			editPostResponse.setCountryName(record.getValue(CountryMaster.COUNTRY_MASTER.NAME));
			editPostResponse
					.setStateId(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.STATE_ID));
			editPostResponse.setCityId(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.CITY_ID));
			editPostResponse.setStateName(record.getValue(StateMaster.STATE_MASTER.STATE));
			editPostResponse.setMobileNumber(
					record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.MOBILE_NUMBER));

			if (record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_1) != null)
				image.add(ConstantKeys.s3MainUrl.concat(record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_1)));
			if (record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_2) != null)
				image.add(ConstantKeys.s3MainUrl.concat(record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_2)));
			if (record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_3) != null)
				image.add(ConstantKeys.s3MainUrl.concat(record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_3)));
		}
		editPostResponse.setImageList(image);
		editPostResponse.setVideoList(video);

		return editPostResponse;
	}

	/**
	 * @param imagePathMap
	 * @param dslContext
	 * @param fetchActivePost
	 * @return
	 */
	public List<ActivePostResponseModel> activePostIndustrialServiceData(Result<IndustrialProductServicesRecord> record)
			throws Exception {
		List<ActivePostResponseModel> activePostList = new ArrayList<>();
		ActivePostResponseModel activePostResponseModel;
		if (record.size() > 0) {
			for (int i = 0; i < record.size(); i++) {
				activePostResponseModel = new ActivePostResponseModel();

				activePostResponseModel.setId(record.get(i).getId());
				activePostResponseModel.setProductTitle(record.get(i).getServiceTitle());
				activePostResponseModel.setProductDetails(record.get(i).getServiceDescription());
				activePostResponseModel
						.setCreateDate(new CommonMethods().convertTimeStampToDate(record.get(i).getCreatedDate()));
				activePostResponseModel.setProductCode(record.get(i).getServiceProductCode());
				if (record.get(i).getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_1) != null)
					activePostResponseModel.setImagePath(ConstantKeys.s3MainUrl.concat(
							record.get(i).getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_1)));

				activePostList.add(activePostResponseModel);
			}
		}
		return activePostList;
	}

	/**
	 * @param imagePathMap
	 * @param dslContext
	 * @param fetchActivePost
	 * @return
	 */
	public List<ActivePostResponseModel> activePostIndustrialPlotData(Result<IpProductListRecord> record)
			throws Exception {
		List<ActivePostResponseModel> activePostList = new ArrayList<>();
		ActivePostResponseModel activePostResponseModel;
		if (record.size() > 0) {
			for (int i = 0; i < record.size(); i++) {
				activePostResponseModel = new ActivePostResponseModel();

				activePostResponseModel.setId(record.get(i).getId());
				activePostResponseModel.setProductTitle(record.get(i).getIpTittle());
				activePostResponseModel.setProductDetails(record.get(i).getIpDescription());
				activePostResponseModel.setPricePerUnit(record.get(i).getRatePerUnit().doubleValue());
				activePostResponseModel
						.setCreateDate(new CommonMethods().convertTimeStampToDate(record.get(i).getCreatedDate()));
				activePostResponseModel.setProductCode(record.get(i).getIpCode());
				activePostResponseModel.setRemark(record.get(i).getRemark().toUpperCase());
				if (record.get(i).getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_1) != null)
					activePostResponseModel.setImagePath(ConstantKeys.s3MainUrl
							.concat(record.get(i).getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_1)));

				activePostList.add(activePostResponseModel);
			}
		}
		return activePostList;
	}

	/**
	 * @param result
	 * @param imagePathMap
	 * @return
	 */
	public List<ProductDetailResModel> getFeaturedProd(Result<Record> result) throws Exception {
		ProductDetailResModel productDetailResModel;
		List<ProductDetailResModel> list = new ArrayList<>();
		List<Integer> idList = new ArrayList<>();

		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				productDetailResModel = new ProductDetailResModel();

				// Add UserId as key and Index as value of ProductDetails
				idList.add(record.getValue(ProductDetail.PRODUCT_DETAIL.ID));

				productDetailResModel.setProductId(record.getValue(ProductDetail.PRODUCT_DETAIL.ID));
				productDetailResModel.setIndustryId(record.getValue(ProductDetail.PRODUCT_DETAIL.INDUSTRIAL_TYPE_ID));
				productDetailResModel.setCategoryId(record.getValue(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID));
				productDetailResModel.setSubCategoryId(record.getValue(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID));
				productDetailResModel
						.setCategoryName(record.getValue(SubCategories.SUB_CATEGORIES.SUB_CATEGORY_NAME) != null
								? record.getValue(SubCategories.SUB_CATEGORIES.SUB_CATEGORY_NAME)
								: record.getValue(CategoriesMaster.CATEGORIES_MASTER.CAT_NAME));
				productDetailResModel
						.setProductName(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME).toUpperCase());
				productDetailResModel
						.setProductDescription(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION));
				productDetailResModel.setProductBrand(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND));
				productDetailResModel.setProductCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE));
				productDetailResModel
						.setProductStatusCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID));
				productDetailResModel.setSubCatName(record.getValue(SubCategories.SUB_CATEGORIES.SUB_CATEGORY_NAME));
				productDetailResModel.setRemark(record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS) != null
						? record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS).toUpperCase()
						: "");

				if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1) != null)
					productDetailResModel.setImagePath(
							ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1)));

				list.add(productDetailResModel);
			}
		}
		return list;
	}

	/**
	 * @param result
	 * @param imagePathMap
	 * @return
	 * @throws Exception
	 */
	public List<ProductDetailResModel> getRecentAddedProd(Result<Record> result) throws Exception {
		ProductDetailResModel productDetailResModel;
		List<ProductDetailResModel> list = new ArrayList<>();

		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				productDetailResModel = new ProductDetailResModel();
				String subCategory = record.getValue(SubCategories.SUB_CATEGORIES.SUB_CATEGORY_NAME);
				String finalSubCat = subCategory != null && !subCategory.isEmpty() ? subCategory
						: record.getValue(CategoriesMaster.CATEGORIES_MASTER.CAT_NAME);

				productDetailResModel.setProductId(record.getValue(ProductDetail.PRODUCT_DETAIL.ID));
				productDetailResModel.setIndustryId(record.getValue(ProductDetail.PRODUCT_DETAIL.INDUSTRIAL_TYPE_ID));
				productDetailResModel.setCategoryId(record.getValue(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID));
				productDetailResModel.setSubCategoryId(record.getValue(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID));
				productDetailResModel.setCategoryName(finalSubCat);
				productDetailResModel
						.setProductName(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME).toUpperCase());
				productDetailResModel
						.setProductDescription(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION));
				productDetailResModel.setProductBrand(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND));
				productDetailResModel.setProductCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE));
				productDetailResModel
						.setProductStatusCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID));
				productDetailResModel.setRemark(record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS) != null
						? record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS).toUpperCase()
						: "");

				if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1) != null)
					productDetailResModel.setImagePath(
							ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1)));

				/*
				 * String image = cmnMethod.getMapValue(imagePathMap,
				 * record.getValue(ProductDetail.PRODUCT_DETAIL.ID)); if(image != null)
				 * productDetailResModel.setImagePath(ConstantKeys.s3MainUrl.concat(image));
				 */

				list.add(productDetailResModel);
			}
		}
		return list;
	}

	/**
	 * @param industrialPlotList
	 * @param imagePathMap
	 * @return
	 * @throws Exception
	 */
	public List<ProductDetailResModel> getStockClearanceLeadsData(Result<?> industrialPlotList) throws Exception {
		ProductDetailResModel productDetailResModel;
		CommonMethods cmnMethod = new CommonMethods();
		List<ProductDetailResModel> list = new ArrayList<>();

		if (industrialPlotList != null && industrialPlotList.isNotEmpty()) {
			for (Record record : industrialPlotList) {
				productDetailResModel = new ProductDetailResModel();

				productDetailResModel.setProductId(record.getValue(ProductDetail.PRODUCT_DETAIL.ID));
				productDetailResModel.setCategoryName(record.getValue(CategoriesMaster.CATEGORIES_MASTER.CAT_NAME));
				productDetailResModel
						.setProductName(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME).toUpperCase());
				productDetailResModel
						.setProductDescription(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION));
				productDetailResModel.setProductBrand(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND));
				productDetailResModel.setProductCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE));
				productDetailResModel
						.setProductStatusCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID));
				productDetailResModel.setProductState(record.getValue(StateMaster.STATE_MASTER.STATE));
				productDetailResModel.setProductCity(record.getValue(CityMaster.CITY_MASTER.CITY));
				productDetailResModel.setRemark(record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS) != null
						? record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS).toUpperCase()
						: "");

				if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1) != null)
					productDetailResModel.setImagePath(
							ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1)));

				list.add(productDetailResModel);
			}
		}
		return list;
	}

	/**
	 * @param industrialPlotList
	 * @param imagePathMap
	 * @return
	 * @throws Exception
	 */
	public List<ProductDetailResModel> getSoldProductData(Result<?> industrialPlotList) throws Exception {
		ProductDetailResModel productDetailResModel;
		CommonMethods cmnMethod = new CommonMethods();
		List<ProductDetailResModel> list = new ArrayList<>();

		if (industrialPlotList != null && industrialPlotList.isNotEmpty()) {
			for (Record record : industrialPlotList) {
				productDetailResModel = new ProductDetailResModel();

				productDetailResModel.setProductId(record.getValue(ProductDetail.PRODUCT_DETAIL.ID));
				productDetailResModel.setCategoryName(record.getValue(CategoriesMaster.CATEGORIES_MASTER.CAT_NAME));
				productDetailResModel
						.setProductName(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME).toUpperCase());
				productDetailResModel
						.setProductDescription(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION));
				productDetailResModel.setProductBrand(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND));
				productDetailResModel.setProductCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE));
				productDetailResModel
						.setProductStatusCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID));
				productDetailResModel.setProductState(record.getValue(StateMaster.STATE_MASTER.STATE));
				productDetailResModel.setProductCity(record.getValue(CityMaster.CITY_MASTER.CITY));
				productDetailResModel.setRemark(record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS) != null
						? record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS).toUpperCase()
						: "");
				if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1) != null)
					productDetailResModel.setImagePath(
							ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1)));

				list.add(productDetailResModel);
			}
		}
		return list;
	}

	/**
	 * @param result
	 * @param imagePathMap
	 * @return
	 * @throws Exception
	 */
	public List<ProductDetailResModel> getIndustrialServicesData(Result<Record> result) throws Exception {
		ProductDetailResModel productDetailResModel;
		CommonMethods cmnMethod = new CommonMethods();
		List<ProductDetailResModel> list = new ArrayList<>();

		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				productDetailResModel = new ProductDetailResModel();

				productDetailResModel
						.setProductId(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.ID));
				productDetailResModel.setProductCode(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_PRODUCT_CODE));
				productDetailResModel.setProductName(record
						.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_TITLE).toUpperCase());
				productDetailResModel.setProductDescription(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_DESCRIPTION));
				productDetailResModel.setExpInMonth(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_MONTH));
				productDetailResModel.setExpInYear(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_YEAR));
				productDetailResModel.setIndustryId(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_CATERORY_ID));
				productDetailResModel
						.setCategoryName(record.getValue(IndustrialServices.INDUSTRIAL_SERVICES.SERVICE_CATERORY));
				productDetailResModel.setProductCountry(record.getValue(CountryMaster.COUNTRY_MASTER.NAME));
				productDetailResModel.setProductState(record.getValue(StateMaster.STATE_MASTER.STATE));
				productDetailResModel.setProductCity(record.getValue(CityMaster.CITY_MASTER.CITY));
				productDetailResModel.setImagePath(ConstantKeys.s3MainUrl
						.concat(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_1)));

				list.add(productDetailResModel);
			}
		}
		return list;
	}

	/**
	 * @param imageVideoList
	 * @param fetchSingleProductDetal
	 * @return
	 */
	public ProductDetailResModel getSingleProdDetails(Result<Record> result) throws Exception {
		ProductDetailResModel productDetailResModel = null;
		List<String> image = new ArrayList<>();
		List<String> video = new ArrayList<>();

		// Add Other Data to response model
		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				productDetailResModel = new ProductDetailResModel();
				productDetailResModel.setItemCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE));
				productDetailResModel.setProductName(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME));
				productDetailResModel
						.setProductSubDescription(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_SUB_DESC));
				productDetailResModel
						.setProductDescription(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION));
				productDetailResModel.setProductBrand(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND));
				productDetailResModel
						.setMfgPurchaseyear(record.getValue(ProductDetail.PRODUCT_DETAIL.MFG_PURCHASE_YEAR));
				productDetailResModel.setQuantity(record.getValue(ProductDetail.PRODUCT_DETAIL.QUANTITY));
				productDetailResModel.setUom(record.getValue(UomMaster.UOM_MASTER.ACRONYM));
				productDetailResModel
						.setPricePerUnit(record.getValue(ProductDetail.PRODUCT_DETAIL.PRICE_PER_UNIT).toString());

				productDetailResModel
						.setPriceType(new CommonMethods().boolToInt((boolean) record.getValue("price_negotiable")));

				productDetailResModel.setProductLoc(record.getValue(ProductDetail.PRODUCT_DETAIL.PROD_LOC));

				if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1) != null) {
					image.add(ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1)));
				}
				if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE2) != null) {
					image.add(ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE2)));
				}
				if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE3) != null) {
					image.add(ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE3)));
				}
				if (record.getValue(ProductDetail.PRODUCT_DETAIL.VIDEO_DOC) != null) {
					video.add(ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.VIDEO_DOC)));
				}
			}
			productDetailResModel.setImageList(image);
			productDetailResModel.setVideoList(video);
		}
		return productDetailResModel;
	}

	/**
	 * @param fetchSingleProductDetal of Industrial Services
	 * @return
	 */
	public ProductDetailResModel getIndustrialServiceSingleProdDetails(Result<Record> result) throws Exception {
		ProductDetailResModel productDetailResModel = null;
		List<String> image = new ArrayList<>();

		// Add Other Data to response model
		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				productDetailResModel = new ProductDetailResModel();
				productDetailResModel.setItemCode(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_PRODUCT_CODE));
				productDetailResModel.setProductName(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_TITLE));
				productDetailResModel.setProductSubDescription(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_DESCRIPTION));
				productDetailResModel.setProductDescription(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_DESCRIPTION));
				productDetailResModel.setProductLoc(record.getValue(CityMaster.CITY_MASTER.CITY));
				productDetailResModel.setServiceCatId(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_CATERORY_ID));
				productDetailResModel
						.setServiceCatName(record.getValue(IndustrialServices.INDUSTRIAL_SERVICES.SERVICE_CATERORY));
				productDetailResModel.setExpInMonth(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_MONTH));
				productDetailResModel.setExpInYear(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_YEAR));

				if (record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_1) != null)
					image.add(ConstantKeys.s3MainUrl
							.concat(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_1)));
				if (record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_2) != null)
					image.add(ConstantKeys.s3MainUrl
							.concat(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_2)));
				if (record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_3) != null)
					image.add(ConstantKeys.s3MainUrl
							.concat(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_3)));

			}
			productDetailResModel.setImageList(image);
		}
		return productDetailResModel;
	}

	/**
	 * @param fetchSingleProductDetal of Industrial Plot
	 * @return
	 */
	public ProductDetailResModel getIndustrialPlotSingleProdDetails(Result<Record> result) throws Exception {
		ProductDetailResModel productDetailResModel = null;
		List<String> image = new ArrayList<>();

		// Add Other Data to response model
		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				productDetailResModel = new ProductDetailResModel();
				productDetailResModel.setItemCode(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_CODE));
				productDetailResModel.setProductName(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_TITTLE));
				productDetailResModel
						.setProductSubDescription(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_DESCRIPTION));
				productDetailResModel
						.setProductDescription(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_DESCRIPTION));
				productDetailResModel.setProductLoc(record.getValue(CityMaster.CITY_MASTER.CITY));
				productDetailResModel.setCategoryName(record.getValue(IpProductList.IP_PRODUCT_LIST.MAIN_CATEGORY));
				productDetailResModel.setServiceCatName(record.getValue(IpTypeMaster.IP_TYPE_MASTER.IP_TYPE_NAME));
				productDetailResModel.setUom(record.getValue(UomMaster.UOM_MASTER.ACRONYM));
				productDetailResModel.setQuantity(record.getValue(IpProductList.IP_PRODUCT_LIST.QUANTITY));
				productDetailResModel
						.setPricePerUnit(record.getValue(IpProductList.IP_PRODUCT_LIST.RATE_PER_UNIT).toString());

				productDetailResModel.setPostedDate(record.getValue("poste_ddate").toString());

				if (record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_1) != null)
					image.add(ConstantKeys.s3MainUrl.concat(record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_1)));
				if (record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_2) != null)
					image.add(ConstantKeys.s3MainUrl.concat(record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_2)));
				if (record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_3) != null)
					image.add(ConstantKeys.s3MainUrl.concat(record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_3)));

			}
			productDetailResModel.setImageList(image);
		}
		return productDetailResModel;
	}

	/**
	 * @param result
	 * @return
	 */
	public SellerDetails getSellerDetails(Result<Record> result) throws Exception {
		SellerDetails sellerDetails = null;

		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				sellerDetails = new SellerDetails();

				sellerDetails.setSellerEmail(record.getValue(Users.USERS.EMAIL));
				sellerDetails.setSellerMobileNumber(record.getValue(Users.USERS.USER_MOBILE).replace("\n", ""));
				sellerDetails.setSellerFName(record.getValue(Users.USERS.FIRST_NAME).replace("\n", ""));
				sellerDetails.setSellerLName(record.getValue(Users.USERS.LAST_NAME));
				sellerDetails.setSellerCmpnyName(record.getValue(Users.USERS.COMPANY).replace("\n", ""));
				sellerDetails.setStateName(record.getValue(StateMaster.STATE_MASTER.STATE));
				sellerDetails.setCityName(record.getValue(CityMaster.CITY_MASTER.CITY));
				sellerDetails.setId((Integer) record.getValue("userId"));
			}
		}
		return sellerDetails;
	}

	/**
	 * @param fetchBuyersCornerRequirmnt
	 * @return
	 * @throws Exception
	 */
	public List<BuyerCornerRequirementModel> getBuyerCornerList(Result<Record> result) throws Exception {
		BuyerCornerRequirementModel buyerCornerRequirementModel = null;
		List<BuyerCornerRequirementModel> list = new ArrayList<>();
		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				buyerCornerRequirementModel = new BuyerCornerRequirementModel();

				buyerCornerRequirementModel.setProductName(
						record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.PRODUCT_NAME).toUpperCase());
				buyerCornerRequirementModel.setProductDescr(
						record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.PRODUCT_DESCRIPTION));
				buyerCornerRequirementModel
						.setProductSize(record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.PRODUCT_SIZE));
				buyerCornerRequirementModel.setProductBrand(
						record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.PRODUCT_BRAND));
				buyerCornerRequirementModel.setRequireQuantity(
						record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.REQUIRED_QUANTITY));
				buyerCornerRequirementModel
						.setCompanyName(record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.COMPANY_NAME));
				buyerCornerRequirementModel.setCompanyAddress(
						record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.COMPANY_ADDRESS));
				buyerCornerRequirementModel.setCompanyPhnNumber(
						record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.COMANY_PHONE_NO));
				buyerCornerRequirementModel
						.setPinCode(record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.PIN_CODE));
				buyerCornerRequirementModel
						.setCountryId(record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.COUNTRY_ID));
				buyerCornerRequirementModel.setCountryName(record.getValue(CountryMaster.COUNTRY_MASTER.NAME));
				buyerCornerRequirementModel
						.setStateId(record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.STATE_ID));
				buyerCornerRequirementModel.setStateName(record.getValue(StateMaster.STATE_MASTER.STATE));
				buyerCornerRequirementModel
						.setCityId(record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.CITY_ID));
				buyerCornerRequirementModel.setCityName(record.getValue(CityMaster.CITY_MASTER.CITY));
				buyerCornerRequirementModel.setContactPerson(
						record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.CONTACT_PERSON));
				buyerCornerRequirementModel
						.setMobileNumber(record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.MOBILE_NO));
				buyerCornerRequirementModel
						.setEmailId(record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.EMAIL_ID));
				buyerCornerRequirementModel.setUomType(record.getValue(UomMaster.UOM_MASTER.ACRONYM));
				buyerCornerRequirementModel
						.setProductcode(record.getValue(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.PROD_CODE));

				list.add(buyerCornerRequirementModel);
			}
		}
		return list;
	}

	/**
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public List<ProductDetailResModel> getSubCatList(Result<?> result) throws Exception {
		ProductDetailResModel productDetailResModel;
		List<ProductDetailResModel> list = new ArrayList<>();

		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				productDetailResModel = new ProductDetailResModel();

				productDetailResModel.setProductId(record.getValue(ProductDetail.PRODUCT_DETAIL.ID));
				productDetailResModel.setProductName(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME));
				productDetailResModel
						.setProductDescription(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION));
				productDetailResModel.setProductBrand(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND));
				productDetailResModel.setProductCountry(record.getValue(CountryMaster.COUNTRY_MASTER.NAME));
				productDetailResModel.setProductState(record.getValue(StateMaster.STATE_MASTER.STATE));
				productDetailResModel.setProductCity(record.getValue(CityMaster.CITY_MASTER.CITY));
				productDetailResModel.setCategoryName(record.getValue(CategoriesMaster.CATEGORIES_MASTER.CAT_NAME));
				productDetailResModel.setProductCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE));
				productDetailResModel
						.setProductStatusCode(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID));
				productDetailResModel.setSubCatName(record.getValue(SubCategories.SUB_CATEGORIES.SUB_CATEGORY_NAME));
				productDetailResModel.setSubCategoryId(record.getValue(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID));

				if (record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS) != null)
					productDetailResModel.setRemark(record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS) != null
							? record.getValue(ProductDetail.PRODUCT_DETAIL.REMARKS).toUpperCase()
							: "");

				if (record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1) != null)
					productDetailResModel.setImagePath(
							ConstantKeys.s3MainUrl.concat(record.getValue(ProductDetail.PRODUCT_DETAIL.IMAGE1)));

				list.add(productDetailResModel);
			}
		}
		return list;
	}

	/**
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public List<InventoryResponse> getInventoryDetails(Result<Record> result) throws Exception {
		InventoryResponse inventoryList;
		List<InventoryResponse> list = new ArrayList<>();
		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				inventoryList = new InventoryResponse();
				if(record.getValue("amount")!=null) {
					inventoryList.setInventoryName(record.getValue("categorytype").toString());
					inventoryList.setInventoryId(record.getValue("id").toString());
					inventoryList.setAmount(record.getValue("amount")!=null?record.getValue("amount").toString():"0");

					list.add(inventoryList);
				}
			}
		}
		
		return list;
	}

	/**
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public List<InventoryResponse> getInventoryList(Result<Record> result) throws Exception {
		InventoryResponse inventoryList;
		List<InventoryResponse> list = new ArrayList<>();
		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				inventoryList = new InventoryResponse();
				inventoryList.setInventoryName(record.getValue("categorytype").toString());
				inventoryList.setInventoryId(record.getValue("id").toString());
				inventoryList.setAmount(record.getValue("amount").toString());
				inventoryList.setPercentage(record.getValue("percent").toString());
				inventoryList.setIgst(record.getValue("igstval").toString());
				inventoryList.setCgst(record.getValue("cgstval").toString());
				inventoryList.setSgst(record.getValue("sgstval").toString());

				list.add(inventoryList);
			}
		}

		return list;
	}

	/**
	 * @param tphTotalChargeAmount
	 * @param cgst
	 * @param sgst
	 * @param tphTotalChargeAmount2
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public InventoryResponse getInventoryTotalPaybleAmount(double sgst, double cgst, double tphTotalChargeAmount,
			double igst, double amount) throws Exception {
		InventoryResponse inventoryList = new InventoryResponse();

		inventoryList.setPaybleAmount(tphTotalChargeAmount);
		inventoryList.setAmount(String.valueOf(amount));
		if (sgst != 0 && sgst != 0.0)
			inventoryList.setSgst(String.valueOf(sgst));
		if (cgst != 0 && cgst != 0.0)
			inventoryList.setCgst(String.valueOf(cgst));
		if (igst != 0 && igst != 0.0)
			inventoryList.setIgst(String.valueOf(igst));

		return inventoryList;
	}

	/**
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public List<SellerAttributesResponse> getAllAttrib(Result<Record> result) throws Exception {
		SellerAttributesResponse sellerAttributesResponse;
		List<SellerAttributesResponse> list = new ArrayList<>();
		if (result != null && result.isNotEmpty()) {
			for (Record record : result) {
				sellerAttributesResponse = new SellerAttributesResponse();

				sellerAttributesResponse.setAttribName(record.getValue("attribute_name").toString());
				if (record.getValue("attrib_alias") != null)
					sellerAttributesResponse.setAttriAlias(record.getValue("attrib_alias").toString());

				list.add(sellerAttributesResponse);
			}
		}
		return list;
	}

	/**
	 * @param imagePathMap
	 * @param imagePathRecord
	 * @return
	 */
	public Map<Integer, String> generateImageMap(Result<Record2<Integer, String>> imagePathRecord) throws Exception {
		Map<Integer, String> imagePathMap = new HashMap<>();
		if (imagePathRecord != null && imagePathRecord.isNotEmpty()) {
			for (Record2<Integer, String> record2 : imagePathRecord) {
				imagePathMap.put(record2.getValue(ProductImageVideo.PRODUCT_IMAGE_VIDEO.PRODUCT_ID),
						record2.getValue(ProductImageVideo.PRODUCT_IMAGE_VIDEO.IMAGE_VEDIO_PATH));
			}
		}

		return imagePathMap;
	}

	/**
	 * @param industrialPlotList
	 * @param imagePathMap
	 * @return
	 */
	public List<IndusPlotResponseModel> getIndustrialPlot(Result<?> industrialPlotList) throws Exception {
		IndusPlotResponseModel indusPlotResponseModel;
		List<IndusPlotResponseModel> list = new ArrayList<>();

		if (industrialPlotList != null && industrialPlotList.isNotEmpty()) {
			for (Record record : industrialPlotList) {
				indusPlotResponseModel = new IndusPlotResponseModel();

				indusPlotResponseModel.setProductId(record.getValue(IpProductList.IP_PRODUCT_LIST.ID));
				indusPlotResponseModel
						.setIpTitle(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_TITTLE).toUpperCase());
				indusPlotResponseModel.setUserId(record.getValue(IpProductList.IP_PRODUCT_LIST.USER_ID));
				indusPlotResponseModel.setProductCode(record.getValue(IpProductList.IP_PRODUCT_LIST.IP_CODE));
				indusPlotResponseModel.setIpType(record.getValue(IpTypeMaster.IP_TYPE_MASTER.IP_TYPE_NAME));
				indusPlotResponseModel.setProductCountry(record.getValue(CountryMaster.COUNTRY_MASTER.NAME));
				indusPlotResponseModel.setProductState(record.getValue(StateMaster.STATE_MASTER.STATE));
				indusPlotResponseModel.setProductCity(record.getValue(CityMaster.CITY_MASTER.CITY));
				indusPlotResponseModel.setRemark(record.getValue(IpProductList.IP_PRODUCT_LIST.REMARK) != null
						? record.getValue(IpProductList.IP_PRODUCT_LIST.REMARK).toUpperCase()
						: "");

				if (record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_1) != null)
					indusPlotResponseModel.setImage(
							ConstantKeys.s3MainUrl.concat(record.getValue(IpProductList.IP_PRODUCT_LIST.IMAGE_1)));

				list.add(indusPlotResponseModel);
			}
		}

		return list;
	}

	/**
	 * @param industrial   Product Service
	 * @param imagePathMap
	 * @return
	 */
	public List<IndusProductServiceResModel> getIndustrialProdService(Result<?> industrialPlotList) throws Exception {
		IndusProductServiceResModel indusProductServiceResModel;
		List<IndusProductServiceResModel> list = new ArrayList<>();

		if (industrialPlotList != null && industrialPlotList.isNotEmpty()) {
			for (Record record : industrialPlotList) {
				indusProductServiceResModel = new IndusProductServiceResModel();

				indusProductServiceResModel
						.setProductId(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.ID));
				indusProductServiceResModel.setIpTitle(record
						.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_TITLE).toUpperCase());
				indusProductServiceResModel
						.setUserId(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.USER_ID));
				indusProductServiceResModel.setProductCode(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_PRODUCT_CODE));
				indusProductServiceResModel.setExpInMonth(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_MONTH));
				indusProductServiceResModel.setExpInYear(
						record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_YEAR));
				indusProductServiceResModel.setProductCity(record.getValue(CityMaster.CITY_MASTER.CITY));

				if (record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_1) != null) {
					indusProductServiceResModel.setImage(ConstantKeys.s3MainUrl
							.concat(record.getValue(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_1)));
				}

				list.add(indusProductServiceResModel);
			}
		}

		return list;
	}

	/**
	 * @param industrial   Service name
	 * @param imagePathMap
	 * @return
	 */
	public List<ProductDetailResModel> getIndustrialServicename(Result<?> industrialServiceList) throws Exception {
		ProductDetailResModel productDetailResModel;
		List<ProductDetailResModel> list = new ArrayList<>();

		if (industrialServiceList != null && industrialServiceList.isNotEmpty()) {
			for (Record record : industrialServiceList) {
				productDetailResModel = new ProductDetailResModel();

				productDetailResModel.setCategoryId(record.getValue(IndustrialServices.INDUSTRIAL_SERVICES.ID));
				productDetailResModel
						.setCategoryName(record.getValue(IndustrialServices.INDUSTRIAL_SERVICES.SERVICE_CATERORY));

				list.add(productDetailResModel);
			}
		}

		return list;
	}

	/**
	 * @param industrial   Product Service
	 * @param imagePathMap
	 * @return
	 */
	public List<ProductDetailResModel> getStockClearanceLeadList(Result<?> industrialPlotList) throws Exception {
		ProductDetailResModel productDetailResModel;
		List<ProductDetailResModel> list = new ArrayList<>();

		if (industrialPlotList != null && industrialPlotList.isNotEmpty()) {
			for (Record record : industrialPlotList) {
				productDetailResModel = new ProductDetailResModel();

				productDetailResModel.setProductId(record.getValue(ProductDetail.PRODUCT_DETAIL.ID));
				productDetailResModel.setProductName(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME));
				productDetailResModel
						.setProductDescription(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION));
				productDetailResModel.setProductBrand(record.getValue(ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND));
				productDetailResModel.setCategoryName(record.getValue(CategoriesMaster.CATEGORIES_MASTER.CAT_NAME));

				list.add(productDetailResModel);
			}
		}

		return list;
	}

	/**
	 * Get Login Res from record..
	 * 
	 * @param record
	 * @return
	 */
	public LoginResponseModel getLoginRes(UsersRecord record) throws Exception {
		LoginResponseModel loginResponseModel = new LoginResponseModel();
		loginResponseModel.setUserId(record.getValue(Users.USERS.ID));
		loginResponseModel.setFirstName(record.getValue(Users.USERS.FIRST_NAME));
		loginResponseModel.setLastName(record.getValue(Users.USERS.LAST_NAME));
		loginResponseModel.setLastLogin(record.getValue(Users.USERS.LAST_LOGIN).toString());

		return loginResponseModel;
	}

	/**
	 * @param record
	 * @return
	 * @throws Exception
	 */
	public List<NotifyResponse> getNotifyResponse(Result<Record2<String, String>> record) throws Exception {

		List<NotifyResponse> list = new ArrayList<>();
		NotifyResponse notifyResponse = null;

		if (record.size() > 0) {
			for (Record2<String, String> record2 : record) {
				notifyResponse = new NotifyResponse();

				notifyResponse
						.setBody(record2.getValue(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.COMM_CONTENT));
				notifyResponse.setName(record2.getValue(Users.USERS.FIRST_NAME));
				list.add(notifyResponse);
			}
		}

		return list;
	}
}
