/**
 * 
 */
package com.tph.request.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class CommonRequestModel extends SellerBaseRequestModel{
	private String productName;
	private String serviceType;
	private Integer subCategoryId;
}
