package com.tph.request.model;

import lombok.Data;

@Data
public class LoginRequestModel {
	private String userName;
	private String password;
}
