package com.tph.request.model;

import lombok.Data;

@Data
public class PasswordRequest {
	private Integer userId;
	private String currentPw;
	private String newPw;
	private String action;
}
