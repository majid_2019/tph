/**
 * 
 */
package com.tph.request.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class RegistraionRequestModel extends SellerBaseRequestModel{
	private String companyName;
	private String contactPerson;
	private String gstNumber;
	private String mobileNumber;
	private String email;
	private String address;
	private String password;
	private String confirmPassword;
	private String ipAddress;
	private String lastLogin;
	private Boolean isSeller;
	private Boolean isBuyer;
	private String userName;
	private Boolean isUpdate;
	private Integer pinCode;
}
