/**
 * 
 */
package com.tph.request.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class SmsRequestModel{
	private Long mobileNumber;
	private String buyerNumber;
	private String productCode;
	private String productTitle;
	private String smsBody;
	private String countryName;
	private String serviceType;
	private Integer userId;
	private Integer otp;
	private Integer countryId;
	private Boolean isSeller;
	private Boolean isBuyer;
	private String buyerName;
	private String templateId;
	private String commCategory;
	private String fromAddress;
	private Integer toUserId;
	private Integer buyerUserID;
	private Integer sellerUserId;
	private String sellerName;
	private String sellerMoble;
	private String offerPrice;
	private String quantity;
	private String uom;
	private Boolean isContactSeller;
	private String isMockSellerSms;
}
