package com.tph.request.model;

import lombok.Data;

@Data
public class ProdViewCountRequest {
	private Integer userId;
	private String productCode;
	private Integer productId;
}
