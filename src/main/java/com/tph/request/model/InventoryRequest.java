package com.tph.request.model;

import lombok.Data;

@Data
public class InventoryRequest {
	private Integer userId;
	private String inventoryName;
	private Integer inventoryId;
	private Double amount;

	private Double percentage;
	private Integer igst;
	private Integer cgst;
	private Integer sgst;
	private Double chargeAmount;
}
