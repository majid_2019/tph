package com.tph.request.model;

import lombok.Data;

@Data
public class NotificationRequest {
	private String notificationType;
	private Long buyerMobile;
	private Long sellerMobile;
	private String buyerName;
	private Long buyerId;
	private Double offerPrice;
	private Double quantity;
	private String productCode;
	private String productTitle;
	private String uom;
	private Long sellerId;
	private String notificationBody;
	private String templateId; 
	private String commType;
}
