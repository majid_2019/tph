package com.tph.request.model;

import java.util.List;

import com.tph.response.model.PhotoVideoPath;

import lombok.Data;

@Data
public class EditPostResponse extends SellerBaseRequestModel {
	private Integer productId;
	private String productTitle;
	private String productDetails;
	private String purchaseYear;
	private String brand;
	private String uomName;
	private String uomAcronym;
	private Integer availableQuantity;
	private String stateName;
	private Double pricePerUnit;
	private int priceType;
	private String productLocation;
	private String productCity;
	private String productStatus;
	private String productCode;
	private Integer productStatusId;
	private String categoryName;
	private String ipNegotiable;
	private String resellerName;
	private String contactPerson;
	private String mobileNumber;
	private String alterMobileNumber;
	private Integer expInMonth;
	private Integer expInYear;
	private String productName;
	private String subsDescryption;
	private String descryption;
	private String availableYear;
	private String countryName;
	
	private String remark;
	private String image1;
	private String image2;
	private String image3;
	private String serviceType;
	private Boolean isPostEdit;
	private String actionType;
	
	private List<String> imageList;
	private List<String> videoList;
	
	private List<PhotoVideoPath> photoVideoPathList;
}
