/**
 * 
 */
package com.tph.request.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class SellerBaseRequestModel {
	private Integer id;
	private Integer userId;
	private Integer stateId;
	private Integer cityId;
	private Integer uomId;
	private Integer countryId;
	private Integer industrialTypeId;
	private Integer catTypeId;
	private Integer subCatTypeId;
	private Integer productId;
	private String serviceType;
	private Integer ipTypeID; //IndustrialProduct Type Id
	private Integer serviceCatId; //industrial_product_services Type Id
}
