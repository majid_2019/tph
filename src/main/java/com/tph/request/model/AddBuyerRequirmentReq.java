/**
 * 
 */
package com.tph.request.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class AddBuyerRequirmentReq extends SellerBaseRequestModel{
	private String productDescription;
	private Integer requiredQuantity;
	private Integer uomName;
	private String brandName;
	private String productTitle;
}
