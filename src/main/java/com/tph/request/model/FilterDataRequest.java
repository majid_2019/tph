package com.tph.request.model;

import lombok.Data;

@Data
public class FilterDataRequest {
	private Integer cityId;
	private String brand;
	private Integer stateId;
	private Integer typeId;
	private Integer countryId;
	private String industiesType;
	private Integer rentSellId;
	private String indServiceCategory;
	
	// for the Invetnory list
	private Integer industrialTypeId;
	private Integer catTypeId;
	private Integer subCatTypeId;
}
