package com.tph.request.model;

import lombok.Data;

@Data
public class MakeAnOfferRquest {
	private Integer productId;
	private Integer sellerId;
	private Integer buyerId;
	private Double availableQuantity;
	private Double requiredQuantity;
	private Double offerPrice;
}
