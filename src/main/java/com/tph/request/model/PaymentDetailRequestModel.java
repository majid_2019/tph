/**
 * 
 */
package com.tph.request.model;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class PaymentDetailRequestModel {
	private Integer productId;
	private String payType;
	private String accountNumber;
	private String checkNumber;
	private String receiptNumber;
	private String ifsc;
	private String transactionId;
	private Integer sellerID;
	private Integer buyerId;
	private BigDecimal dashboardAmount;
	private BigDecimal tphBaseAmount;
	private BigDecimal tphTotalAmount;
	private BigDecimal cgst;
	private BigDecimal sgst;
	private BigDecimal igst;
	private Boolean isPaymentSuccess;
	private String paymentStatus;
}

