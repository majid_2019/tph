package com.tph.request.model;

import lombok.Data;

@Data
public class FinalOtpRequest {
	private Long mobileNumber;
	private String smsBody;
}
