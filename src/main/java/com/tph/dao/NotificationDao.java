package com.tph.dao;

import org.jooq.DSLContext;
import org.jooq.Record;

import com.tph.constants.CommunicationCategory;
import com.tph.jooq.tph_db.tables.CentralCommunicationTemplate;
import com.tph.jooq.tph_db.tables.records.CentralCommunicationLogRecord;
import com.tph.request.model.NotificationRequest;

public class NotificationDao {
	/**
	 * @param dslContext
	 * @return
	 */
	public Record getMkNOfferNotificationTemplete(DSLContext dslContext, String category) throws Exception {
		return dslContext
				.select(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT,
						CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.ID,
						CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_TYPE)
				.from(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE)
				.where(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_CATEGORY.eq(category))
				.and(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.IS_ACTIVE.eq("Y")).fetchOne();
	}
	
	/**
	 * @param dslContext
	 * @param requestModel
	 */
	public void insertNotificationMKNOfferIntoTable(DSLContext dslContext, NotificationRequest requestModel, String category) throws Exception{
		
		CentralCommunicationLogRecord centralCommunicationLogRecord = new CentralCommunicationLogRecord();
		
		centralCommunicationLogRecord.setTemplateId(requestModel.getTemplateId());
		centralCommunicationLogRecord.setFromAddress(requestModel.getBuyerMobile().toString());
		centralCommunicationLogRecord.setFromUserId(requestModel.getBuyerId().intValue());
		centralCommunicationLogRecord.setToUserId(requestModel.getSellerId().intValue());
		centralCommunicationLogRecord.setToAddress(requestModel.getSellerMobile()!=null?String.valueOf(requestModel.getSellerMobile()):null);
		centralCommunicationLogRecord.setCommCategory(category);
		centralCommunicationLogRecord.setCommContent(requestModel.getNotificationBody());
		centralCommunicationLogRecord.setCommType(requestModel.getCommType());
		Integer insert = dslContext.executeInsert(centralCommunicationLogRecord);
		System.out.println("Notification Log inserted -->> "+insert);
	}
}
