package com.tph.dao;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.Result;

import com.tph.constants.CommunicationCategory;
import com.tph.jooq.tph_db.tables.CentralCommunicationLog;
import com.tph.jooq.tph_db.tables.CentralCommunicationTemplate;
import com.tph.jooq.tph_db.tables.OtpLogDetail;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.CentralCommunicationLogRecord;
import com.tph.jooq.tph_db.tables.records.OtpLogDetailRecord;
import com.tph.request.model.SmsRequestModel;

public class SmsServiceDao {
	/**
	 * @param dslContext
	 * @param mobileNumber
	 * @param isSeller
	 * @return 
	 * @throws Exception
	 */
	public Result<Record> checkBuyerExistOrNot(DSLContext dslContext,SmsRequestModel smsRequestModel) throws Exception{
		String isBuyer = "N";
		if (smsRequestModel.getIsBuyer()) {
			isBuyer = "Y";
		}
		return dslContext.select().from(Users.USERS).where(Users.USERS.USER_MOBILE.eq(String.valueOf(smsRequestModel.getMobileNumber())))
		.and(Users.USERS.IS_BYERS.eq(isBuyer))
		.and(Users.USERS.ACTIVE.eq("Y"))
		.fetch();
	}
	
	/**
	 * @param dslContext
	 * @return 
	 */
	public Record verifyOtp(DSLContext dslContext, SmsRequestModel smsRequestModel) throws Exception{
		
	return	(Record) dslContext.select()
		.from(OtpLogDetail.OTP_LOG_DETAIL)
		.where(OtpLogDetail.OTP_LOG_DETAIL.MDN.eq(smsRequestModel.getMobileNumber()))
		.and(OtpLogDetail.OTP_LOG_DETAIL.OTP_CODE.eq(smsRequestModel.getOtp()))
		.and(OtpLogDetail.OTP_LOG_DETAIL.TYPE.eq("OTP SMS"))
		.orderBy(OtpLogDetail.OTP_LOG_DETAIL.CREATED_ON.desc())
		.fetchAny();
	}
	
	/**
	 * @param dslContext
	 * @return 
	 */
	public Record getOtpTemplete(DSLContext dslContext) throws Exception{
	return	(Record) dslContext.select(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT)
		.from(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE)
		.where(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_CATEGORY.eq(CommunicationCategory.valueOf(CommunicationCategory.OTP_SMS.name()).getCommunicationCategory()))
		.and(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.IS_ACTIVE.eq("Y"))
		.fetchOne();
	}
	
	/**
	 * @param dslContext
	 * @param requestModel
	 */
	public void insertOtpIntoTable(DSLContext dslContext, SmsRequestModel requestModel) {
		OtpLogDetailRecord otpLogDetailRecord = new OtpLogDetailRecord();
		
		otpLogDetailRecord.setUserId(requestModel.getUserId());
		otpLogDetailRecord.setMdn(Long.valueOf(requestModel.getMobileNumber()));
		otpLogDetailRecord.setOtpCode(requestModel.getOtp());
		otpLogDetailRecord.setType(requestModel.getServiceType());
		
		dslContext.executeInsert(otpLogDetailRecord);
	}
	
	/**
	 * @param dslContext
	 * @return 
	 */
	public Result<Record> getContactSellerTemplete(DSLContext dslContext) throws Exception{
		String category =CommunicationCategory.valueOf(CommunicationCategory.CONTACT_SELLER.name()).getCommunicationCategory();
		
		String query = "select a.id, a.communication_body_content from tph_db.central_communication_template a where communication_category = 'Contact seller '";
		Result<Record> result = dslContext.fetch(query);
		
		/*Result<Record> result = dslContext.select(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT)
				.from(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE)
				.where(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_CATEGORY.eq(category))
				.and(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.IS_ACTIVE.eq("Y"))
				.fetchOne();*/
		
		return	result;
		
	}
	
	/**
	 * @param dslContext
	 * @return 
	 */
	public Result<Record> getMknAnOfferTemplete(DSLContext dslContext) throws Exception{
		String query = "select a.id, a.communication_body_content from tph_db.central_communication_template a where communication_category = 'Make an offer'";
		Result<Record> result = dslContext.fetch(query);
		
		/*Result<Record> result = dslContext.select(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT)
				.from(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE)
				.where(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_CATEGORY.eq(category))
				.and(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.IS_ACTIVE.eq("Y"))
				.fetchOne();*/
		
		return	result;
		
	}
	
	/**
	 * @param dslContext
	 * @return 
	 */
	public Result<Record> getContactSellerTrackingTemplete(DSLContext dslContext) throws Exception{
		String query = "select a.id, a.communication_body_content from tph_db.central_communication_template a where communication_category = 'Contact Seller Tracking SMS'";
		Result<Record> result = dslContext.fetch(query);
		
		return	result;
		
	}
	
	/**
	 * @param dslContext
	 * @return 
	 */
	public Result<Record> getMknAnOfferTrackingTemplete(DSLContext dslContext) throws Exception{
		String query = "select a.id, a.communication_body_content from tph_db.central_communication_template a where communication_category = 'Make an offer Tracking SMS'";
		Result<Record> result = dslContext.fetch(query);
		
		return	result;
		
	}
	
	public Result<Record> getWelcomeTemplete(DSLContext dslContext) throws Exception{
		String query = "select a.id, a.communication_body_content from tph_db.central_communication_template a where communication_category = 'Welcome SMS'";
		Result<Record> result = dslContext.fetch(query);
		
		return	result;
		
	}

	/**
	 * @param dslContext
	 * @param requestModel
	 */
	public void insertContactSellerSMSIntoTable(DSLContext dslContext, SmsRequestModel requestModel) throws Exception{
		
		CentralCommunicationLogRecord centralCommunicationLogRecord = new CentralCommunicationLogRecord();
		
		centralCommunicationLogRecord.setTemplateId(requestModel.getTemplateId());
		centralCommunicationLogRecord.setFromAddress(requestModel.getFromAddress());
		centralCommunicationLogRecord.setFromUserId(requestModel.getBuyerUserID());
		centralCommunicationLogRecord.setToUserId(requestModel.getSellerUserId());
		centralCommunicationLogRecord.setToAddress(String.valueOf(requestModel.getMobileNumber()));
		centralCommunicationLogRecord.setCommCategory(CommunicationCategory.valueOf(CommunicationCategory.CONTACT_SELLER.name()).getCommunicationCategory());
		centralCommunicationLogRecord.setCommContent(requestModel.getSmsBody());
		centralCommunicationLogRecord.setCommType("SMS");
		dslContext.executeInsert(centralCommunicationLogRecord);
	}
	
	/**
	 * @param dslContext
	 * @param requestModel
	 */
	public void insertMknOfferSMSIntoTable(DSLContext dslContext, SmsRequestModel requestModel) throws Exception{
		
		CentralCommunicationLogRecord centralCommunicationLogRecord = new CentralCommunicationLogRecord();
		
		centralCommunicationLogRecord.setTemplateId(requestModel.getTemplateId());
		centralCommunicationLogRecord.setFromAddress(requestModel.getFromAddress());
		centralCommunicationLogRecord.setFromUserId(requestModel.getBuyerUserID());
		centralCommunicationLogRecord.setToUserId(requestModel.getSellerUserId());
		centralCommunicationLogRecord.setToAddress(String.valueOf(requestModel.getMobileNumber()));
		centralCommunicationLogRecord.setCommCategory(CommunicationCategory.valueOf(CommunicationCategory.MAKE_AN_OFFER.name()).getCommunicationCategory());
		centralCommunicationLogRecord.setCommContent(requestModel.getSmsBody());
		centralCommunicationLogRecord.setCommType("SMS");
		dslContext.executeInsert(centralCommunicationLogRecord);
	}
	
	/**
	 * @param dslContext
	 * @param requestModel
	 */
	public void insertTrackingSMSIntoTable(DSLContext dslContext, SmsRequestModel requestModel) throws Exception{
		
		CentralCommunicationLogRecord centralCommunicationLogRecord = new CentralCommunicationLogRecord();
		
		centralCommunicationLogRecord.setTemplateId(requestModel.getTemplateId());
		centralCommunicationLogRecord.setFromAddress(requestModel.getFromAddress());
		centralCommunicationLogRecord.setFromUserId(requestModel.getBuyerUserID());
		centralCommunicationLogRecord.setToUserId(requestModel.getSellerUserId());
		centralCommunicationLogRecord.setToAddress(String.valueOf(requestModel.getMobileNumber()));
		centralCommunicationLogRecord.setCommCategory(requestModel.getIsContactSeller()?CommunicationCategory.valueOf(CommunicationCategory.CONTACT_SELLER_TRACKING_SMS.name()).getCommunicationCategory():CommunicationCategory.valueOf(CommunicationCategory.MAKE_AN_OFFER_TRACKING_SMS.name()).getCommunicationCategory());
		centralCommunicationLogRecord.setCommContent(requestModel.getSmsBody());
		centralCommunicationLogRecord.setCommType("SMS");
		dslContext.executeInsert(centralCommunicationLogRecord);
	}
	
public void insertWelcomeSMSIntoTable(DSLContext dslContext, Integer returnId, SmsRequestModel requestModel) throws Exception{
		
		CentralCommunicationLogRecord centralCommunicationLogRecord = new CentralCommunicationLogRecord();
		
		centralCommunicationLogRecord.setTemplateId(requestModel.getTemplateId());
		centralCommunicationLogRecord.setFromUserId(returnId);
		centralCommunicationLogRecord.setToAddress(String.valueOf(requestModel.getMobileNumber()));
		centralCommunicationLogRecord.setCommCategory(CommunicationCategory.valueOf(CommunicationCategory.WELCOME_SMS.name()).getCommunicationCategory());
		centralCommunicationLogRecord.setCommContent(requestModel.getSmsBody());
		centralCommunicationLogRecord.setCommType("SMS");
		dslContext.executeInsert(centralCommunicationLogRecord);
	}
}
