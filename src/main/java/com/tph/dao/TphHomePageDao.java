package com.tph.dao;

import java.math.BigDecimal;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;

import com.tph.commonmethods.CommonMethods;
import com.tph.jooq.tph_db.tables.BuyerCornerRequirement;
import com.tph.jooq.tph_db.tables.CategoriesMaster;
import com.tph.jooq.tph_db.tables.CityMaster;
import com.tph.jooq.tph_db.tables.CountryMaster;
import com.tph.jooq.tph_db.tables.IndustrialProductServices;
import com.tph.jooq.tph_db.tables.IndustrialServices;
import com.tph.jooq.tph_db.tables.IpProductList;
import com.tph.jooq.tph_db.tables.IpTypeMaster;
import com.tph.jooq.tph_db.tables.ProductDetail;
import com.tph.jooq.tph_db.tables.ProductLikeShare;
import com.tph.jooq.tph_db.tables.StateMaster;
import com.tph.jooq.tph_db.tables.SubCategories;
import com.tph.jooq.tph_db.tables.UomMaster;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.BuyerCornerRequirementRecord;
import com.tph.jooq.tph_db.tables.records.MakeAnOfferRecord;
import com.tph.jooq.tph_db.tables.records.ProductLikeShareRecord;
import com.tph.request.model.AddBuyerRequirmentReq;
import com.tph.request.model.MakeAnOfferRquest;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.request.model.SellerBaseRequestModel;

public class TphHomePageDao {
	/**
	 * Get All Featured product List as per paid
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<Record> featuredProductList(DSLContext dslContext) throws Exception {

		Result<Record> result = dslContext.select().from(ProductDetail.PRODUCT_DETAIL)
				.join(CategoriesMaster.CATEGORIES_MASTER)
				.on(CategoriesMaster.CATEGORIES_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID))
				.join(SubCategories.SUB_CATEGORIES)
				.on(SubCategories.SUB_CATEGORIES.ID.eq(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID))
				.where(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y"))
				.and(ProductDetail.PRODUCT_DETAIL.IS_FEATURED.eq("Y"))
				.orderBy(ProductDetail.PRODUCT_DETAIL.CREATED_DATE.desc()).limit(12).fetch();

		return result;
	}
	
	/**
	 * Get All recent added product List.
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<Record> recentAddedProductList(DSLContext dslContext) throws Exception {
		
		Result<Record> result = dslContext.select().from(ProductDetail.PRODUCT_DETAIL)
				.join(CategoriesMaster.CATEGORIES_MASTER)
				.on(CategoriesMaster.CATEGORIES_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID))
				.join(SubCategories.SUB_CATEGORIES)
				.on(SubCategories.SUB_CATEGORIES.ID.eq(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID))
				.where(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y"))
				.orderBy(ProductDetail.PRODUCT_DETAIL.CREATED_DATE.desc()).limit(12).fetch();

		return result;
	}
	
	/**
	 * Get All recent added product List.
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<Record> stockClearanceLeadsList(DSLContext dslContext) throws Exception {

		Result<Record> result = dslContext.select().from(ProductDetail.PRODUCT_DETAIL)
				.leftJoin(CategoriesMaster.CATEGORIES_MASTER)
				.on(CategoriesMaster.CATEGORIES_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID))
				.leftJoin(StateMaster.STATE_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.STATE_ID.eq(StateMaster.STATE_MASTER.ID))
				.leftJoin(CityMaster.CITY_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.CITY_ID.eq(CityMaster.CITY_MASTER.ID))
				.where(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y"))
				.and(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID.eq(0))
				.and(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE.contains("TPHSC"))
				.orderBy(ProductDetail.PRODUCT_DETAIL.CREATED_DATE.desc()).limit(12).fetch();

		return result;
	}
	
	/**
	 * Get All recent added product List.
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<Record> industrialServicesList(DSLContext dslContext) throws Exception {

		Result<Record> result = dslContext.select().from(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES)
				.leftJoin(IndustrialServices.INDUSTRIAL_SERVICES).on(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_CATERORY_ID.eq(IndustrialServices.INDUSTRIAL_SERVICES.ID))
				.leftJoin(CountryMaster.COUNTRY_MASTER).on(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.COUNTRY_ID.eq(CountryMaster.COUNTRY_MASTER.ID))
				.leftJoin(StateMaster.STATE_MASTER).on(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.STATE_ID.eq(StateMaster.STATE_MASTER.ID))
				.leftJoin(CityMaster.CITY_MASTER).on(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.CITY_ID.eq(CityMaster.CITY_MASTER.ID))
				.where(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IS_ACTIVE.eq("Y"))
				.orderBy(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.CREATED_DATE.desc()).limit(12).fetch();
						
		return result;
	}
	
	/**
	 * Get Recently Added IndustrialPlot product List..
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<?> recentlyAddedIndustrialPlotList(DSLContext dslContext) {
		return dslContext.select(IpProductList.IP_PRODUCT_LIST.ID, IpProductList.IP_PRODUCT_LIST.IP_TITTLE, IpProductList.IP_PRODUCT_LIST.USER_ID,
				IpProductList.IP_PRODUCT_LIST.IP_CODE,IpProductList.IP_PRODUCT_LIST.IMAGE_1, IpProductList.IP_PRODUCT_LIST.REMARK, IpTypeMaster.IP_TYPE_MASTER.IP_TYPE_NAME, CountryMaster.COUNTRY_MASTER.NAME,
				StateMaster.STATE_MASTER.STATE, CityMaster.CITY_MASTER.CITY).from(IpProductList.IP_PRODUCT_LIST)
				.leftJoin(IpTypeMaster.IP_TYPE_MASTER)
				.on(IpTypeMaster.IP_TYPE_MASTER.ID.eq(IpProductList.IP_PRODUCT_LIST.IP_TYPE_MASTER_ID))
				.leftJoin(CountryMaster.COUNTRY_MASTER).on(IpProductList.IP_PRODUCT_LIST.COUNTRY_ID.eq(CountryMaster.COUNTRY_MASTER.ID))
				.leftJoin(StateMaster.STATE_MASTER).on(IpProductList.IP_PRODUCT_LIST.STATE_ID.eq(StateMaster.STATE_MASTER.ID))
				.leftJoin(CityMaster.CITY_MASTER).on(IpProductList.IP_PRODUCT_LIST.CITY_ID.eq(CityMaster.CITY_MASTER.ID))
				.where(IpProductList.IP_PRODUCT_LIST.IS_ACTIVE.eq("Y"))
				.and(IpTypeMaster.IP_TYPE_MASTER.IS_ACTIVE.eq("Y"))
				.orderBy(IpProductList.IP_PRODUCT_LIST.CREATED_DATE.desc()).limit(12)
				.fetch();
	}
	
	/**
	 * Get All recently sold product List.
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<Record> recentlySoleProdList(DSLContext dslContext) throws Exception {

		Result<Record> result = dslContext.select().from(ProductDetail.PRODUCT_DETAIL)
				.leftJoin(CategoriesMaster.CATEGORIES_MASTER)
				.on(CategoriesMaster.CATEGORIES_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID))
				.leftJoin(StateMaster.STATE_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.STATE_ID.eq(StateMaster.STATE_MASTER.ID))
				.leftJoin(CityMaster.CITY_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.CITY_ID.eq(CityMaster.CITY_MASTER.ID))
				.where(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y"))
				.and(ProductDetail.PRODUCT_DETAIL.REMARKS.eq("Sold"))
				.orderBy(ProductDetail.PRODUCT_DETAIL.UPDATED_DATE.desc()).limit(12).fetch();

		return result;
	}
	
	/**
	 * @param dslContext
	 * @return
	 */
	public Result<Record> fetchBuyersCornerRequirmnt(DSLContext dslContext) throws Exception {
		Result<Record> result = dslContext.select().from(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT)
				.join(CountryMaster.COUNTRY_MASTER)
				.on(CountryMaster.COUNTRY_MASTER.ID.eq(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.COUNTRY_ID))
				.join(StateMaster.STATE_MASTER)
				.on(StateMaster.STATE_MASTER.ID.eq(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.STATE_ID))
				.join(CityMaster.CITY_MASTER)
				.on(CityMaster.CITY_MASTER.ID.eq(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.CITY_ID))
				.join(UomMaster.UOM_MASTER)
				.on(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.UOM_ID.eq(UomMaster.UOM_MASTER.ID))
				.where(CityMaster.CITY_MASTER.IS_ACTIVE.eq("Y"))
				.and(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.IS_ACTIVE.eq("Y"))
				.and(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.IS_APPROVED.eq("Y")).limit(7).fetch();

		return result;
	}

	/**
	 * Get All details of single product and seller details.
	 * 
	 * @param baseRequestModel
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<Record> fetchSingleProductDetal(SellerBaseRequestModel baseRequestModel, DSLContext dslContext)
			throws Exception {

		/*String query2 = "select * from tph_db.product_detail a left join tph_db.uom_master b on a.uom_id = b.id "
				+ "left join tph_db.users c on a.user_id = c.id \n" + 
				"left join tph_db.state_master d on c.state_id=d.id left join tph_db.city_master e on c.city_id = e.id"
				+ " where c.is_seller = 'Y' and a.id ="+baseRequestModel.getProductId();*/
		
		String query2 = "select a.id productId, product_code, product_name,product_sub_desc,product_description,product_brand ,mfg_purchase_year,quantity ,acronym ,uom_name, price_per_unit,price_negotiable,prod_loc,city,\n" + 
				"a.image1,a.image2,a.image3,video_doc, c.id userId,user_mobile,first_name,last_name,company,email,state\n" + 
				"from tph_db.product_detail a left join tph_db.uom_master b on a.uom_id = b.id \n" + 
				"				left join tph_db.users c on a.user_id = c.id \n" + 
				"				left join tph_db.state_master d on c.state_id=d.id \n" + 
				"                left join tph_db.city_master e on c.city_id = e.id\n" + 
				"                left join tph_db.country_master f on a.country_id = f.id\n" + 
				"				where  a.id ="+baseRequestModel.getProductId();
		
		Result<Record> result = dslContext.fetch(query2);
		
		return result;
	}
	
	/**
	 * Get All recent added product List.
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<Record> relatedProductList(Integer subCatId, DSLContext dslContext) throws Exception {

		Result<Record> result = dslContext.select().from(ProductDetail.PRODUCT_DETAIL)
				.leftJoin(CategoriesMaster.CATEGORIES_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID.eq(CategoriesMaster.CATEGORIES_MASTER.ID))
				.leftJoin(SubCategories.SUB_CATEGORIES)
				.on(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID.eq(SubCategories.SUB_CATEGORIES.ID))
				.leftJoin(CountryMaster.COUNTRY_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.COUNTRY_ID.eq(CountryMaster.COUNTRY_MASTER.ID))
				.leftJoin(StateMaster.STATE_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.STATE_ID.eq(StateMaster.STATE_MASTER.ID))
				.leftJoin(CityMaster.CITY_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.CITY_ID.eq(CityMaster.CITY_MASTER.ID))
				.where(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y"))
				//.and(ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME.like("%"+productName+"%"))
				.and(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID.eq(subCatId))
				.orderBy(ProductDetail.PRODUCT_DETAIL.CREATED_DATE.desc()).limit(12).fetch();

		return result;
	}
	
	/**
	 * Get All details of single product of IndustrialService table.
	 * 
	 * @param baseRequestModel
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<Record> fetchSingleProductDetalIndustrialService(SellerBaseRequestModel baseRequestModel, DSLContext dslContext)
			throws Exception {
		/*String query = "select * from tph_db.industrial_product_services a left join tph_db.users b on a.id=b.id left join tph_db.state_master c on a.state_id = c.id\n" + 
				"left join tph_db.city_master d on a.city_id = d.id left join tph_db.industrial_services e on a.service_caterory_id = e.id where b.is_seller = 'Y' and a.is_active = 'Y' and a.id ="+baseRequestModel.getProductId();*/
		
		String query = "select a.id,service_caterory_id, service_product_code, service_title,service_description,b.service_caterory,exp_in_month,exp_in_year,\n" + 
				"a.image_1,a.image_2,a.image_3, c.id userId,user_mobile, email,first_name,last_name,company,f.name,state,e.city\n" + 
				"from tph_db.industrial_product_services a left join industrial_services b on a.service_caterory_id = b.id \n" + 
				"				left join tph_db.users c on a.user_id = c.id \n" + 
				"				left join tph_db.state_master d on c.state_id=d.id \n" + 
				"                left join tph_db.city_master e on c.city_id = e.id\n" + 
				"                left join tph_db.country_master f on a.country_id = f.id\n" + 
				"				where a.is_active = 'Y' and  a.id ="+baseRequestModel.getProductId();
		
		Result<Record> result = dslContext.fetch(query);
		
		return result;
	}
	
	/**
	 * Get All details of single product of Industrial Plot table.
	 * 
	 * @param baseRequestModel
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<Record> fetchSingleProductDetalIndustrialPlot(SellerBaseRequestModel baseRequestModel, DSLContext dslContext)
			throws Exception {
		/* String query = "select * from tph_db.ip_product_list a left join tph_db.users b on a.id=b.id left join tph_db.state_master c on a.state_id = c.id\n" + 
				"left join tph_db.city_master d on a.city_id = d.id left join tph_db.ip_type_master e on a.ip_type_master_id = e.id where b.is_seller = 'Y' and a.is_active = 'Y' and e.is_active = 'Y' and a.id ="+baseRequestModel.getProductId();*/
		
		String query = "select ip_code,main_category,ip_type_name,ip_tittle,ip_description,location,ip_negotiable,date(a.Created_date) as poste_ddate,available_year,\n" + 
				"                rate_per_unit,acronym,quantity,image_1,image_2,image_3,b.id userId,user_mobile,email,first_name,last_name,company,f.name,d.city,state\n" + 
				"                from tph_db.ip_product_list a \n" + 
				"                left join tph_db.users b on a.user_id=b.id left join tph_db.state_master c on a.state_id = c.id\n" + 
				"				left join tph_db.city_master d on a.city_id = d.id \n" + 
				"                left join tph_db.ip_type_master e on a.ip_type_master_id = e.id \n" + 
				"                left join tph_db.uom_master g on a.uom_id = g.id \n" + 
				"                left join tph_db.country_master f on a.country_id = f.id\n" + 
				"                where b.is_seller = 'Y' and a.is_active = 'Y' and e.is_active = 'Y' and a.id ="+baseRequestModel.getProductId();
				
		Result<Record> result = dslContext.fetch(query);
		
		return result;
	}
	
	/**
	 * @param dslContext
	 * @return
	 */
	public Result<Record> fetchAllBuyersCornerRequirmnt(DSLContext dslContext) throws Exception {
		Result<Record> result = dslContext.select().from(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT)
				.join(CountryMaster.COUNTRY_MASTER)
				.on(CountryMaster.COUNTRY_MASTER.ID.eq(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.COUNTRY_ID))
				.join(StateMaster.STATE_MASTER)
				.on(StateMaster.STATE_MASTER.ID.eq(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.STATE_ID))
				.join(CityMaster.CITY_MASTER)
				.on(CityMaster.CITY_MASTER.ID.eq(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.CITY_ID))
				.join(UomMaster.UOM_MASTER)
				.on(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.UOM_ID.eq(UomMaster.UOM_MASTER.ID))
				.where(CityMaster.CITY_MASTER.IS_ACTIVE.eq("Y"))
				.and(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.IS_ACTIVE.eq("Y"))
				.and(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.IS_APPROVED.eq("Y")).fetch();

		return result;
	}
	
	/**
	 * @param request
	 * @param dslContext
	 */
	public Result<Record> fetchUserDataWithId(AddBuyerRequirmentReq request, DSLContext dslContext) throws Exception{
		
		String query = "select * from tph_db.users where id = "+request.getUserId();
		Result<Record> result = dslContext.fetch(query);
		
		return result;
	}
	
	/**
	 * @param request
	 * @param dslContext
	 * @return
	 */
	public Result<?> fetchAllSubCateProdList(SellerBaseRequestModel request, DSLContext dslContext)
			throws Exception {
		Result<?> result = dslContext.select(ProductDetail.PRODUCT_DETAIL.ID, ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME, ProductDetail.PRODUCT_DETAIL.REMARKS,
				ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION, ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND, CountryMaster.COUNTRY_MASTER.NAME,
				StateMaster.STATE_MASTER.STATE, CityMaster.CITY_MASTER.CITY, CategoriesMaster.CATEGORIES_MASTER.CAT_NAME,
				ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE, ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID, SubCategories.SUB_CATEGORIES.SUB_CATEGORY_NAME,
				ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID, ProductDetail.PRODUCT_DETAIL.IMAGE1).from(ProductDetail.PRODUCT_DETAIL)
				.join(CountryMaster.COUNTRY_MASTER)
				.on(CountryMaster.COUNTRY_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.COUNTRY_ID))
				.join(StateMaster.STATE_MASTER)
				.on(StateMaster.STATE_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.STATE_ID))
				.join(CityMaster.CITY_MASTER).on(CityMaster.CITY_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CITY_ID))
				.join(CategoriesMaster.CATEGORIES_MASTER).on(CategoriesMaster.CATEGORIES_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID))
				.join(SubCategories.SUB_CATEGORIES).on(SubCategories.SUB_CATEGORIES.ID.eq(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID))
				.where(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID.eq(request.getCatTypeId()))
				.and(ProductDetail.PRODUCT_DETAIL.INDUSTRIAL_TYPE_ID.eq(request.getIndustrialTypeId()))
				.and(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID.eq(request.getSubCatTypeId()))
				.and(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y")).orderBy(ProductDetail.PRODUCT_DETAIL.ID.desc()).fetch();

		return result;
	}
	
	/**
	 * @param request
	 * @param userResult 
	 * @param dslContext
	 * @return
	 */
	public Integer addBuyersRequirmnt(AddBuyerRequirmentReq request, Result<Record> userResult, DSLContext dslContext) throws Exception {
		
		String email = null, mobileNumber = null, firstName = null, componyName = null;
		
		if (userResult != null && userResult.isNotEmpty()) {
			for (Record record : userResult) {
				email = record.getValue(Users.USERS.EMAIL);
				mobileNumber = record.getValue(Users.USERS.USER_MOBILE);
				firstName = record.getValue(Users.USERS.FIRST_NAME);
				firstName = record.getValue(Users.USERS.COMPANY);
			}
		}
		
		BuyerCornerRequirementRecord record = dslContext.insertInto(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.PRODUCT_NAME,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.PRODUCT_DESCRIPTION,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.REQUIRED_QUANTITY,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.PRODUCT_BRAND,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.STATE_ID,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.CITY_ID,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.COUNTRY_ID,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.UOM_ID,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.EMAIL_ID,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.MOBILE_NO,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.CONTACT_PERSON,
				BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.COMPANY_NAME)
		
		.values(request.getProductTitle(),
				request.getProductDescription(),
				request.getRequiredQuantity(),
				request.getBrandName(),
				request.getStateId(),
				request.getCityId(),
				request.getCountryId(),
				request.getUomId(), email, mobileNumber, firstName, componyName)
		 .returning(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.ID)
		  .fetchOne();
		
		  int returningIndex = record.getId();
		  int returnIndex = 0;
		  if(returningIndex != 0) {
			  String prodCode = new CommonMethods().getProductCodeBuyerCorner(returningIndex);
			 returnIndex = updateBuyerCornerTableProdCode(returningIndex,prodCode, dslContext);
		  }
		
		return returnIndex;
	}
	
	/**
	 * @param returningIndex
	 * @param prodCode 
	 * @param dslContext
	 * @throws Exception
	 */
	private int updateBuyerCornerTableProdCode(int returningIndex, String prodCode, DSLContext dslContext) throws Exception{
		return dslContext.update(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT)
				.set(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.PROD_CODE, prodCode)
				.where(BuyerCornerRequirement.BUYER_CORNER_REQUIREMENT.ID.eq(returningIndex)).execute();
	}

	/**
	 * Get All IndustrialPlot product List..
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<?> industrialPlotList(DSLContext dslContext) {
		
		return dslContext.select(IpProductList.IP_PRODUCT_LIST.ID, IpProductList.IP_PRODUCT_LIST.IP_TITTLE, IpProductList.IP_PRODUCT_LIST.USER_ID,
				IpProductList.IP_PRODUCT_LIST.IP_CODE,IpProductList.IP_PRODUCT_LIST.IMAGE_1, IpProductList.IP_PRODUCT_LIST.REMARK, IpTypeMaster.IP_TYPE_MASTER.IP_TYPE_NAME, CountryMaster.COUNTRY_MASTER.NAME,
				StateMaster.STATE_MASTER.STATE, CityMaster.CITY_MASTER.CITY).from(IpProductList.IP_PRODUCT_LIST)
				.leftJoin(IpTypeMaster.IP_TYPE_MASTER)
				.on(IpTypeMaster.IP_TYPE_MASTER.ID.eq(IpProductList.IP_PRODUCT_LIST.IP_TYPE_MASTER_ID))
				.leftJoin(CountryMaster.COUNTRY_MASTER).on(IpProductList.IP_PRODUCT_LIST.COUNTRY_ID.eq(CountryMaster.COUNTRY_MASTER.ID))
				.leftJoin(StateMaster.STATE_MASTER).on(IpProductList.IP_PRODUCT_LIST.STATE_ID.eq(StateMaster.STATE_MASTER.ID))
				.leftJoin(CityMaster.CITY_MASTER).on(IpProductList.IP_PRODUCT_LIST.CITY_ID.eq(CityMaster.CITY_MASTER.ID))
				.where(IpProductList.IP_PRODUCT_LIST.IS_ACTIVE.eq("Y"))
				.and(IpTypeMaster.IP_TYPE_MASTER.IS_ACTIVE.eq("Y"))
				.orderBy(IpProductList.IP_PRODUCT_LIST.CREATED_DATE.desc())
				.fetch();
	}
	
	/**
	 * Get All Industrial product service List..
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<?> industrialProdServiceList(DSLContext dslContext) {
		return dslContext.select(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.ID, IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_TITLE, 
				IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_PRODUCT_CODE, IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_MONTH, 
				IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_YEAR, IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.USER_ID, 
				IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_1, CityMaster.CITY_MASTER.CITY)
				.from(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES)
				.join(CityMaster.CITY_MASTER)
				.on(CityMaster.CITY_MASTER.ID.eq(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.CITY_ID))
				.where(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IS_ACTIVE.eq("Y"))
				.orderBy(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.CREATED_DATE.desc())
				.fetch();
	}
	
	/**
	 * Get All industrial_services categoru..
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<?> industrialServiceCategory(DSLContext dslContext) {
		return dslContext.select(IndustrialServices.INDUSTRIAL_SERVICES.ID,
				IndustrialServices.INDUSTRIAL_SERVICES.SERVICE_CATERORY)
				.from(IndustrialServices.INDUSTRIAL_SERVICES)
				.fetch();
	}
	
	/**
	 * Get All Industrial product service List..
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<?> stockClearanceLeadList(DSLContext dslContext) {
		
		return dslContext.select(ProductDetail.PRODUCT_DETAIL.ID, ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME, ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION,  ProductDetail.PRODUCT_DETAIL.IMAGE1,
				ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND,CategoriesMaster.CATEGORIES_MASTER.CAT_NAME, ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE,
				ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID, ProductDetail.PRODUCT_DETAIL.REMARKS, StateMaster.STATE_MASTER.STATE, CityMaster.CITY_MASTER.CITY).from(ProductDetail.PRODUCT_DETAIL)
				.join(CategoriesMaster.CATEGORIES_MASTER)
				.on(CategoriesMaster.CATEGORIES_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID))
				.leftJoin(StateMaster.STATE_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.STATE_ID.eq(StateMaster.STATE_MASTER.ID))
				.leftJoin(CityMaster.CITY_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.CITY_ID.eq(CityMaster.CITY_MASTER.ID))
				.where(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID.eq(0))
				.and(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y"))
				.orderBy(ProductDetail.PRODUCT_DETAIL.CREATED_DATE.desc())
				.fetch();
	}
	
	/**
	 * @param dslContext
	 * @param request
	 * @return
	 */
	public Result<ProductLikeShareRecord> checkProductInWishList(DSLContext dslContext, SellerAddNewPostRequestModel request) throws Exception{
		return dslContext.selectFrom(ProductLikeShare.PRODUCT_LIKE_SHARE).where(ProductLikeShare.PRODUCT_LIKE_SHARE.SHARED_BY.eq(request.getUserId())
				.and(ProductLikeShare.PRODUCT_LIKE_SHARE.PRODUCT_ID.eq(request.getProductId())
						.and(ProductLikeShare.PRODUCT_LIKE_SHARE.PRODUCT_CODE.eq(request.getProductCode()))))
				.orderBy(ProductLikeShare.PRODUCT_LIKE_SHARE.ID.desc()).limit(1).fetch();
	}
	
	/**
	 * This function is using for insert data for wishlist
	 * @param returningIndex
	 * @param prodCode
	 * @param sellerAddNewPostRequestModel 
	 * @param dslContext
	 * @return 
	 */
	public Integer saveWishList(DSLContext dslContext, SellerAddNewPostRequestModel request)
			throws Exception {
		
		ProductLikeShareRecord productLikeShareRecord = new ProductLikeShareRecord();
		
		productLikeShareRecord.setSharedBy(request.getUserId());
		productLikeShareRecord.setProductCode(request.getProductCode()); 
		productLikeShareRecord.setProductId(request.getProductId());
		
		return dslContext.executeInsert(productLikeShareRecord);
		
	}
	
	/**
	 * This function is using for insert data for Make an offer
	 * @param returningIndex
	 * @param prodCode
	 * @param sellerAddNewPostRequestModel 
	 * @param dslContext
	 * @return 
	 */
	public Integer saveMakeAnOffer(DSLContext dslContext, MakeAnOfferRquest request)
			throws Exception {
		
		MakeAnOfferRecord makeAnOfferRecord = new MakeAnOfferRecord();
		
		makeAnOfferRecord.setProductId(request.getProductId());
		makeAnOfferRecord.setBuyerUserId(request.getBuyerId());
		makeAnOfferRecord.setSellerUserId(request.getSellerId());
		makeAnOfferRecord.setAvailableQuantity(BigDecimal.valueOf(request.getAvailableQuantity()));
		makeAnOfferRecord.setRequoiredQuantity(BigDecimal.valueOf(request.getRequiredQuantity()));
		makeAnOfferRecord.setNegotiationAmount(BigDecimal.valueOf(request.getOfferPrice()));
		
		return dslContext.executeInsert(makeAnOfferRecord);
		
	}
}
