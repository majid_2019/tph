package com.tph.dao;

import org.jooq.DSLContext;
import org.jooq.Result;

import com.tph.jooq.tph_db.tables.CategoriesMaster;
import com.tph.jooq.tph_db.tables.CityMaster;
import com.tph.jooq.tph_db.tables.CountryMaster;
import com.tph.jooq.tph_db.tables.IndustrialProductServices;
import com.tph.jooq.tph_db.tables.IpProductList;
import com.tph.jooq.tph_db.tables.IpTypeMaster;
import com.tph.jooq.tph_db.tables.ProductDetail;
import com.tph.jooq.tph_db.tables.StateMaster;
import com.tph.jooq.tph_db.tables.SubCategories;
import com.tph.request.model.FilterDataRequest;

public class TphFilterDao {
	/**
	 * @param dslContext
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public Result<?> filterIndustrialPlotList(DSLContext dslContext, FilterDataRequest request) throws Exception{
		
		return dslContext.select(IpProductList.IP_PRODUCT_LIST.ID, IpProductList.IP_PRODUCT_LIST.IP_TITTLE, IpProductList.IP_PRODUCT_LIST.USER_ID,
				IpProductList.IP_PRODUCT_LIST.IP_CODE,IpProductList.IP_PRODUCT_LIST.IMAGE_1, IpProductList.IP_PRODUCT_LIST.REMARK, IpTypeMaster.IP_TYPE_MASTER.IP_TYPE_NAME, CountryMaster.COUNTRY_MASTER.NAME,
				StateMaster.STATE_MASTER.STATE, CityMaster.CITY_MASTER.CITY).from(IpProductList.IP_PRODUCT_LIST)
				.leftJoin(IpTypeMaster.IP_TYPE_MASTER)
				.on(IpTypeMaster.IP_TYPE_MASTER.ID.eq(IpProductList.IP_PRODUCT_LIST.IP_TYPE_MASTER_ID))
				.leftJoin(CountryMaster.COUNTRY_MASTER).on(IpProductList.IP_PRODUCT_LIST.COUNTRY_ID.eq(CountryMaster.COUNTRY_MASTER.ID))
				.leftJoin(StateMaster.STATE_MASTER).on(IpProductList.IP_PRODUCT_LIST.STATE_ID.eq(StateMaster.STATE_MASTER.ID))
				.leftJoin(CityMaster.CITY_MASTER).on(IpProductList.IP_PRODUCT_LIST.CITY_ID.eq(CityMaster.CITY_MASTER.ID))
				.where(IpProductList.IP_PRODUCT_LIST.IS_ACTIVE.eq("Y"))
				.and(IpTypeMaster.IP_TYPE_MASTER.IS_ACTIVE.eq("Y"))
				.and(IpProductList.IP_PRODUCT_LIST.CITY_ID.like(request.getCityId() != null ? request.getCityId().toString() : "%"))
				.and(IpProductList.IP_PRODUCT_LIST.STATE_ID.like(request.getStateId() != null ? request.getStateId().toString() : "%"))
				.and(IpProductList.IP_PRODUCT_LIST.IP_TYPE_MASTER_ID.like(request.getRentSellId() != null ? request.getRentSellId().toString() : "%"))
				.fetch();
	}
	
	/**
	 * @param dslContext
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public Result<?> filterStockClearanceList(DSLContext dslContext, FilterDataRequest request) throws Exception{
		
		return dslContext.select(ProductDetail.PRODUCT_DETAIL.ID, ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME, ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION,  ProductDetail.PRODUCT_DETAIL.IMAGE1,
				ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND,CategoriesMaster.CATEGORIES_MASTER.CAT_NAME, ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE,
				ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID, ProductDetail.PRODUCT_DETAIL.REMARKS, StateMaster.STATE_MASTER.STATE, CityMaster.CITY_MASTER.CITY).from(ProductDetail.PRODUCT_DETAIL)
				.join(CategoriesMaster.CATEGORIES_MASTER)
				.on(CategoriesMaster.CATEGORIES_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID))
				.leftJoin(StateMaster.STATE_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.STATE_ID.eq(StateMaster.STATE_MASTER.ID))
				.leftJoin(CityMaster.CITY_MASTER)
				.on(ProductDetail.PRODUCT_DETAIL.CITY_ID.eq(CityMaster.CITY_MASTER.ID))
				.where(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID.eq(0))
				.and(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y"))
				.and(ProductDetail.PRODUCT_DETAIL.STATE_ID.like(request.getStateId() != null ? request.getStateId().toString() : "%"))
				.and(ProductDetail.PRODUCT_DETAIL.CITY_ID.like(request.getCityId() != null ? request.getCityId().toString() : "%"))
				.and(ProductDetail.PRODUCT_DETAIL.INDUSTRIAL_TYPE_ID.like(request.getIndustiesType() != null ? request.getIndustiesType().toString() : "%"))
				.fetch();
	}
	
	/**
	 * @param dslContext
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public Result<?> filterIndustrialServiceList(DSLContext dslContext, FilterDataRequest request) throws Exception{
		
		return dslContext.select(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.ID, IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_TITLE, 
				IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_PRODUCT_CODE, IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_MONTH, 
				IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_YEAR, IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.USER_ID, 
				IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_1, CityMaster.CITY_MASTER.CITY)
				.from(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES)
				.join(CityMaster.CITY_MASTER)
				.on(CityMaster.CITY_MASTER.ID.eq(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.CITY_ID))
				.where(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IS_ACTIVE.eq("Y"))
				.and(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.STATE_ID.like(request.getStateId() != null ? request.getStateId().toString() : "%"))
				.and(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.CITY_ID.like(request.getCityId() != null ? request.getCityId().toString() : "%"))
				.and(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_CATERORY_ID.like(request.getIndServiceCategory() != null ? request.getIndServiceCategory().toString() : "%"))
				.fetch();
	}
	
	/**
	 * @param dslContext
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public Result<?> filterInventoryDataList(DSLContext dslContext, FilterDataRequest request) throws Exception{
		
		return dslContext.select(ProductDetail.PRODUCT_DETAIL.ID, ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME, ProductDetail.PRODUCT_DETAIL.REMARKS,
				ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION, ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND, CountryMaster.COUNTRY_MASTER.NAME,
				StateMaster.STATE_MASTER.STATE, CityMaster.CITY_MASTER.CITY, CategoriesMaster.CATEGORIES_MASTER.CAT_NAME, 
				ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE, ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID, SubCategories.SUB_CATEGORIES.SUB_CATEGORY_NAME,
				ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID, ProductDetail.PRODUCT_DETAIL.IMAGE1).from(ProductDetail.PRODUCT_DETAIL)
				.join(CountryMaster.COUNTRY_MASTER)
				.on(CountryMaster.COUNTRY_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.COUNTRY_ID))
				.join(StateMaster.STATE_MASTER)
				.on(StateMaster.STATE_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.STATE_ID))
				.join(CityMaster.CITY_MASTER).on(CityMaster.CITY_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CITY_ID))
				.join(CategoriesMaster.CATEGORIES_MASTER).on(CategoriesMaster.CATEGORIES_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID))
				.join(SubCategories.SUB_CATEGORIES).on(SubCategories.SUB_CATEGORIES.ID.eq(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID))
				.where(ProductDetail.PRODUCT_DETAIL.CATEGORY_ID.eq(request.getCatTypeId()))
				.and(ProductDetail.PRODUCT_DETAIL.INDUSTRIAL_TYPE_ID.eq(request.getIndustrialTypeId()))
				.and(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID.eq(request.getSubCatTypeId()))
				.and(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y"))
				.and(ProductDetail.PRODUCT_DETAIL.STATE_ID.like(request.getStateId() != null ? request.getStateId().toString() : "%"))
				.and(ProductDetail.PRODUCT_DETAIL.CITY_ID.like(request.getCityId() != null ? request.getCityId().toString() : "%"))
				.and(ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND.like(request.getBrand() != null ? request.getBrand() : "%"))
				.orderBy(ProductDetail.PRODUCT_DETAIL.ID.desc()).fetch();
	}
}
