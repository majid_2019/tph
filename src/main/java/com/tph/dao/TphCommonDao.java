package com.tph.dao;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Result;

import com.tph.jooq.tph_db.tables.CentralCommunicationLog;
import com.tph.jooq.tph_db.tables.CityMaster;
import com.tph.jooq.tph_db.tables.CountryMaster;
import com.tph.jooq.tph_db.tables.IndustrialProductServices;
import com.tph.jooq.tph_db.tables.IndustrialType;
import com.tph.jooq.tph_db.tables.IpProductList;
import com.tph.jooq.tph_db.tables.ProductBrandMaster;
import com.tph.jooq.tph_db.tables.ProductDetail;
import com.tph.jooq.tph_db.tables.StateMaster;
import com.tph.jooq.tph_db.tables.UomMaster;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.CityMasterRecord;
import com.tph.jooq.tph_db.tables.records.CountryMasterRecord;
import com.tph.jooq.tph_db.tables.records.IndustrialTypeRecord;
import com.tph.jooq.tph_db.tables.records.ProductBrandMasterRecord;
import com.tph.jooq.tph_db.tables.records.StateMasterRecord;
import com.tph.jooq.tph_db.tables.records.UomMasterRecord;
import com.tph.request.model.ProdViewCountRequest;
import com.tph.request.model.SellerBaseRequestModel;

public class TphCommonDao {
	/**
	 * @param dslContext
	 * @return
	 * @throws Exception
	 */
	public Result<UomMasterRecord> uomList(DSLContext dslContext) throws Exception {
		return dslContext.selectFrom(UomMaster.UOM_MASTER).fetch();
	}

	/**
	 * This function is using for get all the Industrial data
	 * 
	 * @param dslContext
	 * @return
	 */
	public Result<IndustrialTypeRecord> fetchIdustryList(DSLContext dslContext) {
		return dslContext.selectFrom(IndustrialType.INDUSTRIAL_TYPE)
				.where(IndustrialType.INDUSTRIAL_TYPE.IS_ACTIVE.eq("Y")).fetch();

	}
	
	/**
	 * @param dslContext
	 * @return
	 */
	public Result<CountryMasterRecord> countryList(DSLContext dslContext) {
		return dslContext.selectFrom(CountryMaster.COUNTRY_MASTER).fetch();
	}
	
	/**
	 * @param dslContext
	 * @param integer
	 * @return
	 * @throws Exception
	 */
	public Result<StateMasterRecord> stateList(DSLContext dslContext, Integer integer) throws Exception {
		return dslContext.selectFrom(StateMaster.STATE_MASTER).fetch();
	}
	
	/**
	 * @param dslContext
	 * @param sellerBaseRequestModel
	 * @return
	 * @throws Exception
	 */
	public Result<Record> fetchCategoryMaster(DSLContext dslContext, SellerBaseRequestModel sellerBaseRequestModel)
			throws Exception {
		String query = "select b.id as  cat_master_id, b.cat_name as cat_name, c.id as sub_category_id, c.sub_category_name as sub_category_name from tph_db.industrial_cat_sub_cat_mapping a join tph_db.categories_master b on (a.cat_id=b.id)\n"
				+ "join tph_db.sub_Categories c on(a.sub_cat_id=c.id) where a.industrial_type_id = "
				+ sellerBaseRequestModel.getIndustrialTypeId();
		
		Result<Record> record = dslContext.fetch(query);

		return record;
	}
	
	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public Result<CityMasterRecord> cityList(DSLContext dslContext, Integer stateId) throws Exception {
		return dslContext.selectFrom(CityMaster.CITY_MASTER).where(CityMaster.CITY_MASTER.STATE_ID.eq(stateId))
				.and(CityMaster.CITY_MASTER.IS_ACTIVE.eq("Y")).fetch();
	}
	
	/**
	 * @param dslContext
	 * @return
	 */
	public Result<ProductBrandMasterRecord> brandList(DSLContext dslContext) {
		return dslContext.selectFrom(ProductBrandMaster.PRODUCT_BRAND_MASTER).fetch();
	}
	
	/**
	 * @param userId
	 * @param dslContext
	 */
	public Result<Record2<String, String>> fetchSMSCommLog(Integer userId, DSLContext dslContext) throws Exception{
		Result<Record2<String, String>> record = dslContext.select(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.COMM_CONTENT, Users.USERS.FIRST_NAME)
		.from(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG)
		.join(Users.USERS)
		.on(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.FROM_USER_ID.eq(Users.USERS.ID))
		.where(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.TO_USER_ID.eq(userId))
		.and(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.COMM_TYPE.eq("SMS"))
		.and(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.IS_ACTIVE.eq("Y"))
		.and(Users.USERS.IS_BYERS.eq("Y"))
		.and(Users.USERS.ACTIVE.eq("Y"))
		.orderBy(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.CREATE_DATE.desc()).limit(10)
		.fetch();
		
		return record;
	}
	
	/**
	 * @param userId
	 * @param dslContext
	 */
	public Result<Record2<String, String>> fetchContactSellerNotiCommLog(Integer userId, DSLContext dslContext) throws Exception{
		Result<Record2<String, String>> record = dslContext.select(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.COMM_CONTENT, Users.USERS.FIRST_NAME)
		.from(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG)
		.join(Users.USERS)
		.on(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.FROM_USER_ID.eq(Users.USERS.ID))
		.where(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.TO_USER_ID.eq(userId))
		.and(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.COMM_CATEGORY.eq("Notification Contact Seller"))
		.and(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.IS_ACTIVE.eq("Y"))
		.and(Users.USERS.IS_BYERS.eq("Y"))
		.and(Users.USERS.ACTIVE.eq("Y"))
		.orderBy(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.CREATE_DATE.desc()).limit(10)
		.fetch();
		
		return record;
	}
	
	/**
	 * @param userId
	 * @param dslContext
	 */
	public Result<Record2<String, String>> fetchMknOfferNotiCommLog(Integer userId, DSLContext dslContext) throws Exception{
		Result<Record2<String, String>> record = dslContext.select(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.COMM_CONTENT, Users.USERS.FIRST_NAME)
		.from(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG)
		.join(Users.USERS)
		.on(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.FROM_USER_ID.eq(Users.USERS.ID))
		.where(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.TO_USER_ID.eq(userId))
		.and(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.COMM_CATEGORY.eq("Notification MKN Offer"))
		.and(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.IS_ACTIVE.eq("Y"))
		.and(Users.USERS.IS_BYERS.eq("Y"))
		.and(Users.USERS.ACTIVE.eq("Y"))
		.orderBy(CentralCommunicationLog.CENTRAL_COMMUNICATION_LOG.CREATE_DATE.desc()).limit(10)
		.fetch();
		
		return record;
	}

	// Update Inventory Table Counter
	public Integer updateInventoryCounter(ProdViewCountRequest request, DSLContext dslContext) {
		Long counter = 0l;
		Result<Record1<Long>> counterValue = dslContext.select(ProductDetail.PRODUCT_DETAIL.PRODUCT_VIEW_COUNT).from(ProductDetail.PRODUCT_DETAIL)
		.where(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE.eq(request.getProductCode())).fetch();
		
		counter = counterValue.get(0).get("product_view_count")==null?0l:(long) counterValue.get(0).get("product_view_count");
		counter = counter+1;
		
		Integer counterUpdate = dslContext.update(ProductDetail.PRODUCT_DETAIL).set(ProductDetail.PRODUCT_DETAIL.PRODUCT_VIEW_COUNT, counter)
		 .where(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE.eq(request.getProductCode())).execute();
		
		System.out.println("counterUpdate -- "+counterUpdate);
		return counterUpdate;
	}
	
	// Update Inventory Table Counter
	public Integer updateIpProdListCounter(ProdViewCountRequest request, DSLContext dslContext) {
		Long counter = 0l;
		Result<Record1<Long>> counterValue = dslContext.select(IpProductList.IP_PRODUCT_LIST.PRODUCT_VIEW_COUNT).from(IpProductList.IP_PRODUCT_LIST)
		.where(IpProductList.IP_PRODUCT_LIST.IP_CODE.eq(request.getProductCode())).fetch();
			
		counter = counterValue.get(0).get("product_view_count")==null?0l:(long) counterValue.get(0).get("product_view_count");
		counter = counter+1;
			
		Integer counterUpdate = dslContext.update(IpProductList.IP_PRODUCT_LIST).set(IpProductList.IP_PRODUCT_LIST.PRODUCT_VIEW_COUNT, counter)
				.where(IpProductList.IP_PRODUCT_LIST.IP_CODE.eq(request.getProductCode())).execute();
			
			System.out.println("counterUpdate -- "+counterUpdate);
			return counterUpdate;
	}
	
	// Update Industrial product Service Table Counter
		public Integer updateIndustrialProdServiceCounter(ProdViewCountRequest request, DSLContext dslContext) {
			Long counter = 0l;
			Result<Record1<Long>> counterValue = dslContext.select(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.PRODUCT_VIEW_COUNT).from(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES)
			.where(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_PRODUCT_CODE.eq(request.getProductCode())).fetch();
				
			counter = counterValue.get(0).get("product_view_count")==null?0l:(long) counterValue.get(0).get("product_view_count");
			counter = counter+1;
				
			Integer counterUpdate = dslContext.update(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES).set(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.PRODUCT_VIEW_COUNT, counter)
					.where(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_PRODUCT_CODE.eq(request.getProductCode())).execute();
				
				System.out.println("counterUpdate -- "+counterUpdate);
				return counterUpdate;
		}
	
	
}
