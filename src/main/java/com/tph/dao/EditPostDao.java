package com.tph.dao;

import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;

import com.tph.constants.ConstantKeys;
import com.tph.jooq.tph_db.tables.CityMaster;
import com.tph.jooq.tph_db.tables.CountryMaster;
import com.tph.jooq.tph_db.tables.IndustrialProductServices;
import com.tph.jooq.tph_db.tables.IpProductList;
import com.tph.jooq.tph_db.tables.ProductDetail;
import com.tph.jooq.tph_db.tables.StateMaster;
import com.tph.jooq.tph_db.tables.UomMaster;
import com.tph.jooq.tph_db.tables.records.IndustrialProductServicesRecord;
import com.tph.jooq.tph_db.tables.records.IndustrialServicesRecord;
import com.tph.jooq.tph_db.tables.records.IpProductListRecord;
import com.tph.jooq.tph_db.tables.records.ProductDetailRecord;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.PhotoVideoPath;

public class EditPostDao {

	public Integer markSoldInvetoryPost(Integer userId, DSLContext dslContext) throws Exception {
		return dslContext.update(ProductDetail.PRODUCT_DETAIL).set(ProductDetail.PRODUCT_DETAIL.REMARKS,"SOLD")
				.where(ProductDetail.PRODUCT_DETAIL.ID.eq(userId)).execute();
	}

	public Integer markSoldPlotPost(Integer userId, DSLContext dslContext) throws Exception {
		return dslContext.update(IpProductList.IP_PRODUCT_LIST).set(IpProductList.IP_PRODUCT_LIST.REMARK,"SOLD")
				.where(IpProductList.IP_PRODUCT_LIST.ID.eq(userId)).execute();
	}

	public Result<Record> fetchnvetoryPost(Integer id, DSLContext dslContext) throws Exception {
		return dslContext.select().from(ProductDetail.PRODUCT_DETAIL).leftJoin(UomMaster.UOM_MASTER)
				.on(UomMaster.UOM_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.UOM_ID))
				.leftJoin(CountryMaster.COUNTRY_MASTER)
				.on(CountryMaster.COUNTRY_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.COUNTRY_ID))
				.leftJoin(StateMaster.STATE_MASTER)
				.on(StateMaster.STATE_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.STATE_ID))
				.leftJoin(CityMaster.CITY_MASTER).on(CityMaster.CITY_MASTER.ID.eq(ProductDetail.PRODUCT_DETAIL.CITY_ID))
				.where(ProductDetail.PRODUCT_DETAIL.ID.eq(id)).and(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y"))
				.fetch();
	}

	public Result<Record> fetchPlotPost(Integer id, DSLContext dslContext) throws Exception {
		return dslContext.select().from(IpProductList.IP_PRODUCT_LIST).leftJoin(UomMaster.UOM_MASTER)
				.on(UomMaster.UOM_MASTER.ID.eq(IpProductList.IP_PRODUCT_LIST.UOM_ID))
				.leftJoin(CountryMaster.COUNTRY_MASTER)
				.on(CountryMaster.COUNTRY_MASTER.ID.eq(IpProductList.IP_PRODUCT_LIST.COUNTRY_ID))
				.leftJoin(StateMaster.STATE_MASTER)
				.on(StateMaster.STATE_MASTER.ID.eq(IpProductList.IP_PRODUCT_LIST.STATE_ID))
				.leftJoin(CityMaster.CITY_MASTER)
				.on(CityMaster.CITY_MASTER.ID.eq(IpProductList.IP_PRODUCT_LIST.CITY_ID))
				.where(IpProductList.IP_PRODUCT_LIST.ID.eq(id)).and(IpProductList.IP_PRODUCT_LIST.IS_ACTIVE.eq("Y"))
				.fetch();
	}

	public Result<Record> fetchIndusServicesPost(Integer id, DSLContext dslContext) throws Exception {
		return dslContext.select().from(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES)
				.leftJoin(CountryMaster.COUNTRY_MASTER)
				.on(CountryMaster.COUNTRY_MASTER.ID
						.eq(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.COUNTRY_ID))
				.leftJoin(StateMaster.STATE_MASTER)
				.on(StateMaster.STATE_MASTER.ID.eq(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.STATE_ID))
				.leftJoin(CityMaster.CITY_MASTER)
				.on(CityMaster.CITY_MASTER.ID.eq(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.CITY_ID))
				.where(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.ID.eq(id))
				.and(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IS_ACTIVE.eq("Y")).fetch();
	}

	
	public Integer updateInventoryPost(SellerAddNewPostRequestModel request, DSLContext dslContext) throws Exception {

		String image1 = null, image2 = null, image3 = null, videoDoc = null;

		if (request.getPhotoVideoPathList() != null) {
			List<PhotoVideoPath> imageVideoList = request.getPhotoVideoPathList();
			for (int i = 0; i < imageVideoList.size(); i++) {
				if (i == 0 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image1 = imageVideoList.get(i).getPath();
				} else if (i == 1 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image2 = imageVideoList.get(i).getPath();
				} else if (i == 2 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image3 = imageVideoList.get(i).getPath();
				}

				if (imageVideoList.get(i).getItemId() == ConstantKeys.VIDEO_ITEM_ID) {
					videoDoc = imageVideoList.get(i).getPath();
				}
			}
		}

		ProductDetailRecord detailRecord = new ProductDetailRecord();

		if (request.getProductTitle() != null) {
			detailRecord.setProductName(request.getProductTitle());
		}
		if (request.getProductDetails() != null) {
			detailRecord.setProductDescription(request.getProductDetails());
		}
		if (image1 != null) {
			detailRecord.setImage1(image1);
		}
		if (image2 != null) {
			detailRecord.setImage2(image1);
		}
		if (image3 != null) {
			detailRecord.setImage3(image3);
		}
		if (videoDoc != null) {
			detailRecord.setVideoDoc(videoDoc);
		}

		return dslContext.executeUpdate(detailRecord, ProductDetail.PRODUCT_DETAIL.ID.eq(request.getId()));
	}
	
	public Integer updatePlotPost(SellerAddNewPostRequestModel request, DSLContext dslContext) throws Exception {

		String image1 = null, image2 = null, image3 = null;

		if (request.getPhotoVideoPathList() != null) {
			List<PhotoVideoPath> imageVideoList = request.getPhotoVideoPathList();
			for (int i = 0; i < imageVideoList.size(); i++) {
				if (i == 0 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image1 = imageVideoList.get(i).getPath();
				} else if (i == 1 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image2 = imageVideoList.get(i).getPath();
				} else if (i == 2 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image3 = imageVideoList.get(i).getPath();
				}
			}
		}

		IpProductListRecord detailRecord = new IpProductListRecord();

		if (request.getProductTitle() != null) {
			detailRecord.setIpTittle(request.getProductTitle());
		}
		if (request.getProductDetails() != null) {
			detailRecord.setIpDescription(request.getProductDetails());
		}
		if (image1 != null) {
			detailRecord.setImage_1(image1);
		}
		if (image2 != null) {
			detailRecord.setImage_2(image1);
		}
		if (image3 != null) {
			detailRecord.setImage_3(image3);
		}

		return dslContext.executeUpdate(detailRecord, IpProductList.IP_PRODUCT_LIST.ID.eq(request.getId()));
	}
	
	public Integer updateIndusServicePost(SellerAddNewPostRequestModel request, DSLContext dslContext) throws Exception {

		String image1 = null, image2 = null, image3 = null;

		if (request.getPhotoVideoPathList() != null) {
			List<PhotoVideoPath> imageVideoList = request.getPhotoVideoPathList();
			for (int i = 0; i < imageVideoList.size(); i++) {
				if (i == 0 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image1 = imageVideoList.get(i).getPath();
				} else if (i == 1 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image2 = imageVideoList.get(i).getPath();
				} else if (i == 2 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image3 = imageVideoList.get(i).getPath();
				}
			}
		}

		IndustrialProductServicesRecord detailRecord = new IndustrialProductServicesRecord();

		if (request.getProductTitle() != null) {
			detailRecord.setServiceTitle(request.getProductTitle());
		}
		if (request.getProductDetails() != null) {
			detailRecord.setServiceDescription(request.getProductDetails());
		}
		if (image1 != null) {
			detailRecord.setImage_1(image1);
		}
		if (image2 != null) {
			detailRecord.setImage_2(image1);
		}
		if (image3 != null) {
			detailRecord.setImage_3(image3);
		}

		return dslContext.executeUpdate(detailRecord, IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.ID.eq(request.getId()));
	}
}
