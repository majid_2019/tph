package com.tph.dao;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;

import com.tph.commonmethods.CommonMethods;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.UsersRecord;
import com.tph.request.model.LoginRequestModel;
import com.tph.request.model.RegistraionRequestModel;
import com.tph.request.model.SmsRequestModel;

public class TphLoginRegistraionDao {
	/**
	 * @param dslContext
	 * @param request
	 * @return
	 */
	public UsersRecord login(DSLContext dslContext, LoginRequestModel request) throws Exception{
		return dslContext.selectFrom(Users.USERS)
				.where(Users.USERS.USERNAME.eq(request.getUserName().trim())
				//.and(Users.USERS.PASSWORD.eq(request.getPassword())
				.and(Users.USERS.IS_SELLER.eq("Y"))
				.and(Users.USERS.ACTIVE.eq("Y"))).fetchOne();
	}
	
	/**
	 * @param dslContext
	 * @param loginRequestModel
	 * @return 
	 * @throws Exception
	 */
	public int updateSeller(DSLContext dslContext, RegistraionRequestModel request) throws Exception {
		return dslContext.update(Users.USERS)
				.set(Users.USERS.GST_NO, request.getGstNumber())
				.set(Users.USERS.ADDRESS_1, request.getAddress())
				.set(Users.USERS.PASSWORD,request.getPassword())
				.set(Users.USERS.USERNAME,request.getUserName())
				.set(Users.USERS.IP_ADDRESS, request.getIpAddress())
				.set(Users.USERS.COUNTRY_ID, request.getCountryId())
				.set(Users.USERS.STATE_ID, request.getStateId())
				.set(Users.USERS.CITY_ID, request.getCityId())
				.set(Users.USERS.LAST_LOGIN, new CommonMethods().currentDate())
				.set(Users.USERS.IS_SELLER, "Y")
				.set( Users.USERS.FIRST_NAME, request.getContactPerson())
				.where(Users.USERS.ID.eq(request.getUserId())).execute();
	}
	
	/**
	 * @param dslContext
	 * @param string 
	 * @param mobileNumber
	 * @param isSeller
	 * @return 
	 * @throws Exception
	 */
	public Result<Record> checkSellerUserExistOrNot(DSLContext dslContext, SmsRequestModel smsRequestModel, String userName) throws Exception{
		return dslContext.select().from(Users.USERS).where(Users.USERS.USER_MOBILE.eq(String.valueOf(smsRequestModel.getMobileNumber())))
		.or(Users.USERS.USERNAME.eq(userName))
		.and(Users.USERS.ACTIVE.eq("Y"))
		.fetch();
	}
	
	/**
	 * @param dslContext
	 * @param loginRequestModel for ServiceProvider
	 * @throws Exception
	 */
	public Integer registraion(DSLContext dslContext, RegistraionRequestModel request)
			throws Exception {
		String isSeller = null;
		String isBuyer = null;
		if(request.getIsSeller() != null && request.getIsSeller()) {
			isSeller = "Y";
			isBuyer = "Y";
		}
		else if(request.getIsBuyer() != null && request.getIsBuyer()) {
			isBuyer = "Y";
		}
		
		  UsersRecord record =
		  dslContext.insertInto(Users.USERS,
				  Users.USERS.COMPANY,
				  Users.USERS.FIRST_NAME,
				  Users.USERS.GST_NO,
				  Users.USERS.USER_MOBILE,
				  Users.USERS.EMAIL,
				  Users.USERS.ADDRESS_1,
				  Users.USERS.PIN_CODE,
				  Users.USERS.PASSWORD,
				  Users.USERS.USERNAME,
				  Users.USERS.IP_ADDRESS,
				  Users.USERS.COUNTRY_ID,
				  Users.USERS.STATE_ID,
				  Users.USERS.CITY_ID,
				  Users.USERS.LAST_LOGIN,
				  Users.USERS.IS_BYERS,
				  Users.USERS.IS_SELLER)
		  
		  .values(request.getCompanyName(),
				  request.getContactPerson(),
				  request.getGstNumber(),
				  request.getMobileNumber(),
				  request.getEmail(),
				  request.getAddress(),
				  request.getPinCode(),
				  request.getPassword().trim(),
				  request.getUserName().trim(),
				  request.getIpAddress(),
				  request.getCountryId(),
				  request.getStateId(),
				  request.getCityId(),
				  new CommonMethods().currentDate(),
				  isBuyer,
				  isSeller)

		  .returning(Users.USERS.ID)
		  .fetchOne();
		 
		 return record.getId();
	}
	
	/**
	 * @param dslContext
	 * @param string 
	 * @param mobileNumber
	 * @param isSeller
	 * @return 
	 * @throws Exception
	 */
	public Result<Record> checkBuyerUserExistOrNot(DSLContext dslContext, SmsRequestModel smsRequestModel, String userName) throws Exception{
		String isBuyer = "N";
		if (smsRequestModel.getIsBuyer()) {
			isBuyer = "Y";
		}
		return dslContext.select().from(Users.USERS).where(Users.USERS.USER_MOBILE.eq(String.valueOf(smsRequestModel.getMobileNumber())))
		.or(Users.USERS.USERNAME.eq(userName))
		//.and(Users.USERS.IS_BYERS.eq(isBuyer))
		.and(Users.USERS.ACTIVE.eq("Y"))
		.fetch();
	}
	
}
