/**
 * 
 */
package com.tph.dao;

import java.math.BigDecimal;
import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;

import com.tph.commonmethods.CommonMethods;
import com.tph.constants.ConstantKeys;
import com.tph.jooq.tph_db.tables.CityMaster;
import com.tph.jooq.tph_db.tables.CountryMaster;
import com.tph.jooq.tph_db.tables.IndustrialProductServices;
import com.tph.jooq.tph_db.tables.IpProductList;
import com.tph.jooq.tph_db.tables.MakeAnOffer;
import com.tph.jooq.tph_db.tables.PaymentDetail;
import com.tph.jooq.tph_db.tables.ProductDetail;
import com.tph.jooq.tph_db.tables.StateMaster;
import com.tph.jooq.tph_db.tables.UserProductViewer;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.IndustrialProductServicesRecord;
import com.tph.jooq.tph_db.tables.records.IpProductListRecord;
import com.tph.jooq.tph_db.tables.records.PaymentDetailRecord;
import com.tph.jooq.tph_db.tables.records.ProductDetailRecord;
import com.tph.request.model.EditProfilleRequest;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.PhotoVideoPath;

/**
 * @author majidkhan
 *
 */
public class SellerDashboardDao {

	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public Integer fetchTotalProduct(DSLContext dslContext, Integer userId) throws Exception {
		
		Integer prodDetailCount = dslContext.selectFrom(ProductDetail.PRODUCT_DETAIL)
				.where(ProductDetail.PRODUCT_DETAIL.USER_ID.eq(userId))
				.and(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y"))
				.fetchCount();
		
		Integer ipDetailCount =  dslContext.selectFrom(IpProductList.IP_PRODUCT_LIST)
		.where(IpProductList.IP_PRODUCT_LIST.USER_ID.eq(userId))
		.and(IpProductList.IP_PRODUCT_LIST.IS_ACTIVE.eq("Y"))
		.fetchCount();
		
		return prodDetailCount+ipDetailCount;
	}

	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public Integer fetchTotalOffers(DSLContext dslContext, Integer userId) throws Exception {

		return dslContext.selectFrom(MakeAnOffer.MAKE_AN_OFFER)
				.where(MakeAnOffer.MAKE_AN_OFFER.SELLER_USER_ID.eq(userId)).fetchCount();
	}
	
	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public Integer fetchTotalProductsold(DSLContext dslContext, Integer userId) throws Exception {

		return dslContext.select().from(ProductDetail.PRODUCT_DETAIL)
		.where(ProductDetail.PRODUCT_DETAIL.USER_ID.eq(userId))
		.and(ProductDetail.PRODUCT_DETAIL.REMARKS.eq("SOLD"))
		.fetchCount();
		
	}
	
	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public Integer fetchTotalProductView(DSLContext dslContext, Integer userId) throws Exception {
		Integer count = 0;

		return dslContext.selectFrom(UserProductViewer.USER_PRODUCT_VIEWER)
				.where(UserProductViewer.USER_PRODUCT_VIEWER.PRODUCT_USER_ID.eq(userId)).fetchCount();

		/*
		 * String query =
		 * "select count(1) as total from tph_db.product_detail where user_id in(select user_id from tph_db.user_activity_log where user_id="
		 * + userId + ")"; count =
		 * Integer.valueOf(dslContext.fetch(query).getValues("total").get(0).toString())
		 * ; return count;
		 */
	}
	
	/**
	 * @param dslContext
	 * @param sellerAddNewPostRequestModel
	 * @throws Exception
	 */
	public ProductDetailRecord saveNewPost(DSLContext dslContext, SellerAddNewPostRequestModel sellerAddNewPostRequestModel)
			throws Exception {
		
		Integer subCatId = 0;
		Double pricePerUnit = 00.0;
			if (sellerAddNewPostRequestModel.getSubCatTypeId()!=null) {
				subCatId = sellerAddNewPostRequestModel.getSubCatTypeId();
			}
		
			String mobileNumber= null, alternateMobileNumber = null;
			
			if (sellerAddNewPostRequestModel.getMobileNumber() != null) {
				mobileNumber = sellerAddNewPostRequestModel.getMobileNumber();
			}
			
			if (sellerAddNewPostRequestModel.getAlterMobileNumber() != null) {
				alternateMobileNumber = sellerAddNewPostRequestModel.getAlterMobileNumber();
			}
			
			if (sellerAddNewPostRequestModel.getPricePerUnit() != null) {
				pricePerUnit = sellerAddNewPostRequestModel.getPricePerUnit();
			}
			
		  ProductDetailRecord record =
		  dslContext.insertInto(ProductDetail.PRODUCT_DETAIL,
		  ProductDetail.PRODUCT_DETAIL.INDUSTRIAL_TYPE_ID,
		  ProductDetail.PRODUCT_DETAIL.CATEGORY_ID,
		  ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID,
		  ProductDetail.PRODUCT_DETAIL.PRODUCT_NAME,
		  ProductDetail.PRODUCT_DETAIL.PRODUCT_DESCRIPTION,
		  ProductDetail.PRODUCT_DETAIL.PRODUCT_BRAND,
		  ProductDetail.PRODUCT_DETAIL.MFG_PURCHASE_YEAR,
		  ProductDetail.PRODUCT_DETAIL.QUANTITY, 
		  ProductDetail.PRODUCT_DETAIL.UOM_ID,
		  ProductDetail.PRODUCT_DETAIL.PRICE_PER_UNIT,
		  ProductDetail.PRODUCT_DETAIL.PRICE_NEGOTIABLE,
		  ProductDetail.PRODUCT_DETAIL.PROD_LOC,
		  ProductDetail.PRODUCT_DETAIL.COUNTRY_ID,
		  ProductDetail.PRODUCT_DETAIL.STATE_ID,
		  ProductDetail.PRODUCT_DETAIL.CITY_ID, 
		  ProductDetail.PRODUCT_DETAIL.USER_ID,
		  ProductDetail.PRODUCT_DETAIL.PRODUCT_STATUS_ID,
		  ProductDetail.PRODUCT_DETAIL.MOBILE_NO,
		  ProductDetail.PRODUCT_DETAIL.ALTERNATE_MOBILE_NO)
		  
		  .values(sellerAddNewPostRequestModel.getIndustrialTypeId(),
		  sellerAddNewPostRequestModel.getCatTypeId(),
		  subCatId,
		  sellerAddNewPostRequestModel.getProductTitle(), 
		  sellerAddNewPostRequestModel.getProductDetails(),
		  sellerAddNewPostRequestModel.getBrand(),
		  sellerAddNewPostRequestModel.getPurchaseYear(),
		  sellerAddNewPostRequestModel.getAvailableQuantity(),
		  sellerAddNewPostRequestModel.getUomId(),
		  new BigDecimal(pricePerUnit),
		  (byte) sellerAddNewPostRequestModel.getPriceType(),
		  sellerAddNewPostRequestModel.getProductLocation(),
		  sellerAddNewPostRequestModel.getCountryId(),
		  sellerAddNewPostRequestModel.getStateId(),
		  sellerAddNewPostRequestModel.getCityId(),
		  sellerAddNewPostRequestModel.getUserId(),
		  sellerAddNewPostRequestModel.getProductStatusId(),
		  mobileNumber,
			alternateMobileNumber)
		  .returning(ProductDetail.PRODUCT_DETAIL.ID)
		  .fetchOne();
		 
		  int returningIndex = record.getId();
		  ProductDetailRecord detailRecord = new ProductDetailRecord();
		  
		  if(returningIndex != 0) {
			  
			  String prodCode = new CommonMethods().getProductCode(returningIndex, subCatId);
			  
			// Update Payment details of listing product
			  if(prodCode.contains("TPHIR")) {
				  updatePaymentDetail(1, sellerAddNewPostRequestModel, dslContext);
			  }
			  else {
				  updatePaymentDetail(4, sellerAddNewPostRequestModel, dslContext);
			  }
			  updateProductDetailTable(returningIndex,prodCode, dslContext);
			  
			  detailRecord.setId(returningIndex);
			  detailRecord.setProductCode(prodCode);
		  }
		  
		 return detailRecord;
	}
	
	/**
	 * @param returningIndex
	 * @param prodCode 
	 * @param dslContext
	 * @throws Exception
	 */
	private int updateProductDetailTable(int returningIndex, String prodCode, DSLContext dslContext) throws Exception{
		return dslContext.update(ProductDetail.PRODUCT_DETAIL)
				.set(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE, prodCode)
				.where(ProductDetail.PRODUCT_DETAIL.ID.eq(returningIndex)).execute();
	}
	
	/**
	 * This function is using for insert data for Image/Video 
	 * @param returningIndex
	 * @param prodCode
	 * @param sellerAddNewPostRequestModel 
	 * @param dslContext
	 * @return 
	 */
	public Integer saveNewPostImage(DSLContext dslContext, SellerAddNewPostRequestModel request)
			throws Exception {
		
		String image1 = null, image2 = null, image3 = null, videoDoc = null;
		
		if(request.getPhotoVideoPathList() != null)
		{
			List<PhotoVideoPath> imageVideoList = request.getPhotoVideoPathList();
			for(int i = 0; i<imageVideoList.size(); i++)
			{
				if(i == 0 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image1 = imageVideoList.get(i).getPath();
				}
				else if(i == 1 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image2 = imageVideoList.get(i).getPath();
				}
				else if(i == 2 && imageVideoList.get(i).getItemId() == ConstantKeys.IMAGE_ITEM_ID) {
					image3 = imageVideoList.get(i).getPath();
				}
				
				if(imageVideoList.get(i).getItemId() == ConstantKeys.VIDEO_ITEM_ID) {
					videoDoc = imageVideoList.get(i).getPath();
				}
			}
		}
		
	Integer updateCounter =	dslContext.update(ProductDetail.PRODUCT_DETAIL)
		.set(ProductDetail.PRODUCT_DETAIL.IMAGE1, image1)
		.set(ProductDetail.PRODUCT_DETAIL.IMAGE2, image2)
		.set(ProductDetail.PRODUCT_DETAIL.IMAGE3, image3)
		.set(ProductDetail.PRODUCT_DETAIL.VIDEO_DOC, videoDoc)
		.where(ProductDetail.PRODUCT_DETAIL.ID.eq(request.getProductId())
		.and(ProductDetail.PRODUCT_DETAIL.USER_ID.eq(request.getUserId())))
		.and(ProductDetail.PRODUCT_DETAIL.PRODUCT_CODE.eq(request.getProductCode()))
		.execute();
		
		return updateCounter;
		
	}
	
	/**
	 * @param dslContext
	 * @param dashboardRequestModel
	 * @return
	 */
	public Result<ProductDetailRecord> fetchInventoryActivePost(DSLContext dslContext,
			SellerBaseRequestModel dashboardRequestModel) throws Exception {
		return dslContext.selectFrom(ProductDetail.PRODUCT_DETAIL)
				.where(ProductDetail.PRODUCT_DETAIL.USER_ID.eq(dashboardRequestModel.getUserId()))
				.and(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y")).fetch();

	}
	
	/**
	 * @param dslContext
	 * @param dashboardRequestModel
	 * @return
	 */
	public Result<ProductDetailRecord> fetchStockClearanceActivePost(DSLContext dslContext,
			SellerBaseRequestModel dashboardRequestModel) throws Exception {
		return dslContext.selectFrom(ProductDetail.PRODUCT_DETAIL)
				.where(ProductDetail.PRODUCT_DETAIL.USER_ID.eq(dashboardRequestModel.getUserId()))
				.and(ProductDetail.PRODUCT_DETAIL.SUB_CATEGORY_ID.eq(0))
				.and(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y")).fetch();

	}
	
	/**
	 * @param dslContext
	 * @param dashboardRequestModel
	 * @return
	 */
	public Result<IndustrialProductServicesRecord> fetchIndustrialServiceActivePost(DSLContext dslContext,
			SellerBaseRequestModel dashboardRequestModel) throws Exception {
		return dslContext.selectFrom(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES)
				.where(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.USER_ID.eq(dashboardRequestModel.getUserId()))
				.and(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IS_ACTIVE.eq("Y")).fetch();

	}
	
	/**
	 * @param dslContext
	 * @param dashboardRequestModel
	 * @return
	 */
	public Result<IpProductListRecord> fetchIndustrialPlotActivePost(DSLContext dslContext,
			SellerBaseRequestModel dashboardRequestModel) throws Exception {
		return dslContext.selectFrom(IpProductList.IP_PRODUCT_LIST)
				.where(IpProductList.IP_PRODUCT_LIST.USER_ID.eq(dashboardRequestModel.getUserId()))
				.and(IpProductList.IP_PRODUCT_LIST.IS_ACTIVE.eq("Y")).fetch();

	}

	
	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public Record fetchUserProfileData(DSLContext dslContext, Integer id) throws Exception {

		return dslContext
				.selectFrom(Users.USERS.join(CountryMaster.COUNTRY_MASTER)
						.on(Users.USERS.COUNTRY_ID.eq(CountryMaster.COUNTRY_MASTER.ID)).join(StateMaster.STATE_MASTER)
						.on(Users.USERS.STATE_ID.eq(StateMaster.STATE_MASTER.ID)).join(CityMaster.CITY_MASTER)
						.on(Users.USERS.CITY_ID.eq(CityMaster.CITY_MASTER.ID)))
				.where(Users.USERS.ID.cast(Integer.class).eq(id))
				.and(Users.USERS.ACTIVE.eq("Y").and(CityMaster.CITY_MASTER.IS_ACTIVE.eq("Y"))).fetchOne();
	}
	/**
	 * @param dslContext
	 * @return
	 * @throws Exception
	 */
	public Result<Record> fetchListingType(DSLContext dslContext) throws Exception{
		
		String query = "select * from tph_db.product_list_amount";
		Result<Record> result = dslContext.fetch(query);
		return result;
	}
	
	/**
	 * This query is using for fetch all list where seller invest the amount
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public Result<Record> fetchAllInventoryData(DSLContext dslContext, Integer userId) throws Exception{
		
		String query = " select  b.id, b.categorytype, sum(a.dashboard_remain_amt)  amount from tph_db.payment_detail a join tph_db.product_list_amount b on(a.prod_list_amt_id = b.id)  \n" + 
				"     where a.seller_user_id=  "+ userId + " and is_expiry='N' group by  b.id, b.categorytype";
		Result<Record> result = dslContext.fetch(query);
		
		return result;
	}
	
	/**
	 * @param dslContext
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public Result<Record> fetchSellerAttri(DSLContext dslContext, SellerBaseRequestModel request) throws Exception{
		String query = "select b.name,c.cat_name,d.attribute_name ,attrib_alias from tph_db.industrial_attribute_mapping a \n" + 
				"join tph_db.industrial_type b on (a.industry_type_id=b.id)\n" + 
				"join tph_db.categories_master c on (a.cat_id=c.id) join tph_db.attribute_master d "
				+ "on (a.attribute_id=d.id) where a.industry_type_id ="
				+request.getIndustrialTypeId()+" and a.cat_id="+request.getIndustrialTypeId();
		
		Result<Record> result = dslContext.fetch(query);
		return result;
	}
	
	/**
	 * @param dslContext
	 * @param sellerAddNewPostRequestModel for Industrial Plot
	 * @throws Exception
	 */
	public IpProductListRecord saveNewPostIndustrialList(DSLContext dslContext, SellerAddNewPostRequestModel sellerAddNewPostRequestModel)
			throws Exception {
		
		  IpProductListRecord record =
		  dslContext.insertInto(IpProductList.IP_PRODUCT_LIST,
				  IpProductList.IP_PRODUCT_LIST.MAIN_CATEGORY,
				  IpProductList.IP_PRODUCT_LIST.IP_TYPE_MASTER_ID,
				  IpProductList.IP_PRODUCT_LIST.IP_TITTLE,
				  IpProductList.IP_PRODUCT_LIST.IP_DESCRIPTION,
				  IpProductList.IP_PRODUCT_LIST.QUANTITY,
				  IpProductList.IP_PRODUCT_LIST.UOM_ID,
				  IpProductList.IP_PRODUCT_LIST.RATE_PER_UNIT,
				  IpProductList.IP_PRODUCT_LIST.AVAILABLE_YEAR, 
				  IpProductList.IP_PRODUCT_LIST.IP_NEGOTIABLE,
				  IpProductList.IP_PRODUCT_LIST.RESELLER_NAME,
				  IpProductList.IP_PRODUCT_LIST.USER_ID,
				  IpProductList.IP_PRODUCT_LIST.COUNTRY_ID,
				  IpProductList.IP_PRODUCT_LIST.STATE_ID,
				  IpProductList.IP_PRODUCT_LIST.CITY_ID,
				  IpProductList.IP_PRODUCT_LIST.LOCATION,
				  IpProductList.IP_PRODUCT_LIST.CONTACT_PERSON,
				  IpProductList.IP_PRODUCT_LIST.MOBILE_NO,
				  IpProductList.IP_PRODUCT_LIST.ALTERNATE_MOBILE_NO)
		  
		  .values(sellerAddNewPostRequestModel.getCategoryName(),
				  sellerAddNewPostRequestModel.getIpTypeID(),
				  sellerAddNewPostRequestModel.getProductTitle(),
				  sellerAddNewPostRequestModel.getProductDetails(),
				  sellerAddNewPostRequestModel.getAvailableQuantity(),
				  sellerAddNewPostRequestModel.getUomId(),
				  new BigDecimal(sellerAddNewPostRequestModel.getPricePerUnit()),
				  sellerAddNewPostRequestModel.getPurchaseYear(),
				  sellerAddNewPostRequestModel.getIpNegotiable(),
				  sellerAddNewPostRequestModel.getResellerName(),
				  sellerAddNewPostRequestModel.getUserId(),
				  sellerAddNewPostRequestModel.getCountryId(),
				  sellerAddNewPostRequestModel.getStateId(),
				  sellerAddNewPostRequestModel.getCityId(),
				  sellerAddNewPostRequestModel.getProductLocation(),
				  sellerAddNewPostRequestModel.getContactPerson(),
				  sellerAddNewPostRequestModel.getMobileNumber(),
				  sellerAddNewPostRequestModel.getAlterMobileNumber())

		  .returning(IpProductList.IP_PRODUCT_LIST.ID)
		  .fetchOne();
		 
		  int returningIndex = record.getId();
		  IpProductListRecord detailRecord = new IpProductListRecord();
		  
		  if(returningIndex != 0) {
			  String prodCode = new CommonMethods().getProductCodeIpProductList(returningIndex);
			  updateIndustrialPlotTable(returningIndex,prodCode, dslContext);
			  
			  detailRecord.setId(returningIndex);
			  detailRecord.setIpCode(prodCode);
			  
			  updatePaymentDetail(2, sellerAddNewPostRequestModel, dslContext);
		  }
		 return detailRecord;
	}
	
	/**
	 * @param returningIndex
	 * @param prodCode 
	 * @param dslContext
	 * @throws Exception
	 */
	private int updateIndustrialPlotTable(int returningIndex, String prodCode, DSLContext dslContext) throws Exception{
		return dslContext.update(IpProductList.IP_PRODUCT_LIST)
				.set(IpProductList.IP_PRODUCT_LIST.IP_CODE, prodCode)
				.where(IpProductList.IP_PRODUCT_LIST.ID.eq(returningIndex)).execute();
	}
	
	/**
	 * This function is using for insert data for Image/Video in Industrial Product List table
	 * @param returningIndex
	 * @param prodCode
	 * @param sellerAddNewPostRequestModel 
	 * @param dslContext
	 * @return 
	 */
	public Integer saveIndustrialProductListImage(DSLContext dslContext, SellerAddNewPostRequestModel request)
			throws Exception {
		
		int insertedValue = 0;
		String image1 = null, image2 = null, image3 = null;
		
		if(request.getPhotoVideoPathList() != null)
		{
			List<PhotoVideoPath> imageVideoList = request.getPhotoVideoPathList();
			
			for(int i = 0; i<imageVideoList.size(); i++)
			{
				if (i == 0) {
					image1 = imageVideoList.get(i).getPath();
				}
				else if (i == 1) {
					image2 = imageVideoList.get(i).getPath();
				}
				else if (i == 2) {
					image3 = imageVideoList.get(i).getPath();
				}
			}
			
			insertedValue = dslContext.update(IpProductList.IP_PRODUCT_LIST)
			.set(IpProductList.IP_PRODUCT_LIST.IMAGE_1, image1)
			.set(IpProductList.IP_PRODUCT_LIST.IMAGE_2, image2)
			.set(IpProductList.IP_PRODUCT_LIST.IMAGE_3, image3)
			.where(IpProductList.IP_PRODUCT_LIST.IP_CODE.eq(request.getProductCode())
			.and(IpProductList.IP_PRODUCT_LIST.USER_ID.eq(request.getUserId())
			.and(IpProductList.IP_PRODUCT_LIST.IS_ACTIVE.eq("Y"))))
			.execute();
		}
		
		return insertedValue;
		
	}
	
	/**
	 * @param dslContext
	 * @param sellerAddNewPostRequestModel for ServiceProvider
	 * @throws Exception
	 */
	public IndustrialProductServicesRecord saveNewPostIndustrialServiceProvider(DSLContext dslContext, SellerAddNewPostRequestModel sellerAddNewPostRequestModel)
			throws Exception {
		
		  IndustrialProductServicesRecord record =
		  dslContext.insertInto(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES,
				  IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_CATERORY_ID,
				  IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_TITLE,
				  IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_DESCRIPTION,
				  IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_MONTH,
				  IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.EXP_IN_YEAR,
				  IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.USER_ID,
				  IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.COUNTRY_ID, 
				  IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.STATE_ID,
				  IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.CITY_ID,
				  IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.MOBILE_NUMBER)
		  
		  .values(sellerAddNewPostRequestModel.getServiceCatId(),
				  sellerAddNewPostRequestModel.getProductTitle(),
				  sellerAddNewPostRequestModel.getProductDetails(),
				  sellerAddNewPostRequestModel.getExpInMonth(),
				  sellerAddNewPostRequestModel.getExpInYear(),
				  sellerAddNewPostRequestModel.getUserId(),
				  sellerAddNewPostRequestModel.getCountryId(),
				  sellerAddNewPostRequestModel.getStateId(),
				  sellerAddNewPostRequestModel.getCityId(),
				  sellerAddNewPostRequestModel.getMobileNumber())

		  .returning(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.ID)
		  .fetchOne();
		 
		  int returningIndex = record.getId();
		  IndustrialProductServicesRecord detailRecord = new IndustrialProductServicesRecord();
		  
		  if(returningIndex != 0) {
			  String prodCode = new CommonMethods().getProductCodeIndusServiceProver(returningIndex);
			  updateIndustrialProductServiceTable(returningIndex,prodCode, dslContext);
			  
			  detailRecord.setId(returningIndex);
			  detailRecord.setServiceProductCode(prodCode);
		  }
		 return detailRecord;
	}
	
	/**
	 * @param returningIndex
	 * @param prodCode 
	 * @param dslContext
	 * @throws Exception
	 */
	private int updateIndustrialProductServiceTable(int returningIndex, String prodCode, DSLContext dslContext) throws Exception{
		return dslContext.update(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES)
				.set(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_PRODUCT_CODE, prodCode)
				.where(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.ID.eq(returningIndex)).execute();
	}
	
	/**
	 * This function is using for insert data for Image/Video in Industrial Product Service table
	 * @param returningIndex
	 * @param prodCode
	 * @param sellerAddNewPostRequestModel 
	 * @param dslContext
	 * @return 
	 */
	public Integer saveIndustrialProductServiceImage(DSLContext dslContext, SellerAddNewPostRequestModel request)
			throws Exception {
		
		int insertedValue = 0;
		String image1 = null, image2 = null, image3 = null;
		
		if(request.getPhotoVideoPathList() != null)
		{
			List<PhotoVideoPath> imageVideoList = request.getPhotoVideoPathList();
			
			for(int i = 0; i<imageVideoList.size(); i++)
			{
				if (i == 0) {
					image1 = imageVideoList.get(i).getPath();
				}
				else if (i == 1) {
					image2 = imageVideoList.get(i).getPath();
				}
				else if (i == 2) {
					image3 = imageVideoList.get(i).getPath();
				}
			}
			
			insertedValue = dslContext.update(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES)
			.set(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_1, image1)
			.set(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_2, image2)
			.set(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IMAGE_3, image3)
			.where(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.SERVICE_PRODUCT_CODE.eq(request.getProductCode())
			.and(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.USER_ID.eq(request.getUserId())
			.and(IndustrialProductServices.INDUSTRIAL_PRODUCT_SERVICES.IS_ACTIVE.eq("Y"))))
			.execute();
		}
		
		return insertedValue;
	}
	
	/**
	 * @param dslContext
	 * @param editProfilleRequest
	 * @return
	 */
	public Integer updateProfileData(DSLContext dslContext, EditProfilleRequest editProfilleRequest) throws Exception {
		return dslContext.update(Users.USERS).set(Users.USERS.FIRST_NAME, editProfilleRequest.getFirstName())
				.set(Users.USERS.LAST_NAME, editProfilleRequest.getLastName())
				.set(Users.USERS.USER_MOBILE, editProfilleRequest.getMobileNumber())
				.set(Users.USERS.EMAIL, editProfilleRequest.getEmailId())
				.set(Users.USERS.GST_NO, editProfilleRequest.getGstNumber()).where(Users.USERS.ID.cast(Integer.class)
						.eq(editProfilleRequest.getUserId()).and(Users.USERS.ACTIVE.eq("Y")))
				.execute();
	}
	
private void updatePaymentDetail(int inventoryId, SellerAddNewPostRequestModel request, DSLContext dslContext) {
	Double listedAmount = null;
	
		switch (inventoryId) {
		case 2:
			
			listedAmount = request.getTotalAmount()!=null?request.getTotalAmount():(1000*request.getAvailableQuantity());
			break;
		case 1:
		case 4:
			
			listedAmount = request.getTotalAmount()!=null?request.getTotalAmount():(request.getPricePerUnit()*request.getAvailableQuantity());
			break;

		default:
			break;
		}
		
		PaymentDetailRecord record = dslContext.selectFrom(PaymentDetail.PAYMENT_DETAIL)
				.where(PaymentDetail.PAYMENT_DETAIL.SELLER_USER_ID.eq(request.getUserId()))
				.and(PaymentDetail.PAYMENT_DETAIL.PROD_LIST_AMT_ID.eq(inventoryId))
				.orderBy(PaymentDetail.PAYMENT_DETAIL.ID.desc()).limit(1).fetchOne();
		
		
		dslContext.update(PaymentDetail.PAYMENT_DETAIL)
		.set(PaymentDetail.PAYMENT_DETAIL.DASHBOARD_REMAIN_AMT, record.getDashboardRemainAmt()!=null?BigDecimal.valueOf((record.getDashboardRemainAmt().doubleValue()-listedAmount)):BigDecimal.valueOf(listedAmount))
		.where(PaymentDetail.PAYMENT_DETAIL.SELLER_USER_ID.eq(request.getUserId()))
		.and(PaymentDetail.PAYMENT_DETAIL.PROD_LIST_AMT_ID.eq(inventoryId)).execute();
		
	}
}
