package com.tph.dao;

import org.jooq.DSLContext;
import org.jooq.Record;

import com.tph.jooq.tph_db.tables.Users;
import com.tph.request.model.PasswordRequest;

public class PasswordDao {

	public Boolean checkPassword(PasswordRequest request, DSLContext dslContext) throws Exception {

		Record available = dslContext.select(Users.USERS.ID).from(Users.USERS)
				.where(Users.USERS.PASSWORD.eq(request.getCurrentPw())).and(Users.USERS.ID.eq(request.getUserId()))
				.fetchOne();

		return available != null ? true : false;
	}

	public Integer changePassword(PasswordRequest request, DSLContext dslContext) throws Exception {

		return dslContext.update(Users.USERS).set(Users.USERS.PASSWORD, request.getNewPw())
				.where(Users.USERS.ID.eq(request.getUserId())).execute();
	}
}
