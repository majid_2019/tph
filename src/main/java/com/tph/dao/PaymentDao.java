/**
 * 
 */
package com.tph.dao;

import java.math.BigDecimal;

import org.jooq.DSLContext;
import org.jooq.Record1;

import com.tph.jooq.tph_db.tables.IpProductList;
import com.tph.jooq.tph_db.tables.PaymentDetail;
import com.tph.jooq.tph_db.tables.PaymentTypeMaster;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.PaymentDetailRecord;
import com.tph.jooq.tph_db.tables.records.PaymentTransactionLogRecord;
import com.tph.request.model.PaymentDetailRequestModel;

/**
 * @author majidkhan
 *
 */
public class PaymentDao {
	/**
	 * Get Payment Type master id..
	 * @param payType
	 * @param dslContext
	 * @return
	 */
	public Integer getPgMasterId(String payType, DSLContext dslContext) throws Exception{
		
		Record1<Integer> id = dslContext.select(PaymentTypeMaster.PAYMENT_TYPE_MASTER.ID).from(PaymentTypeMaster.PAYMENT_TYPE_MASTER)
				.where(PaymentTypeMaster.PAYMENT_TYPE_MASTER.VALUE.eq(payType)).fetchOne();
		return id.getValue(PaymentTypeMaster.PAYMENT_TYPE_MASTER.ID);
	}
	
	/**
	 * Save Payment Details..
	 * @param pgId 
	 * @param request
	 * @param invoicePath 
	 * @param dslContext
	 * @throws Exception
	 */
	public Integer savePayDetails(Integer pgId, PaymentDetailRequestModel request, String invoicePath, DSLContext dslContext) throws Exception{
		
		PaymentTransactionLogRecord paymentTranLogRecord = null;
		PaymentDetailRecord record = null;
		
		if(request.getIsPaymentSuccess()) {
			paymentTranLogRecord = insertIntoPaymenTransLog(pgId, request,invoicePath,  dslContext);
			
			if(dslContext.executeInsert(paymentTranLogRecord) != 0) 
			{
				record = getPaymentDetailRecord(request, dslContext);
				
				if(record != null) {
					return updatePaymentDetails(record, request, dslContext);
				}
				else {
					record = insertIntoPaymentDetail(pgId, request, dslContext);
					return dslContext.executeInsert(record);
				}
			}
		}
		else {
			paymentTranLogRecord = insertIntoPaymenTransLog(pgId, request, invoicePath, dslContext);
			return dslContext.executeInsert(paymentTranLogRecord);
		}
		return null;
	}
	
	public PaymentDetailRecord getPaymentDetailRecord(PaymentDetailRequestModel request, DSLContext dslContext) throws Exception{
		
		PaymentDetailRecord record = dslContext.selectFrom(PaymentDetail.PAYMENT_DETAIL)
		.where(PaymentDetail.PAYMENT_DETAIL.SELLER_USER_ID.eq(request.getSellerID()))
		.and(PaymentDetail.PAYMENT_DETAIL.PROD_LIST_AMT_ID.eq(request.getProductId()))
		.orderBy(PaymentDetail.PAYMENT_DETAIL.ID.desc()).limit(1).fetchOne();
		
		return record;
	}

	/**
	 * @param pgId
	 * @param request
	 * @param invoicePath 
	 * @param dslContext
	 * @return
	 * @throws Exception
	 */
	private PaymentTransactionLogRecord insertIntoPaymenTransLog(Integer pgId, PaymentDetailRequestModel request, String invoicePath, DSLContext dslContext) throws Exception{
		PaymentTransactionLogRecord paymentLog = new PaymentTransactionLogRecord();
		
		paymentLog.setProdListAmtId(request.getProductId());
		paymentLog.setPayTypeMasterId(pgId);
		paymentLog.setAccountNumber(request.getAccountNumber());
		paymentLog.setReceiptNumber(request.getReceiptNumber());
		paymentLog.setTransId(request.getTransactionId());
		paymentLog.setSellerUserId(request.getSellerID());
		paymentLog.setBuyerUserId(request.getBuyerId());
		paymentLog.setDashboardAmount(request.getDashboardAmount());
		paymentLog.setTphBaseAmount(request.getTphBaseAmount());
		paymentLog.setTphTotalAmount(request.getTphTotalAmount());
		paymentLog.setIfscCode(request.getIfsc());
		paymentLog.setPaymentStatus(request.getPaymentStatus());
		paymentLog.setInvoicePath(invoicePath);
		
		if(request.getCgst() != null)
			paymentLog.setCgst(request.getCgst());
		
		if(request.getSgst() != null)
			paymentLog.setSgst(request.getSgst());
		
		if(request.getIgst() != null)
			paymentLog.setIgst(request.getIgst());
		
		return paymentLog;
	}
	
	
	/**
	 * @param pgId
	 * @param request
	 * @param dslContext
	 * @return
	 * @throws Exception
	 */
	private PaymentDetailRecord insertIntoPaymentDetail(Integer pgId, PaymentDetailRequestModel request, DSLContext dslContext) throws Exception{
		PaymentDetailRecord record = new PaymentDetailRecord();
		
		record.setProdListAmtId(request.getProductId());
		record.setPayTypeMasterId(pgId);
		record.setAccountNumber(request.getAccountNumber());
		record.setReceiptNumber(request.getReceiptNumber());
		record.setTransId(request.getTransactionId());
		record.setSellerUserId(request.getSellerID());
		record.setBuyerUserId(request.getBuyerId());
		record.setDashboardAmount(request.getDashboardAmount());
		record.setDashboardRemainAmt(request.getDashboardAmount());
		record.setTphBaseAmount(request.getTphBaseAmount());
		record.setTphTotalAmount(request.getTphTotalAmount());
		record.setIfscCode(request.getIfsc());
		
		if(request.getCgst() != null)
			record.setCgst(request.getCgst());
		
		if(request.getSgst() != null)
			record.setSgst(request.getSgst());
		
		if(request.getIgst() != null)
			record.setIgst(request.getIgst());
		
		return record;
	}
	
	// Update table.
	private Integer updatePaymentDetails(PaymentDetailRecord record, PaymentDetailRequestModel request,
			DSLContext dslContext) {
		return dslContext.update(PaymentDetail.PAYMENT_DETAIL)
			.set(PaymentDetail.PAYMENT_DETAIL.DASHBOARD_AMOUNT, record.getDashboardAmount()!=null?BigDecimal.valueOf((request.getDashboardAmount().doubleValue()+record.getDashboardAmount().doubleValue())):request.getDashboardAmount())
			.set(PaymentDetail.PAYMENT_DETAIL.DASHBOARD_REMAIN_AMT, record.getDashboardRemainAmt()!=null?BigDecimal.valueOf((request.getDashboardAmount().doubleValue()+record.getDashboardRemainAmt().doubleValue())):request.getDashboardAmount())
			.where(PaymentDetail.PAYMENT_DETAIL.SELLER_USER_ID.eq(request.getSellerID()))
			.and(PaymentDetail.PAYMENT_DETAIL.PROD_LIST_AMT_ID.eq(request.getProductId())).execute();
	}
	
	/**
	 * @param userId
	 * @param dslContext
	 * @throws Exception
	 */
	public Record1<Integer> isUserFromMaharashtra(Integer userId, DSLContext dslContext) throws Exception{
		return dslContext.select(Users.USERS.STATE_ID).from(Users.USERS)
		.where(Users.USERS.ID.eq(userId)).fetchOne();
	}
}
