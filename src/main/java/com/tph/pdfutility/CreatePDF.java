package com.tph.pdfutility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.color.DeviceRgb;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.tph.constants.ConstantKeys;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.request.model.PaymentDetailRequestModel;
import com.tph.services.CommonInternalQueryExecuter;
import com.tph.utility.DBConnectionUtility;
import com.tph.utility.S3uploadUtility;

public class CreatePDF {
	private static CommonInternalQueryExecuter executer = new CommonInternalQueryExecuter();
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
	
	InputStream inputStream = null;
	float headingFont = 10f;
	float itemFontSize = 8f;
	float attributeLeftPadding = 5f;
	Color oddRowColor = new DeviceRgb(226, 226, 226);
	String imagePath = "https://tph-doc-prod.s3.ap-south-1.amazonaws.com/media/invoiceImage/tph.png";
	

	public String createPdf(PaymentDetailRequestModel request) throws SQLException {
		String path = null;
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			Record record = executer.fetchUserUsingId(request, dslContext);
			 path = generatePDF(record, request);
		}
		return path;
	}
	
	private String generatePDF(Record record, PaymentDetailRequestModel request) {
		
		String componyName = record.get(Users.USERS.COMPANY);
		String contactPerson = record.get(Users.USERS.FIRST_NAME);
		String mobileNumber = record.get(Users.USERS.USER_MOBILE);
		String emailId = record.get(Users.USERS.EMAIL);
		String gstNumber = record.get(Users.USERS.GST_NO);
		String invoiceNumber = "TPHO-"+request.getReceiptNumber();
		
		System.out.println("Generating PDF---");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		ByteArrayOutputStream bOutput = new ByteArrayOutputStream();

		PdfDocument pdfDoc = new PdfDocument(new PdfWriter(bOutput));
		FileOutputStream fos = null;
		String s3ReturnPath = null;

		try {
			Document doc = new Document(pdfDoc);
			
			Table tableTopImage = new Table(2);
			Table tableDCBTitle = new Table(1);
			Table custDetailTable = new Table(2);
			Table bottomTable = new Table(1);
			Table footerOneTable = new Table(1);
			Table footerTwoTable = new Table(1);
			Table footerThreeTable = new Table(1);
			Table invoice = new Table(1);
			Table tableImage = new Table(2);
			Table tableSoldBy = new Table(2);
			Table tableHeader = new Table(1);
			
			
			invoice.addCell(new Cell().add("Invoice").setFontSize(15f).setBorder(null).setUnderline());
			invoice.setHorizontalAlignment(HorizontalAlignment.CENTER);
			invoice.setWidth(100).setMarginBottom(20);
			
			//tableImage.addCell(createImageCell(imagePath).setWidth(100).setMarginBottom(20));
			
			tableHeader.addCell(new Cell().add("Date :- "+simpleDateFormat.format(new Date())+"\nInvoice :- "+invoiceNumber).setFontSize(8));
			tableHeader.setWidth(140);
			tableHeader.setHorizontalAlignment(HorizontalAlignment.RIGHT);


			tableSoldBy.addCell(new Cell().add("Sold by:").setBorder(null).setFontSize(10f).setBold());
			tableSoldBy.addCell(new Cell().add("Bill to:").setBorder(null).setFontSize(10f).setMarginLeft(70).setBold());
			
			
			custDetailTable.addCell(new Cell().add("Trebax Solutions Pvt. Ltd. \n8,Yashoda Heights, Behind City Center Mall,\nLavate Nagar, Nashik, Maharashtra 422002\nNashik - 422010.").setBorder(null).setFontSize(8f));
			custDetailTable.addCell(new Cell().add(componyName +", \n"+contactPerson+", \n"+mobileNumber+", \n"+emailId+",\n "+gstNumber).setBorder(null).setFontSize(8f).setMarginLeft(70));

			custDetailTable.setBorder(new SolidBorder(Color.WHITE, 0.9f));
			custDetailTable.setWidthPercent(100);
			
			// adding tableTopImage in document
			doc.add(tableHeader);
			doc.add(invoice);
			doc.add(tableImage);
			doc.add(tableTopImage);
			doc.add(tableDCBTitle);
			doc.add(tableSoldBy);
			doc.add(custDetailTable);
			

			// Creating a table
			Table mainDataTable = new Table(3);

			// Adding cells to the table
			mainDataTable.addCell(
					new Cell().add("Services").setBackgroundColor(oddRowColor).setBold().setFontSize(headingFont)
							.setPaddingLeft(attributeLeftPadding).setTextAlignment(TextAlignment.LEFT));
			
			mainDataTable.addCell(new Cell().add("").setBackgroundColor(oddRowColor).setBold().setFontSize(headingFont)
					.setPaddingLeft(attributeLeftPadding).setTextAlignment(TextAlignment.LEFT));
			mainDataTable.addCell(new Cell().add("Amount").setBackgroundColor(oddRowColor).setBold().setFontSize(headingFont)
					.setPaddingLeft(attributeLeftPadding).setTextAlignment(TextAlignment.LEFT));
			
			generateDataRows(mainDataTable, request);

			// Adding Table to document
			mainDataTable.setMarginTop(10);
			doc.add(mainDataTable);
			
			bottomTable.addCell(new Cell().add("\nTerms & Conditions :").setBorder(null).setFontSize(10f).setBold().setMarginTop(10));
			bottomTable.addCell(new Cell().add("1) All disputes are subject to Nashik Jurisdiction.").setBorder(null).setFontSize(8f));
			bottomTable.addCell(new Cell().add("Declarations : We declare that this invoice shows the actual price of the services described and all the particulars mentioned therein are true and correct").setBorder(null).setFontSize(8f));
			
			footerOneTable.addCell(new Cell().add("This is a computer generated invoice and needs no signature").setBorder(null).setFontSize(10f).setMarginTop(10));
			footerTwoTable.addCell(new Cell().add("Thank you for your business!").setBorder(null).setFontSize(10f).setBold().setMarginTop(11));
			footerThreeTable.addCell(new Cell().add("Regd. Address: A-11, Rambag Apartment, Near Vidya Vikas Circle, Gangapur Road, Nashik-422013").setBorder(null).setFontSize(10f).setMarginTop(10));
			
			footerOneTable.setHorizontalAlignment(HorizontalAlignment.CENTER);
			footerOneTable.setWidth(300);
			
			footerTwoTable.setHorizontalAlignment(HorizontalAlignment.CENTER);
			footerTwoTable.setWidth(200);
			
			footerThreeTable.setHorizontalAlignment(HorizontalAlignment.CENTER);
			footerThreeTable.setWidth(500);
			
			doc.add(bottomTable);
			doc.add(footerOneTable);
			doc.add(footerTwoTable);
			doc.add(footerThreeTable);
			
		        
			doc.close();
			System.out.println("Table created successfully..");
			
			
			/*fos = new FileOutputStream(new File("statement.pdf"));
			bOutput.writeTo(fos);*/
			
			inputStream = new ByteArrayInputStream(bOutput.toByteArray());
			S3uploadUtility s3Service = new S3uploadUtility();
			s3ReturnPath = s3Service.uploadToS3(inputStream, ConstantKeys.FILE_UPLOAD_FOLDER, getFileName(request), inputStream.available());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s3ReturnPath;
		
	}

	private void generateDataRows(Table mainDataTable, PaymentDetailRequestModel request) {
		Long totalTaxAmount = 0l;
		Long total = 0l;
		
		mainDataTable.addCell(new Cell().add("Product listing service charges").setFontSize(itemFontSize)
				.setPaddingLeft(attributeLeftPadding));
		mainDataTable.addCell(new Cell().add("").setFontSize(itemFontSize)
				.setPaddingLeft(attributeLeftPadding));
		mainDataTable.addCell(new Cell().add(request.getTphBaseAmount().toString()).setFontSize(itemFontSize)
				.setPaddingLeft(attributeLeftPadding));
		
		mainDataTable.addCell(new Cell().add("").setFontSize(itemFontSize)
				.setPaddingLeft(attributeLeftPadding).setBorderBottom(Border.NO_BORDER).setBackgroundColor(Color.WHITE));
		
		
		if(request.getCgst()!=null) {
			totalTaxAmount = totalTaxAmount + request.getCgst().longValue();
			mainDataTable.addCell(new Cell().add("CGST (9%)").setFontSize(itemFontSize)
					.setPaddingLeft(attributeLeftPadding));
			mainDataTable.addCell(new Cell().add(request.getCgst().toString()).setFontSize(itemFontSize)
					.setPaddingLeft(attributeLeftPadding));
		}
		
		if(request.getSgst()!=null) {
			totalTaxAmount = totalTaxAmount + request.getSgst().longValue();
			mainDataTable.addCell(new Cell().add("").setFontSize(itemFontSize)
					.setPaddingLeft(attributeLeftPadding).setBorderBottom(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER));
			mainDataTable.addCell(new Cell().add("SGST (9%)").setFontSize(itemFontSize)
					.setPaddingLeft(attributeLeftPadding));
			mainDataTable.addCell(new Cell().add(request.getSgst().toString()).setFontSize(itemFontSize)
					.setPaddingLeft(attributeLeftPadding));
		}
		
		if(request.getIgst()!=null) {
			totalTaxAmount = totalTaxAmount + request.getIgst().longValue();
			mainDataTable.addCell(new Cell().add("").setFontSize(itemFontSize)
					.setPaddingLeft(attributeLeftPadding).setBorderBottom(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER));
			mainDataTable.addCell(new Cell().add("IGST (9%)").setFontSize(itemFontSize)
					.setPaddingLeft(attributeLeftPadding));
			mainDataTable.addCell(new Cell().add(request.getIgst().toString()).setFontSize(itemFontSize)
					.setPaddingLeft(attributeLeftPadding));
		}
		
		mainDataTable.addCell(new Cell().add("").setFontSize(itemFontSize)
				.setPaddingLeft(attributeLeftPadding).setBorderBottom(Border.NO_BORDER));
		mainDataTable.addCell(new Cell().add("Total Amount").setFontSize(itemFontSize)
				.setPaddingLeft(attributeLeftPadding).setBold());
		
		System.out.println("totalTaxAmount -- "+totalTaxAmount);
		System.out.println("request.getTphBaseAmount() -- "+request.getTphBaseAmount());
		
		total = totalTaxAmount+request.getTphBaseAmount().longValue();
		mainDataTable.addCell(new Cell().add(total.toString()).setFontSize(itemFontSize)
				.setPaddingLeft(attributeLeftPadding));
		
	}

	private static Cell createImageCell(String path) throws MalformedURLException {
       // Image img = new Image(ImageDataFactory.create(path));

		InputStream is;
		ByteArrayOutputStream bis = new ByteArrayOutputStream();
		try {
			is = new URL(path).openStream();
			 int i;
			    byte[] data = new byte[1024];
			    while ((i = is.read(data, 0, data.length)) != -1) {
			      bis.write(data, 0, i);
			    }
			    bis.flush();
			    is.close();
			    
			    
			   
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ImageData imageData = ImageDataFactory.create(bis.toByteArray());
		 Image img = new Image(imageData);
        img.setWidth(UnitValue.createPercentValue(100));
        Cell cell = new Cell().add(img);
        cell.setBorder(Border.NO_BORDER);
        return cell;
    }
	
	private String getFileName(PaymentDetailRequestModel request) {
		// TODO Auto-generated method stub
		return request.getSellerID()+"_"+request.getReceiptNumber()+"_"+new Timestamp(System.currentTimeMillis()).getTime();
	}
}
