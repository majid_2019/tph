/*
 * This file is generated by jOOQ.
*/
package com.tph.jooq.tph_db.tables;


import com.tph.jooq.tph_db.Keys;
import com.tph.jooq.tph_db.TphDb;
import com.tph.jooq.tph_db.tables.records.RefundLogRecord;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class RefundLog extends TableImpl<RefundLogRecord> {

    private static final long serialVersionUID = 2050481228;

    /**
     * The reference instance of <code>tph_db.refund_log</code>
     */
    public static final RefundLog REFUND_LOG = new RefundLog();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<RefundLogRecord> getRecordType() {
        return RefundLogRecord.class;
    }

    /**
     * The column <code>tph_db.refund_log.id</code>.
     */
    public final TableField<RefundLogRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>tph_db.refund_log.user_id</code>.
     */
    public final TableField<RefundLogRecord, Long> USER_ID = createField("user_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>tph_db.refund_log.tph_account_number</code>.
     */
    public final TableField<RefundLogRecord, String> TPH_ACCOUNT_NUMBER = createField("tph_account_number", org.jooq.impl.SQLDataType.VARCHAR.length(20), this, "");

    /**
     * The column <code>tph_db.refund_log.product_id</code>.
     */
    public final TableField<RefundLogRecord, Integer> PRODUCT_ID = createField("product_id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>tph_db.refund_log.plan_id</code>.
     */
    public final TableField<RefundLogRecord, Integer> PLAN_ID = createField("plan_id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>tph_db.refund_log.service_partner_id</code>.
     */
    public final TableField<RefundLogRecord, Integer> SERVICE_PARTNER_ID = createField("service_partner_id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>tph_db.refund_log.cat_id</code>.
     */
    public final TableField<RefundLogRecord, Integer> CAT_ID = createField("cat_id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>tph_db.refund_log.trans_id</code>.
     */
    public final TableField<RefundLogRecord, String> TRANS_ID = createField("trans_id", org.jooq.impl.SQLDataType.VARCHAR.length(30).nullable(false), this, "");

    /**
     * The column <code>tph_db.refund_log.reference_number</code>.
     */
    public final TableField<RefundLogRecord, String> REFERENCE_NUMBER = createField("reference_number", org.jooq.impl.SQLDataType.VARCHAR.length(200).nullable(false), this, "");

    /**
     * The column <code>tph_db.refund_log.bank_account_number</code>.
     */
    public final TableField<RefundLogRecord, String> BANK_ACCOUNT_NUMBER = createField("bank_account_number", org.jooq.impl.SQLDataType.VARCHAR.length(50), this, "");

    /**
     * The column <code>tph_db.refund_log.bank_ifsc</code>.
     */
    public final TableField<RefundLogRecord, String> BANK_IFSC = createField("bank_ifsc", org.jooq.impl.SQLDataType.VARCHAR.length(20), this, "");

    /**
     * The column <code>tph_db.refund_log.bank_name</code>.
     */
    public final TableField<RefundLogRecord, String> BANK_NAME = createField("bank_name", org.jooq.impl.SQLDataType.VARCHAR.length(100), this, "");

    /**
     * The column <code>tph_db.refund_log.amount</code>.
     */
    public final TableField<RefundLogRecord, Double> AMOUNT = createField("amount", org.jooq.impl.SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>tph_db.refund_log.transaction_date</code>.
     */
    public final TableField<RefundLogRecord, String> TRANSACTION_DATE = createField("transaction_date", org.jooq.impl.SQLDataType.VARCHAR.length(15), this, "");

    /**
     * The column <code>tph_db.refund_log.is_sent_for_refund</code>.
     */
    public final TableField<RefundLogRecord, Byte> IS_SENT_FOR_REFUND = createField("is_sent_for_refund", org.jooq.impl.SQLDataType.TINYINT, this, "");

    /**
     * The column <code>tph_db.refund_log.refund_status</code>.
     */
    public final TableField<RefundLogRecord, String> REFUND_STATUS = createField("refund_status", org.jooq.impl.SQLDataType.VARCHAR.length(50).defaultValue(org.jooq.impl.DSL.inline("'pending'", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>tph_db.refund_log.create_date</code>.
     */
    public final TableField<RefundLogRecord, Timestamp> CREATE_DATE = createField("create_date", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaultValue(org.jooq.impl.DSL.inline("CURRENT_TIMESTAMP", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>tph_db.refund_log.update_date</code>.
     */
    public final TableField<RefundLogRecord, Timestamp> UPDATE_DATE = createField("update_date", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaultValue(org.jooq.impl.DSL.inline("CURRENT_TIMESTAMP", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>tph_db.refund_log.payment_id</code>.
     */
    public final TableField<RefundLogRecord, Integer> PAYMENT_ID = createField("payment_id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>tph_db.refund_log.refund_type</code>.
     */
    public final TableField<RefundLogRecord, String> REFUND_TYPE = createField("refund_type", org.jooq.impl.SQLDataType.VARCHAR.length(13).defaultValue(org.jooq.impl.DSL.inline("doublePayment", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * Create a <code>tph_db.refund_log</code> table reference
     */
    public RefundLog() {
        this("refund_log", null);
    }

    /**
     * Create an aliased <code>tph_db.refund_log</code> table reference
     */
    public RefundLog(String alias) {
        this(alias, REFUND_LOG);
    }

    private RefundLog(String alias, Table<RefundLogRecord> aliased) {
        this(alias, aliased, null);
    }

    private RefundLog(String alias, Table<RefundLogRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return TphDb.TPH_DB;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<RefundLogRecord, Integer> getIdentity() {
        return Keys.IDENTITY_REFUND_LOG;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<RefundLogRecord> getPrimaryKey() {
        return Keys.KEY_REFUND_LOG_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<RefundLogRecord>> getKeys() {
        return Arrays.<UniqueKey<RefundLogRecord>>asList(Keys.KEY_REFUND_LOG_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RefundLog as(String alias) {
        return new RefundLog(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public RefundLog rename(String name) {
        return new RefundLog(name, null);
    }
}
