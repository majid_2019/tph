/**
 * 
 */
package com.tph.services;

import java.sql.Connection;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.constants.ConstantKeys;
import com.tph.dao.PaymentDao;
import com.tph.pdfutility.CreatePDF;
import com.tph.request.model.InventoryRequest;
import com.tph.request.model.PaymentDetail;
import com.tph.request.model.PaymentDetailRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.utility.DBConnectionUtility;
import com.tph.utility.PaymentUtil;

/**
 * @author majidkhan
 *
 */
public class PaymentService {
	private CommonResponseGenerator commonResGenerator = new CommonResponseGenerator();
	private PaymentDao paymentDao = new PaymentDao();

	/**
	 * Save Payment Details in payment_detail Table...
	 * 
	 * @param request
	 * @param baseResponse
	 * @return
	 */
	public BaseResponseModel savePaymentDetails(PaymentDetailRequestModel request, BaseResponseModel baseResponse) {

		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			
			Integer pgId = paymentDao.getPgMasterId(request.getPayType(), dslContext);
			String invoicePath = new CreatePDF().createPdf(request);
			Integer insertedRecord = paymentDao.savePayDetails(pgId, request, invoicePath, dslContext);

			if (insertedRecord != 0) {
				baseResponse.setStatus(ConstantKeys.SUCCESS_CODE);
				baseResponse.setMessage(ConstantKeys.SUCCESS_MSG);
			}
		} catch (Exception e) {
			e.printStackTrace();
			baseResponse = new BaseResponseModel();
			baseResponse = commonResGenerator.getExceptionResponse(baseResponse);
		}
		return baseResponse;
	}

	/**
	 * Save Payment Details in payment_detail Table...
	 * 
	 * @param request
	 * @param baseResponse
	 * @return
	 */
	public BaseResponseModel calculateAmount(InventoryRequest request, BaseResponseModel baseResponse) {

		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			Record1<Integer> result = paymentDao.isUserFromMaharashtra(request.getUserId(), dslContext);
			double sgst = 0, cgst = 0, igst = 0, tphTotalChargeAmount = 0, tphChargeAmount = 0, baseAmount = 0;
			// if State id is 12 Then user is from MAHARASHTRA..
			if (result.getValue("state_id").equals(12)) {
				// Calculate SGST && CGST
				if (request.getPercentage() != null && request.getPercentage() != 0.0 && request.getPercentage() != 0) {
					Double amount = request.getAmount();
					
					tphChargeAmount = (amount/(double) 100)* request.getPercentage();
					baseAmount = tphChargeAmount;
					
					sgst = (tphChargeAmount/(double) 100)* request.getSgst();
					cgst = (tphChargeAmount/(double) 100)* request.getSgst();
					tphTotalChargeAmount = tphChargeAmount + sgst + cgst;
				
				} else if (request.getChargeAmount() != null && request.getChargeAmount() != 0.0 && request.getChargeAmount() != 0) {
					tphChargeAmount = request.getChargeAmount();
					baseAmount = tphChargeAmount;
					
					sgst = (tphChargeAmount/(double) 100)* request.getSgst();
					cgst = (tphChargeAmount/(double) 100)* request.getSgst();
					tphTotalChargeAmount = tphChargeAmount + sgst + cgst;
				}
			} else {
				// Calculate using IGST
				if (request.getPercentage() != null && request.getPercentage() != 0.0 && request.getPercentage() != 0) {
					Double amount = request.getAmount();
					
					tphChargeAmount = (amount/(double) 100)* request.getPercentage();
					baseAmount = tphChargeAmount;
					
					igst = (tphChargeAmount/(double) 100)* request.getIgst();
					tphTotalChargeAmount = tphChargeAmount + igst;
				} else if (request.getChargeAmount() != null && request.getChargeAmount() != 0.0 && request.getChargeAmount() != 0) {
					tphChargeAmount = request.getChargeAmount();
					baseAmount = tphChargeAmount;
					
					igst = (tphChargeAmount/(double) 100)* request.getIgst();
					tphTotalChargeAmount = tphChargeAmount + igst;
				}
			}

			baseResponse.setPaymentData(commonResGenerator.getInventoryTotalPaybleAmount(sgst, cgst, tphTotalChargeAmount, igst, baseAmount));
			if (result != null) {
				baseResponse.setStatus(ConstantKeys.SUCCESS_CODE);
				baseResponse.setMessage(ConstantKeys.SUCCESS_MSG);
			}
		} catch (Exception e) {
			e.printStackTrace();
			baseResponse = new BaseResponseModel();
			baseResponse = commonResGenerator.getExceptionResponse(baseResponse);
		}
		return baseResponse;
	}
	
	public BaseResponseModel generateHash(PaymentDetail paymentDetail, BaseResponseModel response) {
		try
		{
			PaymentDetail payDetail = new PaymentUtil().populatePaymentDetail(paymentDetail);
			
			response.setStatus(ConstantKeys.SUCCESS_CODE);
			response.setMessage(ConstantKeys.SUCCESS_MSG);
			response.setPaymentDetail(payDetail);
		}
		catch(Exception e) {
			e.printStackTrace();
			response = new BaseResponseModel();
			response = commonResGenerator.getExceptionResponse(response);
		}
		return response;
	}
}
