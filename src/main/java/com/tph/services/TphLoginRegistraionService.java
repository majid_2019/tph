/**
 * 
 */
package com.tph.services;

import java.sql.Connection;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.constants.ConstantKeys;
import com.tph.dao.TphCommonDao;
import com.tph.dao.TphLoginRegistraionDao;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.UsersRecord;
import com.tph.request.model.LoginRequestModel;
import com.tph.request.model.RegistraionRequestModel;
import com.tph.request.model.SmsRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.LoginResponseModel;
import com.tph.response.model.SellerDashboardResponseModel;
import com.tph.utility.DBConnectionUtility;

/**
 * @author majidkhan
 *
 */
public class TphLoginRegistraionService {

	private CommonResponseGenerator commonResGenerator = new CommonResponseGenerator();
	private TphCommonDao commonDao = new TphCommonDao();
	private TphLoginRegistraionDao loginDao = new TphLoginRegistraionDao();
	private SmsService smsService = new SmsService();
	/**
	 * @param dashboardRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel fetchUom(BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			SellerDashboardResponseModel sellerDashboardResponseModel = new SellerDashboardResponseModel();

			sellerDashboardResponseModel.setUomList(commonResGenerator.getUonList(commonDao.uomList(dslContext)));
			/**
			 * Add Main Seller Response Model to main object model.
			 */
			sellerBaseResponseModel.setMainResponse(sellerDashboardResponseModel);
			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	
	/**
	 * @param loginRequestModel
	 * @param responseModel
	 * @return
	 */
	public BaseResponseModel login(LoginRequestModel loginRequestModel, BaseResponseModel responseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			
			 UsersRecord record = loginDao.login(dslContext, loginRequestModel);
			if (record != null && record.size() > 0) {
				if(record.getPassword().equals(loginRequestModel.getPassword())) {
					responseModel.setLoginResponse(commonResGenerator.getLoginRes(record));
					responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
				}
				else {
					responseModel.setStatus(ConstantKeys.BAD_REQUEST_CODE);
					responseModel.setMessage(ConstantKeys.INVALID_SELLER_LOGIN_PW);
				}
			} else {
				responseModel.setStatus(ConstantKeys.BAD_REQUEST_CODE);
				responseModel.setMessage(ConstantKeys.USER_NOT_EXIST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new BaseResponseModel();
			responseModel = commonResGenerator.getExceptionResponse(responseModel);
		}
		return responseModel;
	}
	
	/**
	 * @param loginRequestModel
	 * @param responseModel
	 * @return
	 */
	public BaseResponseModel registraion(RegistraionRequestModel loginRequestModel, BaseResponseModel responseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			
			SmsRequestModel smsRequestModel = new SmsRequestModel();
			smsRequestModel.setMobileNumber(Long.valueOf(loginRequestModel.getMobileNumber()));
			smsRequestModel.setIsSeller(loginRequestModel.getIsSeller());
			smsRequestModel.setIsBuyer(loginRequestModel.getIsBuyer());
			
			System.out.println("inside regis");
			if(loginRequestModel.getIsSeller() != null && loginRequestModel.getIsSeller()) {
				
				// Update Seller info agains userId
				if(loginRequestModel.getIsUpdate()!= null && loginRequestModel.getIsUpdate()) {
					Integer sellerRecord = loginDao.updateSeller(dslContext, loginRequestModel);
					System.out.println("if sellerRecord -- "+sellerRecord);
					
					if(sellerRecord!= null && sellerRecord != 0) {
						responseModel.setStatus(ConstantKeys.SUCCESS_CODE);
						responseModel.setMessage(ConstantKeys.SUCCESS_MSG);
					}
					
				}
				else {
					// Fetch Existing record of seller if exists.
					Result<Record> sellerRecord = loginDao.checkSellerUserExistOrNot(dslContext, smsRequestModel, loginRequestModel.getUserName());
				
					System.out.println("else sellerRecord --- "+sellerRecord);
					
					if (sellerRecord != null && sellerRecord.size()>0) {
						if(sellerRecord.get(0).getValue(Users.USERS.IS_SELLER) != null && sellerRecord.get(0).getValue(Users.USERS.IS_SELLER).equals("Y")) {
							responseModel.setStatus(ConstantKeys.BAD_REQUEST_CODE);
							responseModel.setMessage(ConstantKeys.SELLER_ALREDY_EXISTS);
							return responseModel;
						}
						else if(sellerRecord.get(0).getValue(Users.USERS.IS_BYERS) != null && sellerRecord.get(0).getValue(Users.USERS.IS_BYERS).equals("Y")) {
							LoginResponseModel loginResponseModel = new LoginResponseModel();
							
							responseModel.setStatus(ConstantKeys.BAD_REQUEST_CODE);
							responseModel.setMessage(ConstantKeys.SELLER_ALREDY_EXISTS_AS_BUYER);
							responseModel.setUserId(sellerRecord.get(0).getValue(Users.USERS.ID));
							
							loginResponseModel.setFirstName(sellerRecord.get(0).getValue(Users.USERS.FIRST_NAME));
							loginResponseModel.setUserId(sellerRecord.get(0).getValue(Users.USERS.ID));
							loginResponseModel.setMobileNumber(sellerRecord.get(0).getValue(Users.USERS.USER_MOBILE));
							loginResponseModel.setEmail(sellerRecord.get(0).getValue(Users.USERS.EMAIL));
							loginResponseModel.setComponyName(sellerRecord.get(0).getValue(Users.USERS.COMPANY));
							responseModel.setLoginResponse(loginResponseModel);
							
							return responseModel;
						}
					}
					else {
						Integer returnId = loginDao.registraion(dslContext, loginRequestModel);
						System.out.println("returnId -- "+returnId);
						if (returnId != null && returnId > 0) {
							responseModel.setUserId(returnId);
							responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
							// Send Welcome Message..
							smsService.sendWelcomeMsg(loginRequestModel, returnId, dslContext);
						} else {
							responseModel.setStatus(ConstantKeys.RECORD_NOT_AVAIL);
							responseModel.setMessage(ConstantKeys.USER_NOT_EXISTS);
						}
					}
				}
			}
			else if(loginRequestModel.getIsBuyer() != null && loginRequestModel.getIsBuyer()) {
				Result<Record> sellerRecord = loginDao.checkBuyerUserExistOrNot(dslContext, smsRequestModel, loginRequestModel.getUserName());
				
				if (sellerRecord == null || sellerRecord.size()<1) {
					Integer returnId = loginDao.registraion(dslContext, loginRequestModel);
					if (returnId != null && returnId > 0) {
						responseModel.setUserId(returnId);
						responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
						
						// Send Welcome Message..
						smsService.sendWelcomeMsg(loginRequestModel, returnId, dslContext);
					} else {
						responseModel.setStatus(ConstantKeys.RECORD_NOT_AVAIL);
						responseModel.setMessage(ConstantKeys.USER_NOT_EXISTS);
					}
				}
				else {
					responseModel.setStatus(ConstantKeys.BAD_REQUEST_CODE);
					responseModel.setMessage(ConstantKeys.SELLER_ALREDY_EXISTS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new BaseResponseModel();
			responseModel = commonResGenerator.getExceptionResponse(responseModel);
		}
		return responseModel;
	}
}
