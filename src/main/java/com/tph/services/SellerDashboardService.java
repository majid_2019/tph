package com.tph.services;

import java.sql.Connection;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.constants.ConstantKeys;
import com.tph.dao.SellerDashboardDao;
import com.tph.dao.TphCommonDao;
import com.tph.jooq.tph_db.tables.records.IndustrialProductServicesRecord;
import com.tph.jooq.tph_db.tables.records.IpProductListRecord;
import com.tph.jooq.tph_db.tables.records.ProductDetailRecord;
import com.tph.request.model.EditProfilleRequest;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.SellerDashboardResponseModel;
import com.tph.utility.DBConnectionUtility;

/**
 * @author majidkhan
 *
 */
public class SellerDashboardService {

	private CommonResponseGenerator commonResGenerator = new CommonResponseGenerator();
	private TphCommonDao commonDao = new TphCommonDao();
	private SellerDashboardDao dashboardDao = new SellerDashboardDao();

	public BaseResponseModel fetchDashboardData(SellerBaseRequestModel dashboardRequestModel,
			BaseResponseModel sellerBaseResponseModel) {

		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			SellerDashboardResponseModel dashboardResModel = new SellerDashboardResponseModel();

			Integer totalProduct = dashboardDao.fetchTotalProduct(dslContext, dashboardRequestModel.getUserId());
			dashboardResModel.setTotalProductList(totalProduct);

			Integer totalOffers = dashboardDao.fetchTotalOffers(dslContext, dashboardRequestModel.getUserId());
			dashboardResModel.setTotalOffers(totalOffers);

			Integer totalSoldProd = dashboardDao.fetchTotalProductsold(dslContext, dashboardRequestModel.getUserId());
			dashboardResModel.setTotalProductSold(totalSoldProd);

			Integer totalProdView = dashboardDao.fetchTotalProductView(dslContext, dashboardRequestModel.getUserId());
			dashboardResModel.setTotalProductView(totalProdView);

			dashboardResModel.setUomList(commonResGenerator.getUonList(commonDao.uomList(dslContext)));

			dashboardResModel
					.setIndustries(commonResGenerator.IndustrialTypeRecord(commonDao.fetchIdustryList(dslContext)));

			dashboardResModel.setCountryList(commonResGenerator.getCountryList(commonDao.countryList(dslContext)));
			dashboardResModel.setStateList(commonResGenerator
					.getStateList(commonDao.stateList(dslContext, dashboardRequestModel.getCountryId())));

			/**
			 * Add Main Response Model to main object model.
			 */
			sellerBaseResponseModel.setMainResponse(dashboardResModel);
			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);

			// Result<Record> result = cmnQueryExecuter.fetchAllIndustrialData(dslContext);
			// commonResponseGenerator.fetchIndusrialList(result);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerAddNewPostRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel saveNewPost(SellerAddNewPostRequestModel request,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			ProductDetailRecord inventoryPost = null;

			inventoryPost = saveInventoryPost(dslContext, request);

			if (inventoryPost != null && inventoryPost.getId() != 0) {
				SellerDashboardResponseModel dashboardResModel = new SellerDashboardResponseModel();
				dashboardResModel.setProductId(inventoryPost.getId());
				dashboardResModel.setProductCode(inventoryPost.getProductCode());

				/**
				 * Add Main Response Model to main object model.
				 */
				sellerBaseResponseModel.setMainResponse(dashboardResModel);
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			} else {
				sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				sellerBaseResponseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	private ProductDetailRecord saveInventoryPost(DSLContext dslContext, SellerAddNewPostRequestModel request)
			throws Exception {
		ProductDetailRecord productDetailRecord = dashboardDao.saveNewPost(dslContext, request);
		return productDetailRecord;
	}

	/**
	 * @param sellerAddNewPostRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel saveNewPostImage(SellerAddNewPostRequestModel sellerAddNewPostRequestModel,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			int returnIndex = dashboardDao.saveNewPostImage(dslContext, sellerAddNewPostRequestModel);
			if (returnIndex != 0) {
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			} else {
				sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				sellerBaseResponseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param dashboardRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel fetchActivePost(SellerBaseRequestModel dashboardRequestModel,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			if (dashboardRequestModel.getServiceType().equalsIgnoreCase(ConstantKeys.INVENTORY)) {
				Result<ProductDetailRecord> record = dashboardDao.fetchInventoryActivePost(dslContext, dashboardRequestModel);

				sellerBaseResponseModel.setActivePostList(commonResGenerator.activePost(record));
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			}
			else if (dashboardRequestModel.getServiceType().equalsIgnoreCase(ConstantKeys.STOCK_CLEARANCE)) {
				Result<ProductDetailRecord> record = dashboardDao.fetchStockClearanceActivePost(dslContext, dashboardRequestModel);

				sellerBaseResponseModel.setActivePostList(commonResGenerator.activePost(record));
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			}else if (dashboardRequestModel.getServiceType().equalsIgnoreCase(ConstantKeys.INDUSTRIAL_SERVICES)) {
				Result<IndustrialProductServicesRecord> record = dashboardDao.fetchIndustrialServiceActivePost(dslContext, dashboardRequestModel);

				sellerBaseResponseModel.setActivePostList(commonResGenerator.activePostIndustrialServiceData(record));
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			}
			else if (dashboardRequestModel.getServiceType().equalsIgnoreCase(ConstantKeys.INDUSTRIAL_PLOT)) {
				Result<IpProductListRecord> record = dashboardDao.fetchIndustrialPlotActivePost(dslContext, dashboardRequestModel);

				sellerBaseResponseModel.setActivePostList(commonResGenerator.activePostIndustrialPlotData(record));
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerAddNewPostRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel fetchProfileData(SellerBaseRequestModel sellerBaseRequestModel,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			/**
			 * Add Main Response Model to main object model.
			 */
			sellerBaseResponseModel.setProfileView(commonResGenerator
					.getProfileData(dashboardDao.fetchUserProfileData(dslContext, sellerBaseRequestModel.getUserId())));

			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param editProfilleRequest
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel updateProfileData(EditProfilleRequest editProfilleRequest,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			/**
			 * Add Main Response Model to main object model.
			 */

			if (dashboardDao.updateProfileData(dslContext, editProfilleRequest) != 0) {
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			} else {
				sellerBaseResponseModel = commonResGenerator.getBasReqMsgResponse(sellerBaseResponseModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param dashboardRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel fetchSellerAllInventory(SellerBaseRequestModel request,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			Result<Record> record = dashboardDao.fetchListingType(dslContext);

			Result<Record> result = dashboardDao.fetchAllInventoryData(dslContext, request.getUserId());
			
			sellerBaseResponseModel.setAllInventoryList(commonResGenerator.getInventoryDetails(result));
			sellerBaseResponseModel.setInventries(commonResGenerator.getInventoryList(record));
			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param dashboardRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel fetchSellerFormAttributes(SellerBaseRequestModel request,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			/**
			 * Add Main Response Model to main object model.
			 */
			Result<Record> result = dashboardDao.fetchSellerAttri(dslContext, request);

			sellerBaseResponseModel.setSellrAllAttributesList(commonResGenerator.getAllAttrib(result));

			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerAddNewPostRequestModel For Industrial Plot.
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel saveNewindustrialProductList(SellerAddNewPostRequestModel sellerAddNewPostRequestModel,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			IpProductListRecord ipProductListRecord = dashboardDao.saveNewPostIndustrialList(dslContext,
					sellerAddNewPostRequestModel);
			if (ipProductListRecord != null && ipProductListRecord.getId() != 0) {
				SellerDashboardResponseModel dashboardResModel = new SellerDashboardResponseModel();
				dashboardResModel.setProductId(ipProductListRecord.getId());
				dashboardResModel.setProductCode(ipProductListRecord.getIpCode());

				/**
				 * Add Main Response Model to main object model.
				 */
				sellerBaseResponseModel.setMainResponse(dashboardResModel);
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			} else {
				sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				sellerBaseResponseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerAddNewPostRequestModel for Industrial Product List table..
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel saveIndustrialProductListImage(SellerAddNewPostRequestModel sellerAddNewPostRequestModel,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			int returnIndex = dashboardDao.saveIndustrialProductListImage(dslContext, sellerAddNewPostRequestModel);
			System.out.println("-- Save Seller IndustrialProduct List Post Image return index is -- " + returnIndex);
			if (returnIndex != 0) {
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			} else {
				sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				sellerBaseResponseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerAddNewPostRequestModel For industrial_product_services.
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel saveIndustrialProdService(SellerAddNewPostRequestModel sellerAddNewPostRequestModel,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			IndustrialProductServicesRecord record = dashboardDao.saveNewPostIndustrialServiceProvider(dslContext,
					sellerAddNewPostRequestModel);
			if (record != null && record.getId() != 0) {
				SellerDashboardResponseModel dashboardResModel = new SellerDashboardResponseModel();
				dashboardResModel.setProductId(record.getId());
				dashboardResModel.setProductCode(record.getServiceProductCode());

				/**
				 * Add Main Response Model to main object model.
				 */
				sellerBaseResponseModel.setMainResponse(dashboardResModel);
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			} else {
				sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				sellerBaseResponseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerAddNewPostRequestModel for Industrial Product List table..
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel saveIndustrialProductServiceImage(
			SellerAddNewPostRequestModel sellerAddNewPostRequestModel, BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			int returnIndex = dashboardDao.saveIndustrialProductServiceImage(dslContext, sellerAddNewPostRequestModel);
			System.out.println("-- Save Seller IndustrialProduct Service Image return index is -- " + returnIndex);
			if (returnIndex != 0) {
				sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			} else {
				sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				sellerBaseResponseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}
}
