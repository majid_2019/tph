package com.tph.services;

import java.sql.Connection;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.commonmethods.CommonMethods;
import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.constants.CommunicationCategory;
import com.tph.constants.NotificationRequestType;
import com.tph.dao.NotificationDao;
import com.tph.dao.TphCommonDao;
import com.tph.jooq.tph_db.tables.CentralCommunicationTemplate;
import com.tph.request.model.NotificationRequest;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.SellerDashboardResponseModel;
import com.tph.utility.DBConnectionUtility;

public class NotificationService {
	private static final CommonResponseGenerator commonResGenerator = new CommonResponseGenerator();
	private static final CommonMethods comnMethod = new CommonMethods();
	private static final NotificationDao dao = new NotificationDao();
	private TphCommonDao comnDao = new TphCommonDao();

	public BaseResponseModel sendNotification(NotificationRequest request) {
		BaseResponseModel response = new BaseResponseModel();
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			Record record = null;
			String notificationContent = null;
			String finalNotificationBody = null;
			String commCategory = null;

			if (request.getNotificationType().equals(NotificationRequestType.MAKE_AN_OFFER.toString())) {
				commCategory = CommunicationCategory.valueOf(CommunicationCategory.NOTIFICATION_MKN_OFFER.name()).getCommunicationCategory();
				record = dao.getMkNOfferNotificationTemplete(dslContext, commCategory);
				notificationContent = record.getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT);
				finalNotificationBody = comnMethod.getMKNOfferNotiBody(notificationContent, request);
			} else if (request.getNotificationType().equals(NotificationRequestType.CONTACT_SELLER.toString())) {
				commCategory = CommunicationCategory.valueOf(CommunicationCategory.NOTIFICATION_CONTACT_SELLER.name()).getCommunicationCategory();
				record = dao.getMkNOfferNotificationTemplete(dslContext, commCategory);
				notificationContent = record.getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT);
				finalNotificationBody = comnMethod.getContSellerNotiBody(notificationContent, request);
			}

			System.out.println("final Notification Body -- " + finalNotificationBody);

			request.setNotificationBody(finalNotificationBody);
			request.setTemplateId(
					record.getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.ID).toString());
			request.setCommType(
					record.getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_TYPE));

			dao.insertNotificationMKNOfferIntoTable(dslContext, request, commCategory);
		} catch (Exception e) {
			e.printStackTrace();
			response = new BaseResponseModel();
			response = commonResGenerator.getExceptionResponse(response);
		}
		return response;
	}
	
	/**
	 * @param requestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel fetchNotification(SellerBaseRequestModel requestModel, BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			SellerDashboardResponseModel dashboardResModel = new SellerDashboardResponseModel();
			Result<Record2<String, String>> smsRecord = comnDao.fetchSMSCommLog(requestModel.getUserId(), dslContext);
			Result<Record2<String, String>> notiContactSellerRecord = comnDao.fetchContactSellerNotiCommLog(requestModel.getUserId(), dslContext);
			Result<Record2<String, String>> notiMknOfferRecord = comnDao.fetchMknOfferNotiCommLog(requestModel.getUserId(), dslContext);
			
			dashboardResModel.setMessages(commonResGenerator.getNotifyResponse(smsRecord));
			dashboardResModel.setNotification(commonResGenerator.getNotifyResponse(notiContactSellerRecord));
			dashboardResModel.setOffers(commonResGenerator.getNotifyResponse(notiMknOfferRecord));
			
			/**
			 * Add Main Response Model to main object model.
			 */
			sellerBaseResponseModel.setMainResponse(dashboardResModel);
			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}

		return sellerBaseResponseModel;
	}
}
