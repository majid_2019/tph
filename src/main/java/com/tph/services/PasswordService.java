package com.tph.services;

import java.sql.Connection;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.constants.ConstantKeys;
import com.tph.constants.Password;
import com.tph.dao.PasswordDao;
import com.tph.request.model.PasswordRequest;
import com.tph.response.model.BaseResponseModel;
import com.tph.utility.DBConnectionUtility;

public class PasswordService {
	private static PasswordDao passwordDao = new PasswordDao();

	public BaseResponseModel changePassword(PasswordRequest request) {
		BaseResponseModel responseModel = new BaseResponseModel();
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			if (request.getAction().equalsIgnoreCase(String.valueOf(Password.CHANGE_PASSWORD))) {
				if (!passwordDao.checkPassword(request, dslContext)) {
					responseModel.setStatus(ConstantKeys.RECORD_NOT_AVAIL);
					responseModel.setMessage(ConstantKeys.OLD_PW_NOT_VALID);

					return responseModel;
				}

				if (passwordDao.changePassword(request, dslContext) == 1) {
					responseModel.setStatus(ConstantKeys.SUCCESS_CODE);
					responseModel.setMessage(ConstantKeys.PW_CHANGE_SUCCESSFULLY);

					return responseModel;
				}
			} else if (request.getAction().equalsIgnoreCase(String.valueOf(Password.FORGOT_PASSWORD))) {
				if (passwordDao.changePassword(request, dslContext) == 1) {
					responseModel.setStatus(ConstantKeys.SUCCESS_CODE);
					responseModel.setMessage(ConstantKeys.PW_CHANGE_SUCCESSFULLY);

					return responseModel;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

}
