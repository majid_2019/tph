/**
 * 
 */
package com.tph.services;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.commonmethods.CommonMethods;
import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.constants.ConstantKeys;
import com.tph.dao.TphHomePageDao;
import com.tph.jooq.tph_db.tables.records.ProductLikeShareRecord;
import com.tph.request.model.AddBuyerRequirmentReq;
import com.tph.request.model.CommonRequestModel;
import com.tph.request.model.MakeAnOfferRquest;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.HomePageResponseModel;
import com.tph.response.model.ProductDetailResModel;
import com.tph.response.model.SellerDetails;
import com.tph.utility.DBConnectionUtility;

/**
 * @author majidkhan
 *
 */
public class TphHomePageService {

	private TphHomePageDao homePageDao = new TphHomePageDao();
	private CommonResponseGenerator commonResGenerator = new CommonResponseGenerator();
	private CommonMethods cmnMethod = new CommonMethods();
	private CommonInternalQueryExecuter commonQuery = new CommonInternalQueryExecuter();
	
	/**
	 * 
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel fetchHomePageData(BaseResponseModel baseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			HomePageResponseModel homePageResponseModel = new HomePageResponseModel();
			//Map<Integer, String> imagePathMap;
			
			// Fetch Featured Product List..
			Result<Record> featuredProductList = homePageDao.featuredProductList(dslContext);
			//productIdList = cmnMethod.getProductIdIdex(featuredProductList);
			//Result<Record2<Integer, String>> featuredProdImage = cmnQueryExecuter.fetchHomePageProdImage(dslContext, productIdList);
			//imagePathMap = commonResGenerator.generateImageMap(featuredProdImage);
			homePageResponseModel.setFeaturedProductList(commonResGenerator.getFeaturedProd(featuredProductList));
			
			// Fetch RecentAdded Product List..
			Result<Record> recentAddedProductList = homePageDao.recentAddedProductList(dslContext);
			//productIdList = cmnMethod.getProductIdIdex(recentAddedProductList);
			//Result<Record2<Integer, String>> recentAddedProdImage = cmnQueryExecuter.fetchHomePageProdImage(dslContext, productIdList);
			//imagePathMap = commonResGenerator.generateImageMap(recentAddedProdImage);
			homePageResponseModel.setRecentAddedProductList(commonResGenerator.getRecentAddedProd(recentAddedProductList));
			
			// Fetch Stock ClearanceLeads Product List..
			Result<Record> stockClearanceLeadsList = homePageDao.stockClearanceLeadsList(dslContext);
			//productIdList = cmnMethod.getProductIdIdex(stockClearanceLeadsList);
			//Result<Record2<Integer, String>> stockClearanceProdImage = cmnQueryExecuter.fetchHomePageProdImage(dslContext, productIdList);
			//imagePathMap = commonResGenerator.generateImageMap(stockClearanceProdImage);
			homePageResponseModel.setStockClearanceLeadsList(commonResGenerator.getStockClearanceLeadsData(stockClearanceLeadsList));
			
			// Fetch Stock IndustrialServiceList Product List..
			Result<Record> industrialServicesList = homePageDao.industrialServicesList(dslContext);
			homePageResponseModel.setIndustrialServicesList(commonResGenerator.getIndustrialServicesData(industrialServicesList));
			
			// Fetch Stock IndustrialServiceList Plot List..
			Result<?> industrialPlotList = homePageDao.recentlyAddedIndustrialPlotList(dslContext);
			homePageResponseModel.setIndustrialPlotList(commonResGenerator.getIndustrialPlot(industrialPlotList));
			
			Result<Record> soldedProductList = homePageDao.recentlySoleProdList(dslContext);
			homePageResponseModel.setSoldProductList(commonResGenerator.getSoldProductData(soldedProductList));
			
			homePageResponseModel.setInventoryProducts(20000L);
			homePageResponseModel.setServiceProvider(150L);
			homePageResponseModel.setHappyClients(900L);
			homePageResponseModel.setBuyerCornerReqList(commonResGenerator.getBuyerCornerList(homePageDao.fetchBuyersCornerRequirmnt(dslContext)));
			
			/**
			 * Add Main Seller Response Model to main object model.
			 */
			baseResponseModel.setHomePageMainResponse(homePageResponseModel);
			baseResponseModel = commonResGenerator.getSuccessMsgResponse(baseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			baseResponseModel = new BaseResponseModel();
			baseResponseModel = commonResGenerator.getExceptionResponse(baseResponseModel);
		}
		return baseResponseModel;
	}

	/**
	 * Fetch all product details.
	 * @param baseRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel dashProductDetails(CommonRequestModel baseRequestModel,
			BaseResponseModel baseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			
			switch (baseRequestModel.getServiceType()) {
			// This case for both INVENTORY and STOCK Clearance
			case ConstantKeys.INVENTORY:
			case ConstantKeys.STOCK_CLEARANCE:
				
				return inventoryProdDetails(baseRequestModel, baseResponseModel, dslContext);
				
			case ConstantKeys.INDUSTRIAL_SERVICES:
				
				return industrialServiceProdDetails(baseRequestModel, baseResponseModel, dslContext);
				
			case ConstantKeys.INDUSTRIAL_PLOT:
				
				return industrialPlotProdDetails(baseRequestModel, baseResponseModel, dslContext);
				
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			baseResponseModel = new BaseResponseModel();
			baseResponseModel = commonResGenerator.getExceptionResponse(baseResponseModel);
		}
		return baseResponseModel;
	}
	
	/**
	 * Invetory Prod Details...
	 * @param baseRequestModel
	 * @param dslContext 
	 * @return
	 */
	private BaseResponseModel inventoryProdDetails(CommonRequestModel baseRequestModel, BaseResponseModel baseResponseModel, DSLContext dslContext) throws Exception{
		HomePageResponseModel homePageResponseModel = new HomePageResponseModel();
		
		List<Integer> productIdList;
		Map<Integer, String> imagePathMap;

		Result<Record> result = homePageDao.fetchSingleProductDetal(baseRequestModel,dslContext);
		System.out.println("result --"+result);
		//Result<Record2<String, String>> imageVideoList = cmnQueryExecuter.fetchSingleProdctImageVideo(dslContext, baseRequestModel.getProductId());
		
		ProductDetailResModel productDetailResModel = commonResGenerator.getSingleProdDetails(result);
		SellerDetails sellerDetails = commonResGenerator.getSellerDetails(result);
		
		homePageResponseModel.setDetails(productDetailResModel);
		homePageResponseModel.setSellerDetail(sellerDetails);
		
		Result<Record> featuredProductList = homePageDao.relatedProductList(baseRequestModel.getSubCategoryId(), dslContext);
		productIdList = cmnMethod.getProductIdIdex(featuredProductList);
		//Result<Record2<Integer, String>> indusServiceProdImage = cmnQueryExecuter.fetchHomePageProdImage(dslContext, productIdList);
		//imagePathMap = commonResGenerator.generateImageMap(indusServiceProdImage);
		
		homePageResponseModel.setRelatedProductList(commonResGenerator.getFeaturedProd(featuredProductList));
		
		/**
		 * Add Main Seller Response Model to main object model.
		 */
		baseResponseModel.setHomePageMainResponse(homePageResponseModel);
		baseResponseModel = commonResGenerator.getSuccessMsgResponse(baseResponseModel);
		
		// Insert logs in ActivityLog
		commonQuery.insertInActivityLog(sellerDetails, productDetailResModel.getItemCode(), dslContext);
		
		return baseResponseModel;
	}
	
	/**
	 * Invetory Prod Details...
	 * @param baseRequestModel
	 * @param dslContext 
	 * @return
	 */
	private BaseResponseModel industrialServiceProdDetails(CommonRequestModel baseRequestModel, BaseResponseModel baseResponseModel, DSLContext dslContext) throws Exception{
		HomePageResponseModel homePageResponseModel = new HomePageResponseModel();
		
		Result<Record> result = homePageDao.fetchSingleProductDetalIndustrialService(baseRequestModel, dslContext);
		
		ProductDetailResModel productDetailResModel = commonResGenerator.getIndustrialServiceSingleProdDetails(result);
		SellerDetails sellerDetails = commonResGenerator.getSellerDetails(result);
		
		homePageResponseModel.setDetails(productDetailResModel);
		homePageResponseModel.setSellerDetail(sellerDetails);
		
		/**
		 * Add Main Seller Response Model to main object model.
		 */
		baseResponseModel.setHomePageMainResponse(homePageResponseModel);
		baseResponseModel = commonResGenerator.getSuccessMsgResponse(baseResponseModel);
		
		// Insert logs in ActivityLog
		commonQuery.insertInActivityLog(sellerDetails, productDetailResModel.getItemCode(), dslContext);
		
		return baseResponseModel;
	}
	
	/**
	 * Invetory Prod Details...
	 * @param baseRequestModel
	 * @param dslContext 
	 * @return
	 */
	private BaseResponseModel industrialPlotProdDetails(CommonRequestModel baseRequestModel, BaseResponseModel baseResponseModel, DSLContext dslContext) throws Exception{
		HomePageResponseModel homePageResponseModel = new HomePageResponseModel();
		
		Result<Record> result = homePageDao.fetchSingleProductDetalIndustrialPlot(baseRequestModel, dslContext);
		
		ProductDetailResModel productDetailResModel = commonResGenerator.getIndustrialPlotSingleProdDetails(result);
		SellerDetails sellerDetails = commonResGenerator.getSellerDetails(result);
		
		homePageResponseModel.setDetails(productDetailResModel);
		homePageResponseModel.setSellerDetail(sellerDetails);
		
		/**
		 * Add Main Seller Response Model to main object model.
		 */
		baseResponseModel.setHomePageMainResponse(homePageResponseModel);
		baseResponseModel = commonResGenerator.getSuccessMsgResponse(baseResponseModel);
		
		// Insert logs in ActivityLog
		commonQuery.insertInActivityLog(sellerDetails, productDetailResModel.getItemCode(), dslContext);
		
		return baseResponseModel;
	}

	/**
	 * 
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel fetchAllBuyersRequirements(BaseResponseModel baseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			HomePageResponseModel homePageResponseModel = new HomePageResponseModel();

			homePageResponseModel.setBuyerCornerReqList(commonResGenerator.getBuyerCornerList(homePageDao.fetchAllBuyersCornerRequirmnt(dslContext)));
			
			/**
			 * Add Main Seller Response Model to main object model.
			 */
			baseResponseModel.setHomePageMainResponse(homePageResponseModel);
			baseResponseModel = commonResGenerator.getSuccessMsgResponse(baseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			baseResponseModel = new BaseResponseModel();
			baseResponseModel = commonResGenerator.getExceptionResponse(baseResponseModel);
		}
		return baseResponseModel;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public BaseResponseModel addBuyersRequirmnts(AddBuyerRequirmentReq request, BaseResponseModel response){
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			Result<Record> userResult = homePageDao.fetchUserDataWithId(request,dslContext);
			if(userResult.size() == 0) {
				response.setStatus(ConstantKeys.BAD_REQUEST_CODE);
				response.setMessage(ConstantKeys.BUYER_NOT_AVAIL);
			}
			else {
				Integer count = homePageDao.addBuyersRequirmnt(request,userResult,dslContext);
				if (count != 0) {
					response = commonResGenerator.getSuccessMsgResponse(response);
				} else {
					response.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
					response.setMessage(ConstantKeys.NOT_SAVE);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new BaseResponseModel();
			response = commonResGenerator.getExceptionResponse(response);
		}
		return response;
	}

	/**
	 * Fetch all product list of Selected SubCategory List..
	 * @param baseRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 * @throws Exception
	 */
	public BaseResponseModel fetchSubCatProdList(SellerBaseRequestModel request,
			BaseResponseModel baseResponseModel) throws Exception{
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			HomePageResponseModel homePageResponseModel = new HomePageResponseModel();
			
			Result<?> fetchAllSubCateProdList = homePageDao.fetchAllSubCateProdList(request, dslContext);
			homePageResponseModel.setSubCatProductList(commonResGenerator.getSubCatList(fetchAllSubCateProdList));
			
			/**
			 * Add Main Seller Response Model to main object model.
			 */
			baseResponseModel.setHomePageMainResponse(homePageResponseModel);
			baseResponseModel = commonResGenerator.getSuccessMsgResponse(baseResponseModel);
		
		
		} catch (Exception e) {
			e.printStackTrace();
			baseResponseModel = new BaseResponseModel();
			baseResponseModel = commonResGenerator.getExceptionResponse(baseResponseModel);
		}
		return baseResponseModel;
	}

	/**
	 * Fetch Industrial Plot
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel industrialPlot(BaseResponseModel baseResponseModel){

		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			HomePageResponseModel homePageResponseModel = new HomePageResponseModel();
			
			// Fetch Industrial Plot Product List..
			Result<?> industrialPlotList = homePageDao.industrialPlotList(dslContext);
			homePageResponseModel.setIndustrialPlotList(commonResGenerator.getIndustrialPlot(industrialPlotList));
			
			/**
			 * Add Main Seller Response Model to main object model.
			 */
			baseResponseModel.setHomePageMainResponse(homePageResponseModel);
			baseResponseModel = commonResGenerator.getSuccessMsgResponse(baseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			baseResponseModel = new BaseResponseModel();
			baseResponseModel = commonResGenerator.getExceptionResponse(baseResponseModel);
		}
		return baseResponseModel;
	}
	
	/**
	 * Fetch Industrial Plot
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel industrialProductService(BaseResponseModel baseResponseModel){

		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			HomePageResponseModel homePageResponseModel = new HomePageResponseModel();
			
			// Fetch Industrial Plot Product List..
			Result<?> industrialPlotList = homePageDao.industrialProdServiceList(dslContext);
			Result<?> serviceCategory = homePageDao.industrialServiceCategory(dslContext);
			
			homePageResponseModel.setIndustrialProdServiceList(commonResGenerator.getIndustrialProdService(industrialPlotList));
			homePageResponseModel.setIndustrialServicesList(commonResGenerator.getIndustrialServicename(serviceCategory));
			/**
			 * Add Main Seller Response Model to main object model.
			 */
			baseResponseModel.setHomePageMainResponse(homePageResponseModel);
			baseResponseModel = commonResGenerator.getSuccessMsgResponse(baseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			baseResponseModel = new BaseResponseModel();
			baseResponseModel = commonResGenerator.getExceptionResponse(baseResponseModel);
		}
		return baseResponseModel;
	}
	
	/**
	 * Fetch Stock Clearance List
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel stockClearanceLeads(BaseResponseModel baseResponseModel){

		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			HomePageResponseModel homePageResponseModel = new HomePageResponseModel();
			
			// Fetch Industrial Plot Product List..
			Result<?> industrialPlotList = homePageDao.stockClearanceLeadList(dslContext);
			homePageResponseModel.setStockClearanceLeadList(commonResGenerator.getStockClearanceLeadsData(industrialPlotList));
			
			/**
			 * Add Main Seller Response Model to main object model.
			 */
			baseResponseModel.setHomePageMainResponse(homePageResponseModel);
			baseResponseModel = commonResGenerator.getSuccessMsgResponse(baseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			baseResponseModel = new BaseResponseModel();
			baseResponseModel = commonResGenerator.getExceptionResponse(baseResponseModel);
		}
		return baseResponseModel;
	}

	/**
	 * @param sellerAddNewPostRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel wishList(SellerAddNewPostRequestModel sellerAddNewPostRequestModel,
			BaseResponseModel responseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			Result<ProductLikeShareRecord> record = homePageDao.checkProductInWishList(dslContext, sellerAddNewPostRequestModel);
			if(record == null || record .isEmpty()) {
				int returnIndex = homePageDao.saveWishList(dslContext, sellerAddNewPostRequestModel);
				if (returnIndex != 0) {
					responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
				} else {
					responseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
					responseModel.setMessage(ConstantKeys.NOT_SAVE);
				}
			}
			else {
				responseModel.setStatus(ConstantKeys.BAD_REQUEST_CODE);
				responseModel.setMessage(ConstantKeys.ALREADY_IN_WISH_LIST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new BaseResponseModel();
			responseModel = commonResGenerator.getExceptionResponse(responseModel);
		}
		return responseModel;
	}
	
	/**
	 * @param sellerAddNewPostRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel makeAnOffer(MakeAnOfferRquest makeAnOfferRquest,
			BaseResponseModel responseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			int returnIndex = homePageDao.saveMakeAnOffer(dslContext, makeAnOfferRquest);
			if (returnIndex != 0) {
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
			} else {
				responseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				responseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new BaseResponseModel();
			responseModel = commonResGenerator.getExceptionResponse(responseModel);
		}
		return responseModel;
	}
}
