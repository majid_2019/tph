package com.tph.services;

import java.sql.Connection;
import java.sql.SQLException;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.jooq.tph_db.tables.Users;
import com.tph.response.model.BaseResponseModel;
import com.tph.utility.DBConnectionUtility;

public class UserRecordTempService {
	public BaseResponseModel fetchUserRecord(BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			Integer total = 0;
			Long totalToday = 0l;
			
			total = dslContext.selectCount().from(Users.USERS).fetchOne(0, Integer.class);
			
			
			String currentDateQuery = "select count(*) as count from tph_db.users where   date(created_on)= current_date();";
			Result<Record> record = dslContext.fetch(currentDateQuery);
			totalToday =  (Long) record.get(0).get("count");
			
			sellerBaseResponseModel.setTempTotalUser(total);
			sellerBaseResponseModel.setTempTodayUser(totalToday);
			
			return sellerBaseResponseModel;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String[] args) {
		//new UserRecordTempService().fetchUserRecord();
	}
}
