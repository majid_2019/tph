/**
 * 
 */
package com.tph.services;

import java.sql.Connection;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.constants.ConstantKeys;
import com.tph.dao.TphFilterDao;
import com.tph.request.model.FilterDataRequest;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.HomePageResponseModel;
import com.tph.utility.DBConnectionUtility;

/**
 * @author majidkhan
 *
 */
public class TphFilterService {

	private CommonResponseGenerator commonResGenerator = new CommonResponseGenerator();
	private TphFilterDao filterDao = new TphFilterDao();

	/**
	 * Fetch Industrial Plot
	 * 
	 * @param filterData
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel filterData(BaseResponseModel baseResponseModel, FilterDataRequest filterData) {

		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			HomePageResponseModel homePageResponseModel = new HomePageResponseModel();

			if (filterData.getTypeId() != null) {
				switch (filterData.getTypeId()) {
				case 1:
					// INDUSTRIAL_PLOT
					homePageResponseModel = filterIndustrialPlotList(dslContext, homePageResponseModel, filterData);

					break;

				case 2:
					// STOCK_CLEARANCE
					homePageResponseModel = filterStockClearanceData(dslContext, homePageResponseModel, filterData);

					break;

				case 3:
					// INDUSTRIAL_SERVICES
					homePageResponseModel = filterIndustrialServicesData(dslContext, homePageResponseModel, filterData);
					
				case 4:
					// INVENTORIES
					homePageResponseModel = filterInVentoryData(dslContext, homePageResponseModel, filterData);

					break;

				default:
					break;
				}
			} else {
				baseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				baseResponseModel.setMessage(ConstantKeys.TYPE_NOT_AVAIL);
			}


			/**
			 * Add Main Seller Response Model to main object model.
			 */
			baseResponseModel.setHomePageMainResponse(homePageResponseModel);
			baseResponseModel = commonResGenerator.getSuccessMsgResponse(baseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			baseResponseModel = new BaseResponseModel();
			baseResponseModel = commonResGenerator.getExceptionResponse(baseResponseModel);
		}
		return baseResponseModel;
	}

	/**
	 * @param dslContext
	 * @param homePageResponseModel 
	 * @param filterData 
	 * @return
	 */
	private HomePageResponseModel filterIndustrialPlotList(DSLContext dslContext, HomePageResponseModel homePageResponseModel, FilterDataRequest filterData) throws Exception{
		// Fetch Industrial Plot Product List..
		Result<?> industrialPlotList = filterDao.filterIndustrialPlotList(dslContext, filterData);
		homePageResponseModel.setIndustrialPlotList(commonResGenerator.getIndustrialPlot(industrialPlotList));
		return homePageResponseModel;
	}
	
	/**
	 * @param dslContext
	 * @param homePageResponseModel 
	 * @param filterData 
	 * @return
	 */
	private HomePageResponseModel filterStockClearanceData(DSLContext dslContext, HomePageResponseModel homePageResponseModel, FilterDataRequest filterData) throws Exception{
		// Fetch Industrial Plot Product List..
		Result<?> industrialPlotList = filterDao.filterStockClearanceList(dslContext, filterData);
		homePageResponseModel.setStockClearanceLeadList(commonResGenerator.getStockClearanceLeadsData(industrialPlotList));
		
		
		return homePageResponseModel;
	}
	
	/**
	 * @param dslContext
	 * @param homePageResponseModel 
	 * @param filterData 
	 * @return
	 */
	private HomePageResponseModel filterIndustrialServicesData(DSLContext dslContext, HomePageResponseModel homePageResponseModel, FilterDataRequest filterData) throws Exception{
		// Fetch Industrial Plot Product List..
		Result<?> industrialPlotList = filterDao.filterIndustrialServiceList(dslContext, filterData);
		homePageResponseModel.setIndustrialProdServiceList(commonResGenerator.getIndustrialProdService(industrialPlotList));
		
		
		return homePageResponseModel;
	}
	
	/**
	 * @param dslContext
	 * @param homePageResponseModel 
	 * @param filterData 
	 * @return
	 */
	private HomePageResponseModel filterInVentoryData(DSLContext dslContext, HomePageResponseModel homePageResponseModel, FilterDataRequest filterData) throws Exception{
		// Fetch Industrial Plot Product List..
		Result<?> industrialPlotList = filterDao.filterInventoryDataList(dslContext, filterData);
		homePageResponseModel.setSubCatProductList(commonResGenerator.getSubCatList(industrialPlotList));
		
		return homePageResponseModel;
	}
}
