/**
 * 
 */
package com.tph.services;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.google.gson.Gson;
import com.tph.commonmethods.CommonMethods;
import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.constants.CommunicationCategory;
import com.tph.constants.ConstantKeys;
import com.tph.dao.SmsServiceDao;
import com.tph.jooq.tph_db.tables.CentralCommunicationTemplate;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.request.model.RegistraionRequestModel;
import com.tph.request.model.SmsRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.LoginResponseModel;
import com.tph.response.model.SmsResponseModel;
import com.tph.sms.externalservice.LambdaAPICaller;
import com.tph.utility.DBConnectionUtility;

/**
 * @author majidkhan
 *
 */
public class SmsService {

	private CommonResponseGenerator commonResGenerator = new CommonResponseGenerator();
	private CommonMethods comnMethod = new CommonMethods();
	private SmsServiceDao smsDao = new SmsServiceDao();
	private LambdaAPICaller lambdaAPICaller = new LambdaAPICaller();

	/**
	 * @param requestModel
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public BaseResponseModel sendSms(SmsRequestModel requestModel, BaseResponseModel response) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			/**
			 * Function for OTP
			 */
			if (requestModel.getServiceType().equalsIgnoreCase(
					CommunicationCategory.valueOf(CommunicationCategory.OTP_SMS.name()).getCommunicationCategory())) {
				Result<Record> userRecord = smsDao.checkBuyerExistOrNot(dslContext, requestModel);

				if (userRecord != null && userRecord.size() > 0) {
					sendOtp(requestModel, response, dslContext);
				} else {
					response.setStatus(ConstantKeys.BAD_REQUEST_CODE);
					response.setMessage(ConstantKeys.SELLER_NOT_EXIST);
				}
			}
			/**
			 * Function for See Details SMS
			 */
			else if (requestModel.getServiceType().equalsIgnoreCase(CommunicationCategory
					.valueOf(CommunicationCategory.CONTACT_SELLER.name()).getCommunicationCategory())) {

				if (isNumberMocked(ConstantKeys.MOCK_MOBILE_NUMBER, requestModel.getFromAddress())) {
					response.setStatus(ConstantKeys.SUCCESS_CODE);
					response.setMessage(ConstantKeys.CONTACT_SELLER_MSG_STOPPED);
					return response;
				}

				if (Integer.parseInt(ConstantKeys.IS_MOCK_SELLER_SMS) == 1)
					sendContactSellerMessage(requestModel, response, dslContext);
			}

			/**
			 * Function for See Details SMS to Make an offer
			 */
			else if (requestModel.getServiceType().equalsIgnoreCase(CommunicationCategory
					.valueOf(CommunicationCategory.MAKE_AN_OFFER.name()).getCommunicationCategory())) {

				if (isNumberMocked(ConstantKeys.MOCK_MOBILE_NUMBER, requestModel.getFromAddress())) {
					response.setStatus(ConstantKeys.SUCCESS_CODE);
					response.setMessage(ConstantKeys.CONTACT_SELLER_MSG_STOPPED);
					return response;
				}

				if (Integer.parseInt(ConstantKeys.IS_MOCK_SELLER_SMS) == 1)
					sendMakeAnOfferMessage(requestModel, response, dslContext);
			}

			else if (requestModel.getServiceType().equalsIgnoreCase(CommunicationCategory
					.valueOf(CommunicationCategory.CONTACT_SELLER_TRACKING_SMS.name()).getCommunicationCategory())) {

				sendTrackingMessage(requestModel, response, true, dslContext);
			}

			else if (requestModel.getServiceType().equalsIgnoreCase(CommunicationCategory
					.valueOf(CommunicationCategory.MAKE_AN_OFFER_TRACKING_SMS.name()).getCommunicationCategory())) {

				sendTrackingMessage(requestModel, response, false, dslContext);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new BaseResponseModel();
			response = commonResGenerator.getExceptionResponse(response);
		}
		return response;
	}

	private boolean isNumberMocked(String mockMobileNumber, String mobileNumber) {
		String[] allNumbers = mockMobileNumber.split(",");
		Boolean isNumberMatch = false;
		
		for (int i = 0; i < allNumbers.length; i++) {
			if(allNumbers[i].equals(mobileNumber.toString()))
				isNumberMatch = true;
		}
		
		return isNumberMatch;
	}

	/**
	 * @param requestModel
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public BaseResponseModel verifySms(SmsRequestModel requestModel, BaseResponseModel response) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			LoginResponseModel loginResponseModel = new LoginResponseModel();

			Record userRecord = smsDao.verifyOtp(dslContext, requestModel);

			if (userRecord != null && userRecord.size() > 0) {
				requestModel.setIsBuyer(true);
				Result<Record> record = smsDao.checkBuyerExistOrNot(dslContext, requestModel);

				response.setStatus(ConstantKeys.SUCCESS_CODE);
				response.setMessage(ConstantKeys.SUCCESS_MSG);

				loginResponseModel.setFirstName(record.get(0).getValue(Users.USERS.FIRST_NAME));
				loginResponseModel.setUserId(record.get(0).getValue(Users.USERS.ID));
				loginResponseModel.setMobileNumber(record.get(0).getValue(Users.USERS.USER_MOBILE));
				response.setLoginResponse(loginResponseModel);
			} else {
				response.setStatus(ConstantKeys.BAD_REQUEST_CODE);
				response.setMessage(ConstantKeys.IN_VALID_OTP);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new BaseResponseModel();
			response = commonResGenerator.getExceptionResponse(response);
		}
		return response;
	}

	/**
	 * @param requestModel
	 * @param response
	 * @param dslContext
	 * @return
	 * @throws Exception
	 */
	private BaseResponseModel sendOtp(SmsRequestModel requestModel, BaseResponseModel response, DSLContext dslContext)
			throws Exception {
		if (requestModel.getServiceType() != null && requestModel.getServiceType().toUpperCase().equalsIgnoreCase(
				CommunicationCategory.valueOf(CommunicationCategory.OTP_SMS.name()).getCommunicationCategory())) {

			Integer otp = comnMethod.generateOtp();
			Record record = smsDao.getOtpTemplete(dslContext);
			String smsContent = record
					.getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT);
			String finalSmsBody = comnMethod.getSMsBody(smsContent, otp);

			requestModel.setOtp(otp);
			requestModel.setSmsBody(finalSmsBody);
			smsDao.insertOtpIntoTable(dslContext, requestModel);
			SmsResponseModel otpRes = lambdaAPICaller.sendSms(requestModel, new SmsResponseModel());
			// SmsResponseModel otpRes = new
			// ExternalService().sendSms(requestModel.getSmsBody(),
			// requestModel.getMobileNumber(), new SmsResponseModel()); // Temp function for
			// SMS

			if (otpRes.getCode() == 200) {
				response.setStatus(ConstantKeys.SUCCESS_CODE);
				response.setMessage(ConstantKeys.OTP_SEND);
			} else {
				response.setStatus(otpRes.getCode());
				response.setMessage(otpRes.getMessage());
			}
			// String resBody =
			// AWSClientBuilder.invokeLambdaFunction(ConstantKeys.smsServiceLambda, new
			// Gson().toJson(requestModel));
		} else {
			response.setStatus(ConstantKeys.BAD_REQUEST_CODE);
			response.setMessage(ConstantKeys.SEND_SMS_SERVICE_TYPE);
		}
		return response;
	}

	/**
	 * @param requestModel
	 * @param response
	 * @param dslContext
	 * @return
	 * @throws Exception
	 */
	private BaseResponseModel sendContactSellerMessage(SmsRequestModel requestModel, BaseResponseModel response,
			DSLContext dslContext) throws Exception {
		if (requestModel.getServiceType() != null
				&& requestModel.getServiceType().toUpperCase().equalsIgnoreCase(CommunicationCategory
						.valueOf(CommunicationCategory.CONTACT_SELLER.name()).getCommunicationCategory())) {
			Result<Record> record = smsDao.getContactSellerTemplete(dslContext);

			String smsContent = record.get(0)
					.getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT);
			String finalSmsBody = comnMethod.getContactSellerSMSBody(smsContent, requestModel);

			System.out.println("final Contact Seller Sms Body -->> " + finalSmsBody);

			requestModel.setSmsBody(finalSmsBody);
			requestModel.setTemplateId(
					record.get(0).getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.ID).toString());

			smsDao.insertContactSellerSMSIntoTable(dslContext, requestModel);
			SmsResponseModel otpRes = lambdaAPICaller.sendSms(requestModel, new SmsResponseModel());
			// SmsResponseModel otpRes = new
			// ExternalService().sendSms(requestModel.getSmsBody(),
			// requestModel.getMobileNumber(), new SmsResponseModel()); // Temp function for
			// SMS

			if (otpRes.getCode() == 200) {
				response.setStatus(ConstantKeys.SUCCESS_CODE);
				response.setMessage(ConstantKeys.OTP_SEND);
			} else {
				response.setStatus(otpRes.getCode());
				response.setMessage(otpRes.getMessage());
			}
		} else {
			response.setStatus(ConstantKeys.BAD_REQUEST_CODE);
			response.setMessage(ConstantKeys.SEND_SMS_SERVICE_TYPE);
		}
		return response;
	}

	private BaseResponseModel sendMakeAnOfferMessage(SmsRequestModel requestModel, BaseResponseModel response,
			DSLContext dslContext) throws Exception {
		if (requestModel.getServiceType() != null && requestModel.getServiceType().toUpperCase().equalsIgnoreCase(
				CommunicationCategory.valueOf(CommunicationCategory.MAKE_AN_OFFER.name()).getCommunicationCategory())) {
			Result<Record> record = smsDao.getMknAnOfferTemplete(dslContext);

			System.out.println("record-- " + record);

			String smsContent = record.get(0)
					.getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT);

			System.out.println("final Make an offer SMS Body -->> " + smsContent);

			requestModel.setSmsBody(smsContent);
			requestModel.setTemplateId(
					record.get(0).getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.ID).toString());

			smsDao.insertMknOfferSMSIntoTable(dslContext, requestModel);
			SmsResponseModel otpRes = lambdaAPICaller.sendSms(requestModel, new SmsResponseModel());

			if (otpRes.getCode() == 200) {
				response.setStatus(ConstantKeys.SUCCESS_CODE);
				response.setMessage(ConstantKeys.OTP_SEND);
			} else {
				response.setStatus(otpRes.getCode());
				response.setMessage(otpRes.getMessage());
			}
		} else {
			response.setStatus(ConstantKeys.BAD_REQUEST_CODE);
			response.setMessage(ConstantKeys.SEND_SMS_SERVICE_TYPE);
		}
		return response;
	}

	private BaseResponseModel sendTrackingMessage(SmsRequestModel requestModel, BaseResponseModel response,
			boolean isContactSeller, DSLContext dslContext) throws Exception {
		Result<Record> record = null;

		if (isContactSeller) {
			record = smsDao.getContactSellerTrackingTemplete(dslContext);
		} else
			record = smsDao.getMknAnOfferTrackingTemplete(dslContext);

		System.out.println("record-- " + record);

		String smsContent = record.get(0)
				.getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT);

		String finalSmsBody = comnMethod.getContactSellerTrackingBody(smsContent, requestModel, isContactSeller);

		System.out.println("final Make an offer SMS Body -->> " + finalSmsBody);

		requestModel.setMobileNumber(7410062231L);
		requestModel.setIsContactSeller(isContactSeller);
		requestModel.setSmsBody(finalSmsBody);
		requestModel.setTemplateId(
				record.get(0).getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.ID).toString());

		smsDao.insertTrackingSMSIntoTable(dslContext, requestModel);
		SmsResponseModel otpRes = lambdaAPICaller.sendSms(requestModel, new SmsResponseModel());

		if (otpRes.getCode() == 200) {
			response.setStatus(ConstantKeys.SUCCESS_CODE);
			response.setMessage(ConstantKeys.OTP_SEND);
		} else {
			response.setStatus(otpRes.getCode());
			response.setMessage(otpRes.getMessage());
		}
		return response;
	}
	
	public void sendWelcomeMsg(RegistraionRequestModel loginRequestModel, Integer returnId, DSLContext dslContext) throws Exception {
		SmsRequestModel requestModel = new SmsRequestModel();
		
		Result<Record> record = smsDao.getWelcomeTemplete(dslContext);
		
		String smsContent = record.get(0)
				.getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.COMMUNICATION_BODY_CONTENT);
		
		String finalSmsBody = comnMethod.getWelcomeSMSBody(smsContent, loginRequestModel);
		
		System.out.println(finalSmsBody);

		requestModel.setServiceType(CommunicationCategory.valueOf(CommunicationCategory.WELCOME_SMS.name()).getCommunicationCategory());
		requestModel.setMobileNumber(Long.parseLong(loginRequestModel.getMobileNumber()));
		requestModel.setSmsBody(finalSmsBody);
		requestModel.setTemplateId(
				record.get(0).getValue(CentralCommunicationTemplate.CENTRAL_COMMUNICATION_TEMPLATE.ID).toString());
		
		SmsResponseModel otpRes = lambdaAPICaller.sendSms(requestModel, new SmsResponseModel());
		
		smsDao.insertWelcomeSMSIntoTable(dslContext,returnId, requestModel);
		System.out.println("Welcome SMS Res-- "+ new Gson().toJson(otpRes));
	}
	
	public static void main(String[] args) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			RegistraionRequestModel loginRequestModel = new RegistraionRequestModel();
			
			loginRequestModel.setMobileNumber("8109536446");
			
			new SmsService().sendWelcomeMsg(loginRequestModel, 1, dslContext);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
