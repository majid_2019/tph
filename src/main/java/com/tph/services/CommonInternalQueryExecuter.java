package com.tph.services;

import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.Result;
import org.jooq.impl.DSL;

import com.tph.jooq.tph_db.tables.MakeAnOffer;
import com.tph.jooq.tph_db.tables.PaymentTransactionLog;
import com.tph.jooq.tph_db.tables.ProductImageVideo;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.UserActivityLogRecord;
import com.tph.request.model.CommonRequestModel;
import com.tph.request.model.PaymentDetailRequestModel;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.request.model.SmsRequestModel;
import com.tph.response.model.HomePageResponseModel;
import com.tph.response.model.ProductDetailResModel;
import com.tph.response.model.SellerDetails;

public class CommonInternalQueryExecuter {

	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public Integer fetchIndustrialProductList(DSLContext dslContext, Integer userId) throws Exception {

		return dslContext.selectFrom(MakeAnOffer.MAKE_AN_OFFER)
				.where(MakeAnOffer.MAKE_AN_OFFER.SELLER_USER_ID.eq(userId)).fetchCount();
	}

	/**
	 * @param dslContext
	 * @return
	 */
	public Result<Record> fetchAllIndustrialData(DSLContext dslContext) {
		String query = "select b.id as ind_id,b.name industrial_name, c.id as cat_master_id,c.cat_name industrial_cat_name , d.id as sub_cat_id,d.sub_category_name from tph_db.`industrial_cat_sub_cat_mapping` a \n"
				+ "join tph_db.industrial_type b on (a.industrial_type_id=b.id)\n"
				+ "join tph_db.categories_master c on (a.cat_id=c.id) join tph_db.sub_Categories d on (a.sub_cat_id=d.id)";

		Result<Record> record = dslContext.fetch(query);
		return record;

	}

	/**
	 * @param dslContext
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public Result<Record2<Integer, String>> fetchActiveProdImage(DSLContext dslContext, SellerBaseRequestModel request) throws Exception{
		return dslContext.select(ProductImageVideo.PRODUCT_IMAGE_VIDEO.PRODUCT_ID, ProductImageVideo.PRODUCT_IMAGE_VIDEO.IMAGE_VEDIO_PATH)
				.from(ProductImageVideo.PRODUCT_IMAGE_VIDEO)
		.where(ProductImageVideo.PRODUCT_IMAGE_VIDEO.PRODUCT_USER_ID.eq(request.getUserId()))
		.and(ProductImageVideo.PRODUCT_IMAGE_VIDEO.UPLOAD_ENUM.eq("IMAGE"))
		.and(ProductImageVideo.PRODUCT_IMAGE_VIDEO.IMAGE_VEDIO_STATUS.eq("Y"))
		.fetch();
	}
	
	/**
	 * @param dslContext
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public Result<Record2<Integer, String>> fetchHomePageProdImage(DSLContext dslContext, List<Integer> IdList) throws Exception{
		return dslContext.select(ProductImageVideo.PRODUCT_IMAGE_VIDEO.PRODUCT_ID, ProductImageVideo.PRODUCT_IMAGE_VIDEO.IMAGE_VEDIO_PATH).from(ProductImageVideo.PRODUCT_IMAGE_VIDEO)
		.where(ProductImageVideo.PRODUCT_IMAGE_VIDEO.PRODUCT_ID.in(IdList))
		.and(ProductImageVideo.PRODUCT_IMAGE_VIDEO.UPLOAD_ENUM.eq("IMAGE"))
		.and(ProductImageVideo.PRODUCT_IMAGE_VIDEO.IMAGE_VEDIO_STATUS.eq("Y"))
		.fetch();
	}
	
	public Result<Record2<String, String>> fetchSingleProdctImageVideo(DSLContext dslContext, Integer prodId) throws Exception{
		return dslContext.select(ProductImageVideo.PRODUCT_IMAGE_VIDEO.IMAGE_VEDIO_PATH, ProductImageVideo.PRODUCT_IMAGE_VIDEO.UPLOAD_ENUM).from(ProductImageVideo.PRODUCT_IMAGE_VIDEO)
		.where(ProductImageVideo.PRODUCT_IMAGE_VIDEO.PRODUCT_ID.eq(prodId))
		.and(ProductImageVideo.PRODUCT_IMAGE_VIDEO.IMAGE_VEDIO_STATUS.eq("Y"))
		.orderBy(ProductImageVideo.PRODUCT_IMAGE_VIDEO.CREATED_DATE.desc()).limit(3)
		.fetch();
	}
	
	public Result<Record> checkSellerExistOrNot(DSLContext dslContext, SmsRequestModel smsRequestModel) throws Exception{
		String isSeller = "N";
		if (smsRequestModel.getIsSeller()) {
			isSeller = "Y";
		}
		return dslContext.select().from(Users.USERS).where(Users.USERS.USER_MOBILE.eq(String.valueOf(smsRequestModel.getMobileNumber())))
		.and(Users.USERS.IS_SELLER.eq(isSeller))
		.and(Users.USERS.ACTIVE.eq("Y"))
		.fetch();
	}

	public void insertInActivityLog(SellerDetails sellerDetails, String productCode, DSLContext dslContext) {
		
		UserActivityLogRecord activityLogRecord = new UserActivityLogRecord();
		
		activityLogRecord.setUserId(sellerDetails.getId());
		activityLogRecord.setEmailId(sellerDetails.getSellerEmail());
		activityLogRecord.setMobileNumber(Long.parseLong(sellerDetails.getSellerMobileNumber()));
		activityLogRecord.setProductDetId(productCode);
		
		Integer inserted = dslContext.executeInsert(activityLogRecord);
		System.out.println("Inserted in User activity log -- "+inserted);
	}
	
	public Record fetchUserUsingId(PaymentDetailRequestModel request, DSLContext dslContext) {
		return dslContext.selectFrom(Users.USERS).where(Users.USERS.ID.eq(request.getSellerID())).fetchOne();
	}
}
