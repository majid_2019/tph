package com.tph.services;

import java.sql.Connection;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.constants.ConstantKeys;
import com.tph.constants.PostAction;
import com.tph.dao.EditPostDao;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.utility.DBConnectionUtility;

public class EditPostService {
	private CommonResponseGenerator commonResGenerator = new CommonResponseGenerator();
	private EditPostDao editPostDao = new EditPostDao();

	public BaseResponseModel postAction(SellerAddNewPostRequestModel request) {
		BaseResponseModel responseModel = new BaseResponseModel();
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			if (request.getActionType().toUpperCase().equals(PostAction.SOLD_POST.toString())) {
				responseModel = postSoldMark(request, responseModel, dslContext);
			} else if (request.getActionType().toUpperCase().equals(PostAction.FETCH_POST.toString())) {
				responseModel = postFetch(request, responseModel, dslContext);
			} else if (request.getActionType().toUpperCase().equals(PostAction.UPDATE_POST.toString())) {
				responseModel = uppdatePost(request, responseModel, dslContext);
			}

		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new BaseResponseModel();
			responseModel = commonResGenerator.getExceptionResponse(responseModel);
		}
		return responseModel;
	}

	private BaseResponseModel postSoldMark(SellerAddNewPostRequestModel request, BaseResponseModel responseModel,
			DSLContext dslContext) throws Exception {
		if (request.getServiceType().equalsIgnoreCase(ConstantKeys.INVENTORY) 
				|| request.getServiceType().equalsIgnoreCase(ConstantKeys.STOCK_CLEARANCE)) {

			Integer rowUpdated = editPostDao.markSoldInvetoryPost(request.getId(), dslContext);
			if (rowUpdated != 0) {
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
			} else {
				responseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				responseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		}

		if (request.getServiceType().equalsIgnoreCase(ConstantKeys.INDUSTRIAL_PLOT)) {

			Integer rowUpdated = editPostDao.markSoldPlotPost(request.getId(), dslContext);
			if (rowUpdated != 0) {
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
			} else {
				responseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				responseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		}

		return responseModel;
	}

	private BaseResponseModel postFetch(SellerAddNewPostRequestModel request, BaseResponseModel responseModel,
			DSLContext dslContext) throws Exception {
		if (request.getServiceType().equalsIgnoreCase(ConstantKeys.INVENTORY)
				||request.getServiceType().equalsIgnoreCase(ConstantKeys.STOCK_CLEARANCE)) {
			Result<Record> record = editPostDao.fetchnvetoryPost(request.getId(), dslContext);
			if (record != null) {
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
				responseModel.setEditPost(commonResGenerator.inventoryEditPost(record));
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
			} else {
				responseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				responseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		}

		if (request.getServiceType().equalsIgnoreCase(ConstantKeys.INDUSTRIAL_PLOT)) {

			System.out.println("Id--- "+request.getId());
			Result<Record> record = editPostDao.fetchPlotPost(request.getId(), dslContext);
			if (record != null) {
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);

				responseModel.setEditPost(commonResGenerator.plotEditPost(record));
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
			} else {
				responseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				responseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		}

		if (request.getServiceType().equalsIgnoreCase(ConstantKeys.INDUSTRIAL_SERVICES)) {

			Result<Record> record = editPostDao.fetchIndusServicesPost(request.getId(), dslContext);
			if (record != null) {
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);

				responseModel.setEditPost(commonResGenerator.industServiceEditPost(record));
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
			} else {
				responseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				responseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		}
		return responseModel;
	}

	private BaseResponseModel uppdatePost(SellerAddNewPostRequestModel request, BaseResponseModel responseModel,
			DSLContext dslContext) throws Exception{
		if (request.getServiceType().equalsIgnoreCase(ConstantKeys.INVENTORY)
				||request.getServiceType().equalsIgnoreCase(ConstantKeys.STOCK_CLEARANCE)) {
			Integer rowUpdated = editPostDao.updateInventoryPost(request, dslContext);
			if (rowUpdated != 0) {
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
			} else {
				responseModel.setStatus(ConstantKeys.RECORD_NOT_AVAIL);
				responseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		}
		
		if (request.getServiceType().equalsIgnoreCase(ConstantKeys.INDUSTRIAL_PLOT)) {
			Integer rowUpdated = editPostDao.updatePlotPost(request, dslContext);
			if (rowUpdated != 0) {
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
			} else {
				responseModel.setStatus(ConstantKeys.RECORD_NOT_AVAIL);
				responseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		}
		
		if (request.getServiceType().equalsIgnoreCase(ConstantKeys.INDUSTRIAL_SERVICES)) {
			Integer rowUpdated = editPostDao.updateIndusServicePost(request, dslContext);
			if (rowUpdated != 0) {
				responseModel = commonResGenerator.getSuccessMsgResponse(responseModel);
			} else {
				responseModel.setStatus(ConstantKeys.RECORD_NOT_AVAIL);
				responseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		}

		return responseModel;
	}
}
