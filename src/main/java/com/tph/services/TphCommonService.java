/**
 * 
 */
package com.tph.services;

import java.sql.Connection;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.constants.ConstantKeys;
import com.tph.dao.TphCommonDao;
import com.tph.jooq.tph_db.tables.records.ProductBrandMasterRecord;
import com.tph.request.model.ProdViewCountRequest;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.SellerDashboardResponseModel;
import com.tph.utility.DBConnectionUtility;

/**
 * @author majidkhan
 *
 */
public class TphCommonService {
	private CommonResponseGenerator commonResGenerator = new CommonResponseGenerator();
	private TphCommonDao commonDao = new TphCommonDao();
	
	/**
	 * @param dashboardRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel fetchUom(BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			SellerDashboardResponseModel sellerDashboardResponseModel = new SellerDashboardResponseModel();

			sellerDashboardResponseModel.setUomList(commonResGenerator.getUonList(commonDao.uomList(dslContext)));
			/**
			 * Add Main Seller Response Model to main object model.
			 */
			sellerBaseResponseModel.setMainResponse(sellerDashboardResponseModel);
			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param dashboardRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel fetchCountry(BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			SellerDashboardResponseModel sellerDashboardResponseModel = new SellerDashboardResponseModel();

			sellerDashboardResponseModel.setCountryList(commonResGenerator.getCountryList(commonDao.countryList(dslContext)));

			/**
			 * Add Main Seller Response Model to main object model.
			 */
			sellerBaseResponseModel.setMainResponse(sellerDashboardResponseModel);
			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param dashboardRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel fetchCity(SellerBaseRequestModel dashboardRequestModel,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			SellerDashboardResponseModel sellerDashboardResponseModel = new SellerDashboardResponseModel();

			sellerDashboardResponseModel.setCityList(commonResGenerator.getCityList(commonDao.cityList(dslContext, dashboardRequestModel.getStateId())));

			/**
			 * Add Main Response Model to main object model.
			 */

			sellerBaseResponseModel.setMainResponse(sellerDashboardResponseModel);
			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}
	
	/**
	 * @param sellerBaseRequestModel 
	 * @param dashboardRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel fetchState(SellerBaseRequestModel sellerBaseRequestModel, BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			SellerDashboardResponseModel sellerDashboardResponseModel = new SellerDashboardResponseModel();

			sellerDashboardResponseModel.setStateList(commonResGenerator.getStateList(commonDao.stateList(dslContext, sellerBaseRequestModel.getCountryId())));
			/**
			 * Add Main Seller Response Model to main object model.
			 */
			sellerBaseResponseModel.setMainResponse(sellerDashboardResponseModel);
			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param dashboardRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel fetchIndustriesMaster(SellerBaseRequestModel dashboardRequestModel,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			
			SellerDashboardResponseModel dashboardResModel = new SellerDashboardResponseModel();
			
			dashboardResModel.setIndustries(
					commonResGenerator.IndustrialTypeRecord(commonDao.fetchIdustryList(dslContext)));
			
			/**
			 * Add Main Response Model to main object model.
			 */
			sellerBaseResponseModel.setMainResponse(dashboardResModel);
			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);

		}
		catch (Exception e) {
			e.printStackTrace();
			/**
			 * Add Main Response Model to main object model.
			 */
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		
		return sellerBaseResponseModel;
	}
	
	/**
	 * 
	 * @param dashboardRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public BaseResponseModel fetchCategoryMaster(SellerBaseRequestModel sellerBaseRequestModel,
			BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			sellerBaseResponseModel.setIndustriesCatData(commonResGenerator
					.fetchIndCategoryList(sellerBaseResponseModel,commonDao.fetchCategoryMaster(dslContext, sellerBaseRequestModel)));
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param dashboardRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public BaseResponseModel fetchBrand(BaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			SellerDashboardResponseModel sellerDashboardResponseModel = new SellerDashboardResponseModel();

			Result<ProductBrandMasterRecord> brandList = commonDao.brandList(dslContext);
			sellerDashboardResponseModel.setBrandList(commonResGenerator.getBrandList(brandList));

			/**
			 * Add Main Seller Response Model to main object model.
			 */
			sellerBaseResponseModel.setMainResponse(sellerDashboardResponseModel);
			sellerBaseResponseModel = commonResGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = commonResGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}
	
	// Product View Counter..
	public BaseResponseModel productViewcounter(ProdViewCountRequest request, BaseResponseModel responseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			Integer counter = 0;
			if(request.getProductCode().contains("TPHIR") || request.getProductCode().contains("TPHSC")) {
				counter = commonDao.updateInventoryCounter(request, dslContext);
			}
			else if(request.getProductCode().contains("TPHIP")) {
				counter = commonDao.updateIpProdListCounter(request,dslContext);
			}
			else if(request.getProductCode().contains("TPHSP")) {
				counter = commonDao.updateIndustrialProdServiceCounter(request, dslContext);				
			}
			
			if(counter == 1) {
				responseModel.setStatus(ConstantKeys.SUCCESS_CODE);
				responseModel.setMessage(ConstantKeys.SUCCESS_MSG);
			}
			else {
				responseModel.setStatus(ConstantKeys.RECORD_NOT_AVAIL);
				responseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}
}
