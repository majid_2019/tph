/**
 * 
 */
package com.tph.handler;

import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.request.model.AddBuyerRequirmentReq;
import com.tph.request.model.CommonRequestModel;
import com.tph.request.model.EditProfilleRequest;
import com.tph.request.model.FilterDataRequest;
import com.tph.request.model.InventoryRequest;
import com.tph.request.model.LoginRequestModel;
import com.tph.request.model.MakeAnOfferRquest;
import com.tph.request.model.NotificationRequest;
import com.tph.request.model.PasswordRequest;
import com.tph.request.model.PaymentDetail;
import com.tph.request.model.PaymentDetailRequestModel;
import com.tph.request.model.ProdViewCountRequest;
import com.tph.request.model.RegistraionRequestModel;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.request.model.SmsRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.EditPostService;
import com.tph.services.NotificationService;
import com.tph.services.PasswordService;
import com.tph.services.PaymentService;
import com.tph.services.SellerDashboardService;
import com.tph.services.SmsService;
import com.tph.services.TphCommonService;
import com.tph.services.TphFilterService;
import com.tph.services.TphHomePageService;
import com.tph.services.TphLoginRegistraionService;
import com.tph.services.UserRecordTempService;

/**
 * @author majidkhan
 *
 */
public class TphHandler implements RequestHandler<Map<String, Object>, BaseResponseModel> {

	private final static Gson gson = new Gson();
	private BaseResponseModel sellerBaseResponseModel;
	private static SellerBaseRequestModel sellerBaseRequestModel = new SellerBaseRequestModel();
	private static TphCommonService tphCommonService = new TphCommonService();
	private CommonRequestModel commonRequestModel;

	enum RequestType {
		sellerDashboard, sellerActivePost, cityList, countryList, stateList, uomList, saveSellerMyPost, industriesList,
		subCategoryList, updateSellerProfile, viewSellerProfile, homePage, dashProductDetail, addBuyersRequirements,
		fetchBuyersAllRequirements, subCatProductList, sellerInventoryList, sellerFormAttribute,saveImageVideo,brandList, sendSms, 
		industrialPlotList, industrialProdService, stockClearanceLeadList, saveIndustrialPlotPost, saveIpProductImageVideo,
		saveIndustrialServicePost, saveIndustrialServiceImageVideo, wishList, makeAnOffer, login, sellerRegistration, verifyOtp,
		savePayment, fetchNotification, payuHash, calculateAmount, sendNotification, editPost, sellerPassword, filterData, productCounter, test
	}

	@Override
	public BaseResponseModel handleRequest(Map<String, Object> input, Context context) {
		try {
			TphHomePageService tphHomePageDao =  new TphHomePageService();
			SellerDashboardService sellerDashboardDataDao = new SellerDashboardService();
			SellerAddNewPostRequestModel sellerAddNewPostRequestModel = null;
			
			String requestId = String.valueOf(input.get("requestId"));

			RequestType requestType = RequestType.valueOf(requestId);
			String payLoad = (String) input.get("payLoad");

			System.out.println("Request Type -- " + requestType);
			System.out.println("payLoad -- " + payLoad);

			switch (requestType) {
			case sellerDashboard:

				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.fetchDashboardData(sellerBaseRequestModel,
						sellerBaseResponseModel);

				break;

			case sellerActivePost:

				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.fetchActivePost(sellerBaseRequestModel,
						sellerBaseResponseModel);

				break;

			case cityList:

				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = tphCommonService.fetchCity(sellerBaseRequestModel, sellerBaseResponseModel);

				break;
				
			case countryList:

				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseResponseModel = tphCommonService.fetchCountry(sellerBaseResponseModel);

				break;
				
			case brandList:

				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseResponseModel = tphCommonService.fetchBrand(sellerBaseResponseModel);

				break;
				
			case stateList:

				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = tphCommonService.fetchState(sellerBaseRequestModel, sellerBaseResponseModel);

				break;
				
			case uomList:

				sellerBaseResponseModel = new BaseResponseModel();
				sellerBaseResponseModel = tphCommonService.fetchUom(sellerBaseResponseModel);

				break;
				
			case saveSellerMyPost:

				sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				sellerAddNewPostRequestModel = gson.fromJson(payLoad, SellerAddNewPostRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.saveNewPost(sellerAddNewPostRequestModel,
						sellerBaseResponseModel);

				break;
				
			case saveImageVideo:

				sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				sellerAddNewPostRequestModel = gson.fromJson(payLoad, SellerAddNewPostRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.saveNewPostImage(sellerAddNewPostRequestModel, sellerBaseResponseModel);

				break;
				
			case industriesList:

				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = tphCommonService.fetchIndustriesMaster(sellerBaseRequestModel, sellerBaseResponseModel);

				break;
				
			case subCategoryList:

				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = tphCommonService.fetchCategoryMaster(sellerBaseRequestModel, sellerBaseResponseModel);

				break;

			case updateSellerProfile:

				EditProfilleRequest editProfilleRequest = new EditProfilleRequest();
				sellerBaseResponseModel = new BaseResponseModel();

				editProfilleRequest = gson.fromJson(payLoad, EditProfilleRequest.class);
				sellerBaseResponseModel = sellerDashboardDataDao.updateProfileData(editProfilleRequest, sellerBaseResponseModel);

				break;
				
			case viewSellerProfile:

				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.fetchProfileData(sellerBaseRequestModel, sellerBaseResponseModel);

				break;
				
			case homePage:

				sellerBaseResponseModel = new BaseResponseModel();
				sellerBaseResponseModel = tphHomePageDao.fetchHomePageData(sellerBaseResponseModel);

				break;
				
			case dashProductDetail:

				commonRequestModel = new CommonRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				commonRequestModel = gson.fromJson(payLoad, CommonRequestModel.class);
				sellerBaseResponseModel = tphHomePageDao.dashProductDetails(commonRequestModel, sellerBaseResponseModel);

				break;
				
			case addBuyersRequirements:
				AddBuyerRequirmentReq addBuyerRequirmentReq = new AddBuyerRequirmentReq();
				sellerBaseResponseModel = new BaseResponseModel();
				
				addBuyerRequirmentReq = gson.fromJson(payLoad, AddBuyerRequirmentReq.class);
				sellerBaseResponseModel = tphHomePageDao.addBuyersRequirmnts(addBuyerRequirmentReq, sellerBaseResponseModel);
				
				break;
				
			case fetchBuyersAllRequirements:
				
				sellerBaseResponseModel = new BaseResponseModel();
				sellerBaseResponseModel = tphHomePageDao.fetchAllBuyersRequirements(sellerBaseResponseModel);
				
				break;
				
			case subCatProductList:
				
				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = tphHomePageDao.fetchSubCatProdList(sellerBaseRequestModel, sellerBaseResponseModel);
				
				break;
				
			case sellerInventoryList:
				
				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.fetchSellerAllInventory(sellerBaseRequestModel, sellerBaseResponseModel);
				
				break;
				
			case sellerFormAttribute:
				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.fetchSellerFormAttributes(sellerBaseRequestModel, sellerBaseResponseModel);
				
				break;
				
			case industrialPlotList:

				sellerBaseResponseModel = new BaseResponseModel();
				sellerBaseResponseModel = tphHomePageDao.industrialPlot(sellerBaseResponseModel);

				break;
				
			case industrialProdService:

				sellerBaseResponseModel = new BaseResponseModel();
				sellerBaseResponseModel = tphHomePageDao.industrialProductService(sellerBaseResponseModel);

				break;
				
			case stockClearanceLeadList:

				sellerBaseResponseModel = new BaseResponseModel();
				sellerBaseResponseModel = tphHomePageDao.stockClearanceLeads(sellerBaseResponseModel);

				break;
				
			case saveIndustrialPlotPost:

				sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				sellerAddNewPostRequestModel = gson.fromJson(payLoad, SellerAddNewPostRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.saveNewindustrialProductList(sellerAddNewPostRequestModel, sellerBaseResponseModel);

				break;
				
			case saveIpProductImageVideo:

				sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				sellerAddNewPostRequestModel = gson.fromJson(payLoad, SellerAddNewPostRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.saveIndustrialProductListImage(sellerAddNewPostRequestModel, sellerBaseResponseModel);

				break;
				
			case saveIndustrialServicePost:

				sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				sellerAddNewPostRequestModel = gson.fromJson(payLoad, SellerAddNewPostRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.saveIndustrialProdService(sellerAddNewPostRequestModel, sellerBaseResponseModel);

				break;
				
			case saveIndustrialServiceImageVideo:

				sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				sellerAddNewPostRequestModel = gson.fromJson(payLoad, SellerAddNewPostRequestModel.class);
				sellerBaseResponseModel = sellerDashboardDataDao.saveIndustrialProductServiceImage(sellerAddNewPostRequestModel, sellerBaseResponseModel);

				break;
				
			case wishList:

				sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				sellerAddNewPostRequestModel = gson.fromJson(payLoad, SellerAddNewPostRequestModel.class);
				sellerBaseResponseModel = tphHomePageDao.wishList(sellerAddNewPostRequestModel, sellerBaseResponseModel);

				break;
				
			case makeAnOffer:

				MakeAnOfferRquest makeAnOfferRquest = new MakeAnOfferRquest();
				sellerBaseResponseModel = new BaseResponseModel();

				makeAnOfferRquest = gson.fromJson(payLoad, MakeAnOfferRquest.class);
				sellerBaseResponseModel = tphHomePageDao.makeAnOffer(makeAnOfferRquest, sellerBaseResponseModel);

				break;
				
			case login:

				LoginRequestModel loginRequestModel = new LoginRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				loginRequestModel = gson.fromJson(payLoad, LoginRequestModel.class);
				sellerBaseResponseModel = new TphLoginRegistraionService().login(loginRequestModel, sellerBaseResponseModel);

				break;
				
			case sellerRegistration:

				RegistraionRequestModel registraionRequestModel = new RegistraionRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				registraionRequestModel = gson.fromJson(payLoad, RegistraionRequestModel.class);
				sellerBaseResponseModel = new TphLoginRegistraionService().registraion(registraionRequestModel, sellerBaseResponseModel);

				break;
				
			case savePayment:
				
				PaymentDetailRequestModel paymentRequest = new PaymentDetailRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				paymentRequest = gson.fromJson(payLoad, PaymentDetailRequestModel.class);
				sellerBaseResponseModel = new PaymentService().savePaymentDetails(paymentRequest, sellerBaseResponseModel);
				
				break;
				
			case sendSms:
				
				SmsRequestModel smsRequestModel = new SmsRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				smsRequestModel = gson.fromJson(payLoad, SmsRequestModel.class);
				sellerBaseResponseModel = new SmsService().sendSms(smsRequestModel, sellerBaseResponseModel);
				
				break;
				
			case verifyOtp:
				
				SmsRequestModel verifyModel = new SmsRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();

				verifyModel = gson.fromJson(payLoad, SmsRequestModel.class);
				sellerBaseResponseModel = new SmsService().verifySms(verifyModel, sellerBaseResponseModel);
				
				break;
				
			case sendNotification:
				
				NotificationRequest request = new NotificationRequest();
				sellerBaseResponseModel = new BaseResponseModel();
				
				request = gson.fromJson(payLoad, NotificationRequest.class);
				sellerBaseResponseModel = new NotificationService().sendNotification(request);
				
				break;
				
			case fetchNotification:
				
				sellerBaseResponseModel = new BaseResponseModel();

				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				sellerBaseResponseModel = new NotificationService().fetchNotification(sellerBaseRequestModel, sellerBaseResponseModel);
				
				break;
				
			case sellerPassword:
				
				PasswordRequest passwordReq = new PasswordRequest();
				sellerBaseResponseModel = new BaseResponseModel();
				
				passwordReq = gson.fromJson(payLoad, PasswordRequest.class);
				sellerBaseResponseModel = new PasswordService().changePassword(passwordReq);
				
				break;
				
			case editPost:
				
				sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
				sellerBaseResponseModel = new BaseResponseModel();
				
				sellerAddNewPostRequestModel = gson.fromJson(payLoad, SellerAddNewPostRequestModel.class);
				sellerBaseResponseModel = new EditPostService().postAction(sellerAddNewPostRequestModel);
				
				break;
				
			case calculateAmount:
				
				InventoryRequest inventoryRequest = new InventoryRequest();
				sellerBaseResponseModel = new BaseResponseModel();

				inventoryRequest = gson.fromJson(payLoad, InventoryRequest.class);
				sellerBaseResponseModel = new PaymentService().calculateAmount(inventoryRequest, sellerBaseResponseModel);
				
				break;
				
			case filterData:
				
				FilterDataRequest filterData = new FilterDataRequest();
				
				String cityId, brandId, stateId, typeId, countryId, industiesType, rentSellId, indServiceCategory, 
						industrialTypeId, subCatTypeId, catTypeId;
				
				cityId = String.valueOf(input.get("cityId"));
				brandId = String.valueOf(input.get("brand"));
				stateId = String.valueOf(input.get("stateId"));
				typeId = String.valueOf(input.get("typeId"));
				countryId = String.valueOf(input.get("countryId"));
				industiesType = String.valueOf(input.get("industryType"));
				rentSellId = String.valueOf(input.get("rentSellId"));
				indServiceCategory = String.valueOf(input.get("indServiceCategory"));
				
				industrialTypeId = String.valueOf(input.get("industrialTypeId"));
				subCatTypeId = String.valueOf(input.get("subCatTypeId"));
				catTypeId = String.valueOf(input.get("catTypeId"));
				
				
				if(cityId != null && !cityId.isEmpty() )
				filterData.setCityId(Integer.parseInt(cityId));
				
				if(brandId != null && !brandId.isEmpty())
				filterData.setBrand(brandId);
				
				if(stateId != null && !stateId.isEmpty())
				filterData.setStateId(Integer.parseInt(stateId));
				
				if(typeId != null && !typeId.isEmpty())
				filterData.setTypeId(Integer.parseInt(typeId));
				
				if(countryId != null && !countryId.isEmpty())
				filterData.setCountryId(Integer.parseInt(countryId));
				
				if(industiesType != null && !industiesType.isEmpty())
				filterData.setIndustiesType(industiesType);
				
				if(rentSellId != null && !rentSellId.isEmpty())
				filterData.setRentSellId(Integer.parseInt(rentSellId));
				
				if(indServiceCategory != null && !indServiceCategory.isEmpty())
				filterData.setIndServiceCategory(indServiceCategory);
				
				if(industrialTypeId != null && !industrialTypeId.isEmpty() )
					filterData.setIndustrialTypeId(Integer.parseInt(industrialTypeId));
				
				if(subCatTypeId != null && !subCatTypeId.isEmpty() )
					filterData.setSubCatTypeId(Integer.parseInt(subCatTypeId));
				
				if(catTypeId != null && !catTypeId.isEmpty() )
					filterData.setCatTypeId(Integer.parseInt(catTypeId));
				
				System.out.println("FiterData -- "+new Gson().toJson(filterData));
				
				sellerBaseResponseModel = new BaseResponseModel();
				sellerBaseResponseModel = new TphFilterService().filterData(sellerBaseResponseModel, filterData);
				
				break;
				
			case payuHash:
				
				PaymentDetail paymentDetail = new PaymentDetail();
				sellerBaseResponseModel = new BaseResponseModel();

				paymentDetail = gson.fromJson(payLoad, PaymentDetail.class);
				sellerBaseResponseModel = new PaymentService().generateHash(paymentDetail, sellerBaseResponseModel);
				
				break;
				
			case productCounter:
				
				ProdViewCountRequest productCounter = new ProdViewCountRequest();

				productCounter = gson.fromJson(payLoad, ProdViewCountRequest.class);
				
				sellerBaseResponseModel = new BaseResponseModel();
				sellerBaseResponseModel = new TphCommonService().productViewcounter(productCounter, sellerBaseResponseModel);
				
				break;
				
			case test:
				
				sellerBaseResponseModel = new BaseResponseModel();
				sellerBaseResponseModel = new UserRecordTempService().fetchUserRecord(sellerBaseResponseModel);
				
				break;
				
			default:
				System.out.println(">>>>----- No Payload match -----<<<< ");
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new BaseResponseModel();
			sellerBaseResponseModel = new CommonResponseGenerator().getLambdaFailResponse(sellerBaseResponseModel);
		}
		System.out.println(">>>>----- Response -----<<<< " + new Gson().toJson(sellerBaseResponseModel));
		return sellerBaseResponseModel;
	}

}
