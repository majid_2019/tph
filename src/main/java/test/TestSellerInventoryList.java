/**
 * 
 */
package test;

import com.google.gson.Gson;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestSellerInventoryList {
public static void main(String[] args) {
		
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();

		SellerBaseRequestModel dashboardRequestModel = new SellerBaseRequestModel();
		dashboardRequestModel.setUserId(467);

		sellerBaseResponseModel = new SellerDashboardService().fetchSellerAllInventory(dashboardRequestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}
