package test;

import com.google.gson.Gson;
import com.tph.request.model.SmsRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SmsService;

public class TestVerifySms {

	public static void main(String[] args) {
		SmsRequestModel requestModel = new SmsRequestModel();
		
		requestModel.setMobileNumber(9960559090l);
		requestModel.setServiceType("OTP SMS");
		requestModel.setOtp(3081);
		requestModel.setIsSeller(false);
		requestModel.setIsBuyer(true);
		
		
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		sellerBaseResponseModel = new SmsService().verifySms(requestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}

