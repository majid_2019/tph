/**
 * 
 */
package test;

import com.google.gson.Gson;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestSellerFormAttributes {
public static void main(String[] args) {
		
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();

		SellerBaseRequestModel dashboardRequestModel = new SellerBaseRequestModel();
		dashboardRequestModel.setUserId(1);
		dashboardRequestModel.setIndustrialTypeId(2);
		dashboardRequestModel.setCatTypeId(3);

		sellerBaseResponseModel = new SellerDashboardService().fetchSellerFormAttributes(dashboardRequestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}
