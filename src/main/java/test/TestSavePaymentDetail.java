package test;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.google.gson.Gson;
import com.tph.pdfutility.CreatePDF;
import com.tph.request.model.PaymentDetailRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.PaymentService;

public class TestSavePaymentDetail {
	public static void main(String[] args) throws SQLException {
		
		BaseResponseModel baseResponse = new BaseResponseModel();
		PaymentDetailRequestModel request = new PaymentDetailRequestModel();
		
		request.setIsPaymentSuccess(true);
		request.setProductId(11);
		request.setPayType("PAYU");
		request.setAccountNumber("123456788");
		request.setReceiptNumber("43434");
		request.setTransactionId("abcd1234");
		request.setIfsc("SBIN0000444");
		request.setSellerID(1);
		request.setDashboardAmount(BigDecimal.valueOf(444444.09));
		request.setTphBaseAmount(BigDecimal.valueOf(456.4));
		request.setTphTotalAmount(BigDecimal.valueOf(444444.09));
		request.setPaymentStatus("Success");
		request.setCgst(BigDecimal.valueOf(180));
		request.setSgst(BigDecimal.valueOf(400));
		//request.setIgst(BigDecimal.valueOf(18));
		
		baseResponse = new PaymentService().savePaymentDetails(request, baseResponse);
		
		System.out.println("REsponse -- "+new Gson().toJson(baseResponse));
		//new CreatePDF().createPdf(request);
		
	}
}	
