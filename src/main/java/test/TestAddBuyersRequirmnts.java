package test;

import com.google.gson.Gson;
import com.tph.request.model.AddBuyerRequirmentReq;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphHomePageService;

public class TestAddBuyersRequirmnts {
	public static void main(String[] args) {
		
		AddBuyerRequirmentReq addBuyerRequirmentReq = new AddBuyerRequirmentReq();
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		addBuyerRequirmentReq.setProductDescription("Slotting Machine 20 inch and 14 inch");
		addBuyerRequirmentReq.setRequiredQuantity(5);
		addBuyerRequirmentReq.setBrandName("Apple");
		addBuyerRequirmentReq.setProductTitle("Electronic");
		addBuyerRequirmentReq.setStateId(3);
		addBuyerRequirmentReq.setCityId(3);
		addBuyerRequirmentReq.setUomId(1);
		addBuyerRequirmentReq.setCountryId(101);
		addBuyerRequirmentReq.setUserId(1);
		
		try {
			sellerBaseResponseModel = new TphHomePageService().addBuyersRequirmnts(addBuyerRequirmentReq, sellerBaseResponseModel);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}
