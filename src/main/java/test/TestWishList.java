/**
 * 
 */
package test;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.PhotoVideoPath;
import com.tph.services.SellerDashboardService;
import com.tph.services.TphHomePageService;

/**
 * @author majidkhan
 *
 */
public class TestWishList {
public static void main(String[] args) {
		
	BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
	SellerAddNewPostRequestModel sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
	
	sellerAddNewPostRequestModel.setUserId(2);
	sellerAddNewPostRequestModel.setProductCode("TPHIR00000002");
	sellerAddNewPostRequestModel.setProductId(2);
	sellerBaseResponseModel = new TphHomePageService().wishList(sellerAddNewPostRequestModel, sellerBaseResponseModel);
	
	System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}
