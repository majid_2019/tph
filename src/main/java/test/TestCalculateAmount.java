package test;

import com.google.gson.Gson;
import com.tph.request.model.InventoryRequest;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.PaymentService;

public class TestCalculateAmount {
	public static void main(String[] args) {
		BaseResponseModel baseRes = new BaseResponseModel();
		InventoryRequest request = new InventoryRequest();
		request.setUserId(3);
		request.setChargeAmount(1000.0);
		request.setAmount(100000.0);
		request.setPercentage(0.2);
		request.setIgst(18);
		request.setCgst(9);
		request.setSgst(9);
		request.setInventoryId(1);
		
		baseRes = new PaymentService().calculateAmount(new Gson().fromJson("{\"userId\":1156,\"inventoryId\":\"1\",\"percentage\":\"0.2\",\"igst\":\"18\",\"cgst\":\"9\",\"sgst\":\"9\"}", InventoryRequest.class), baseRes);
		
		System.out.println("Res -- "+new Gson().toJson(baseRes));
	}
}
