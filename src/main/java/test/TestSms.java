package test;

import com.google.gson.Gson;
import com.tph.request.model.SmsRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SmsService;

public class TestSms {

	public static void main(String[] args) {
		SmsRequestModel requestModel = new SmsRequestModel();
		
		// OTP SMS 
		requestModel.setMobileNumber(8109536446L);
		requestModel.setServiceType("OTP SMS");
		requestModel.setUserId(1);
		requestModel.setCountryId(1);
		requestModel.setIsSeller(false);
		requestModel.setIsBuyer(true);
		
		// Contact Seller SMS
				/*requestModel.setFromAddress("7709335910");
				requestModel.setMobileNumber(8109536446L);
				requestModel.setServiceType("Contact seller");
				requestModel.setBuyerUserID(298);
				requestModel.setSellerUserId(1);
				requestModel.setBuyerName("Mr.Rushikesh Bhandari");
				requestModel.setIsMockSellerSms("0");*/
		
		// Make an offer SMS
		/*requestModel.setFromAddress("7709335910");
		requestModel.setMobileNumber(8109536446L);
		requestModel.setServiceType("Make an offer");
		requestModel.setBuyerUserID(298);
		requestModel.setSellerUserId(1);
		requestModel.setBuyerName("Mr.Rushikesh Bhandari");
		requestModel.setIsMockSellerSms("0");*/
		
		// Tacking ContactSeller SMS
		/*requestModel.setBuyerNumber("8109536446");
		requestModel.setBuyerName("Majid khan");
		requestModel.setProductCode("TPHIR1111111");
		requestModel.setServiceType("Contact Seller Tracking SMS");
		requestModel.setProductTitle("ABC");
		requestModel.setSellerName("Khan");
		requestModel.setSellerMoble("7709335910");*/
		
		// Tacking Make an offer SMS
		/*requestModel.setBuyerNumber("8109536446");
		requestModel.setBuyerName("Majid khan");
		requestModel.setProductCode("TPHIR1111111");
		requestModel.setServiceType("Make an offer Tracking SMS");
		requestModel.setProductTitle("ABC");
		requestModel.setSellerName("Khan");
		requestModel.setSellerMoble("7709335910");
		requestModel.setOfferPrice("5000");
		requestModel.setQuantity("5");
		requestModel.setUom("Ltr");*/
		
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		sellerBaseResponseModel = new SmsService().sendSms(requestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}

