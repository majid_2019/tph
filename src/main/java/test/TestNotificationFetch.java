package test;

import com.google.gson.Gson;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.NotificationService;

public class TestNotificationFetch {

	public static void main(String[] args) {
		SellerBaseRequestModel requestModel = new SellerBaseRequestModel();
		
		requestModel.setUserId(1);
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		sellerBaseResponseModel = new NotificationService().fetchNotification(requestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}

