package test;

import com.google.gson.Gson;
import com.tph.request.model.PaymentDetail;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.PaymentService;

public class TestPayuHash {

	public static void main(String[] args) {
		PaymentDetail paymentDetail = new PaymentDetail();
		
		paymentDetail.setTxnId("fdf232jmfdf3");
		paymentDetail.setProductInfo("Payment");
		paymentDetail.setAmount("1234");
		paymentDetail.setEmail("abc@gmailc.om");
		paymentDetail.setName("Majid Khan");
		paymentDetail.setMobile("7709335910");
		
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		sellerBaseResponseModel = new PaymentService().generateHash(paymentDetail, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}

