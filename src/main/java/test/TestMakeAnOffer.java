/**
 * 
 */
package test;

import com.google.gson.Gson;
import com.tph.jooq.tph_db.tables.records.MakeAnOfferRecord;
import com.tph.request.model.MakeAnOfferRquest;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphHomePageService;

/**
 * @author majidkhan
 *
 */
public class TestMakeAnOffer {
public static void main(String[] args) {
		
	BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
	MakeAnOfferRquest makeAnOfferRquest = new MakeAnOfferRquest();
	
	makeAnOfferRquest.setProductId(1);
	makeAnOfferRquest.setSellerId(2);
	makeAnOfferRquest.setBuyerId(2);
	makeAnOfferRquest.setAvailableQuantity(234.5);
	makeAnOfferRquest.setRequiredQuantity(10.2);
	makeAnOfferRquest.setOfferPrice(344.4);
	
	//sellerBaseResponseModel = new TphHomePageDao().makeAnOffer(makeAnOfferRquest, sellerBaseResponseModel);
	
	System.out.println("Response - "+new Gson().toJson(makeAnOfferRquest));
	}
}
