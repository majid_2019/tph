package test;

import com.google.gson.Gson;
import com.tph.request.model.RegistraionRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphLoginRegistraionService;

public class TestRegistraion {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		RegistraionRequestModel registraionRequestModel = new RegistraionRequestModel();
		registraionRequestModel.setCompanyName("Es");
		registraionRequestModel.setContactPerson("Mk");
		registraionRequestModel.setGstNumber("XYZ");
		registraionRequestModel.setMobileNumber("8109536446");
		registraionRequestModel.setEmail("abc.com");
		registraionRequestModel.setAddress("Pune");
		registraionRequestModel.setCountryId(1);
		registraionRequestModel.setStateId(3);
		registraionRequestModel.setCityId(14);
		registraionRequestModel.setPassword("222");
		registraionRequestModel.setUserName("Majid");
		registraionRequestModel.setIpAddress("202.23.23.4");
		//registraionRequestModel.setIsUpdate(true);
		registraionRequestModel.setIsSeller(true);
		registraionRequestModel.setUserId(440);
		
		sellerBaseResponseModel = new TphLoginRegistraionService().registraion(registraionRequestModel, sellerBaseResponseModel);
		
		System.out.println("Res -- "+new Gson().toJson(sellerBaseResponseModel));
	}
}
