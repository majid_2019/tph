/**
 * 
 */
package test;

import com.google.gson.Gson;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphHomePageService;

/**
 * @author majidkhan
 *
 */
public class TestStockClearanceList {
public static void main(String[] args) {
		
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		sellerBaseResponseModel = new TphHomePageService().stockClearanceLeads(sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}
