package test;

import com.google.gson.Gson;
import com.tph.request.model.SmsRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SmsService;

public class TestSeeDetailSms {

	public static void main(String[] args) {
		SmsRequestModel requestModel = new SmsRequestModel();
		requestModel = new Gson().fromJson("{\n" + 
				"    \"mobileNumber\": \"8109536446\",\n" + 
				"    \"serviceType\": \"Contact seller\",\n" + 
				"    \"buyerName\": \"rushikesh bhandari\",\n" + 
				"    \"fromAddress\": \"8109536446\",\n" + 
				"    \"buyerUserID\": 481,\n" + 
				"    \"sellerUserID\": 53,\n" + 
				"    \"isMockSellerSms\": 1\n" + 
				"}", SmsRequestModel.class);
		/*
		requestModel.setMobileNumber(81095364461L);
		requestModel.setServiceType("OTP");
		//requestModel.setUserId(1);
		requestModel.setCountryId(1);
//		requestModel.setIsSeller(true);
		requestModel.setIsBuyer(true);*/
		
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		sellerBaseResponseModel = new SmsService().sendSms(requestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}

