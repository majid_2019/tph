package test;

import com.google.gson.Gson;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphHomePageService;

public class TestHomePage {
	public static void main(String[] args) {
		
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		sellerBaseResponseModel = new TphHomePageService().fetchHomePageData(sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}

