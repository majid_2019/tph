package test;

import com.google.gson.Gson;
import com.tph.constants.ConstantKeys;
import com.tph.request.model.CommonRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphHomePageService;

public class TestDashProductDetails {

	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		CommonRequestModel baseRequestModel = new CommonRequestModel();
		
		//baseRequestModel.setProductId(210);
		//baseRequestModel.setProductName("CNC Turning Machine");
		//baseRequestModel.setSubCategoryId(11);
		baseRequestModel.setServiceType(ConstantKeys.INVENTORY);
		
		sellerBaseResponseModel = new TphHomePageService().dashProductDetails(baseRequestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}	
}
