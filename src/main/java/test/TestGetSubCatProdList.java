package test;

import com.google.gson.Gson;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphHomePageService;

public class TestGetSubCatProdList {
	public static void main(String[] args) {

		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		SellerBaseRequestModel baseRequestModel = new SellerBaseRequestModel();
		baseRequestModel.setSubCatTypeId(1);
		baseRequestModel.setCatTypeId(1);
		baseRequestModel.setIndustrialTypeId(1);
		
		try {
			sellerBaseResponseModel = new TphHomePageService().fetchSubCatProdList(baseRequestModel, sellerBaseResponseModel);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Response - " + new Gson().toJson(sellerBaseResponseModel));
	}
}
