/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphCommonService;

/**
 * @author majidkhan
 *
 */
public class TestIndustriesList {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		SellerBaseRequestModel dashboardRequestModel = new SellerBaseRequestModel();
		dashboardRequestModel.setIndustrialTypeId(1);;

		sellerBaseResponseModel = new TphCommonService().fetchIndustriesMaster(dashboardRequestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));

	}
}
