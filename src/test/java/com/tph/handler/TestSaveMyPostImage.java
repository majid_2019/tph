/**
 * 
 */
package com.tph.handler;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.tph.services.SellerDashboardService;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.PhotoVideoPath;

/**
 * @author majidkhan
 *
 */
public class TestSaveMyPostImage {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		SellerAddNewPostRequestModel sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
		PhotoVideoPath photoPath = new PhotoVideoPath();
		PhotoVideoPath videoPath = new PhotoVideoPath();
		List<PhotoVideoPath> phEs =  new ArrayList<PhotoVideoPath>(); 
		
		sellerAddNewPostRequestModel.setProductId(5);
		sellerAddNewPostRequestModel.setUserId(298);
		sellerAddNewPostRequestModel.setProductCode("TPHSC000000005");
		
		photoPath.setItemId(1);
		photoPath.setPath("2/20190727/abc.jpg");
		photoPath.setFileName("abc.jpg");
		
		videoPath.setItemId(2);
		videoPath.setPath("2/20190727/xyz.mp4");
		videoPath.setFileName("xyz.mp4");
		
		phEs.add(photoPath);
		phEs.add(videoPath);
		
		sellerAddNewPostRequestModel.setPhotoVideoPathList(phEs);

		sellerBaseResponseModel = new SellerDashboardService().saveNewPostImage(sellerAddNewPostRequestModel, sellerBaseResponseModel);
		
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));

	}

}
