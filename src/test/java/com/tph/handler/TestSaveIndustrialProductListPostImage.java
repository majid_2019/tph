/**
 * 
 */
package com.tph.handler;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.PhotoVideoPath;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestSaveIndustrialProductListPostImage {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		SellerAddNewPostRequestModel sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
		PhotoVideoPath photoPath = new PhotoVideoPath();
		PhotoVideoPath photoPath2 = new PhotoVideoPath();
		//PhotoVideoPath photoPath = new PhotoVideoPath();
		List<PhotoVideoPath> phEs =  new ArrayList<PhotoVideoPath>(); 
		
		sellerAddNewPostRequestModel.setUserId(1);
		sellerAddNewPostRequestModel.setProductCode("TPHSP000000002");
		
		photoPath.setPath("2/20190727/abc.jpg");
		photoPath2.setPath("2/2019072722/abc.jpg");
		
		phEs.add(photoPath);
		phEs.add(photoPath2);
		
		
		
		sellerAddNewPostRequestModel.setPhotoVideoPathList(phEs);

		//sellerBaseResponseModel = new SellerDashboardDataDao().saveIndustrialProductServiceImage(sellerAddNewPostRequestModel, sellerBaseResponseModel);
		
		System.out.println("Response - "+new Gson().toJson(sellerAddNewPostRequestModel));

	}

}
