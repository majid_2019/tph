package com.tph.handler;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.tph.constants.ConstantKeys;
import com.tph.constants.PostAction;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.PhotoVideoPath;
import com.tph.services.EditPostService;

public class TestEditPost {
	public static void main(String[] args) {
		SellerAddNewPostRequestModel request = new SellerAddNewPostRequestModel();
		BaseResponseModel response = new BaseResponseModel();
		
		request.setId(2128);
		request.setActionType(PostAction.SOLD_POST.toString());
		request.setServiceType(ConstantKeys.STOCK_CLEARANCE);
		
		// Update Post
		
		request.setProductTitle("ACDEFDDDD");
		request.setProductDetails("AAAABBBDD");
		PhotoVideoPath photoPath = new PhotoVideoPath();
		PhotoVideoPath videoPath = new PhotoVideoPath();
		
		List<PhotoVideoPath> phEs =  new ArrayList<PhotoVideoPath>(); 
		
		photoPath.setItemId(2);
		photoPath.setPath("2/20190727/abc.jpg");
		photoPath.setFileName("abc.jpg");
		
		videoPath.setItemId(2);
		videoPath.setPath("2/20190727/xyz.mp4");
		videoPath.setFileName("xyz.mp4");
		
		phEs.add(photoPath);
		phEs.add(videoPath);
		request.setPhotoVideoPathList(phEs);
		
		response = new EditPostService().postAction(request);
		
		System.out.println("request--"+new Gson().toJson(request));
		System.out.println(new Gson().toJson(response));
	}
}
