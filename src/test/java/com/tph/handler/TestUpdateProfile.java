/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.request.model.EditProfilleRequest;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestUpdateProfile {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		//ProfileViewResponseModel sellerAddNewPostRequestModel = new ProfileViewResponseModel();
		EditProfilleRequest editProfilleRequest = new EditProfilleRequest();
		
		editProfilleRequest.setUserId(1);
		editProfilleRequest.setFirstName("Majid");
		editProfilleRequest.setLastName("Khan");
		editProfilleRequest.setEmailId("mazidkhan008@gmail.com");
		editProfilleRequest.setMobileNumber("8109536446");
		editProfilleRequest.setGstNumber("11111");
		
		sellerBaseResponseModel = new SellerDashboardService().updateProfileData(editProfilleRequest, sellerBaseResponseModel);
		
		System.out.println("Request - "+new Gson().toJson(sellerBaseResponseModel));

	}

}
