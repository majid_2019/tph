/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphCommonService;

/**
 * @author majidkhan
 *
 */
public class TestFetchUom {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		sellerBaseResponseModel = new TphCommonService().fetchUom(sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));

	}
}
