/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestSaveIndustrialProdService {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		SellerAddNewPostRequestModel sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
		
		sellerAddNewPostRequestModel.setUserId(1);
		sellerAddNewPostRequestModel.setServiceCatId(1);
		sellerAddNewPostRequestModel.setProductTitle("CNC Turning Machine");
		sellerAddNewPostRequestModel.setProductDetails("CNC Turning Machine, Model No. DX 200 3A, Chuck Dia 200mm, Between Centre 300mm");
		sellerAddNewPostRequestModel.setExpInMonth(36);
		sellerAddNewPostRequestModel.setExpInYear(3);
		sellerAddNewPostRequestModel.setCountryId(1);
		sellerAddNewPostRequestModel.setStateId(1);
		sellerAddNewPostRequestModel.setCityId(1);
		sellerAddNewPostRequestModel.setMobileNumber("8109536446");
		
		
		sellerBaseResponseModel = new SellerDashboardService().saveIndustrialProdService(sellerAddNewPostRequestModel, sellerBaseResponseModel);
		
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));

	}

}
