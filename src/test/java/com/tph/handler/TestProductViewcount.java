package com.tph.handler;

import com.google.gson.Gson;
import com.tph.request.model.ProdViewCountRequest;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphCommonService;

public class TestProductViewcount {
	public static void main(String[] args) {
		ProdViewCountRequest request = new ProdViewCountRequest();
		
	//	request.setUserId(1);
	//	request.setProductId(1);
		request.setProductCode("TPHSP000000001");
		BaseResponseModel res = new BaseResponseModel();
		
		new TphCommonService().productViewcounter(request, res);
		System.out.println(new Gson().toJson(res));
	}
}
