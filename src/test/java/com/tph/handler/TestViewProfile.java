/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestViewProfile {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		//ProfileViewResponseModel sellerAddNewPostRequestModel = new ProfileViewResponseModel();
		SellerBaseRequestModel baseRequestModel = new SellerBaseRequestModel();
		
		baseRequestModel.setUserId(312);
		
		sellerBaseResponseModel = new SellerDashboardService().fetchProfileData(baseRequestModel, sellerBaseResponseModel);
		
		System.out.println("Response -- "+new Gson().toJson(sellerBaseResponseModel));

	}

}
