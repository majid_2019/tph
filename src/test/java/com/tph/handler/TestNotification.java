package com.tph.handler;

import com.google.gson.Gson;
import com.tph.constants.NotificationRequestType;
import com.tph.request.model.NotificationRequest;
import com.tph.services.NotificationService;

public class TestNotification {
public static void main(String[] args) {
	NotificationRequest request = new NotificationRequest();
	
	request.setBuyerId(1l);
	request.setBuyerMobile(8109536446l);
	request.setBuyerName("Majid Khan");
	request.setOfferPrice(555.0);
	request.setQuantity(12.0);
	request.setProductCode("TPHIR000001707");
	request.setProductTitle("VMC");
	request.setUom("Nos");
	request.setSellerId(1l);
	request.setSellerMobile(7709335910l);
	
	request.setNotificationType(NotificationRequestType.MAKE_AN_OFFER.toString());
	//request.setNotificationType(NotificationRequestType.CONTACT_SELLER.toString());
	System.out.println("Request -- "+new Gson().toJson(request));
	
	new NotificationService().sendNotification(request);
}
}
