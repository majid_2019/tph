/**
 * 
 */
package com.tph.handler;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.PhotoVideoPath;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestIndustrialProductServicePostImage {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		SellerAddNewPostRequestModel sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
		PhotoVideoPath photoPath = new PhotoVideoPath();
		List<PhotoVideoPath> phEs =  new ArrayList<PhotoVideoPath>(); 
		
		sellerAddNewPostRequestModel.setUserId(4);
		sellerAddNewPostRequestModel.setProductCode("TPHIP000000004");
		
		photoPath.setPath("2/20190727/abc.jpg");
		
		phEs.add(photoPath);
		
		
		
		sellerAddNewPostRequestModel.setPhotoVideoPathList(phEs);

		sellerBaseResponseModel = new SellerDashboardService().saveIndustrialProductListImage(sellerAddNewPostRequestModel, sellerBaseResponseModel);
		
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));

	}

}
