/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestDashboardApi {

	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		SellerBaseRequestModel baseRequestModel = new SellerBaseRequestModel();
		
		baseRequestModel.setUserId(1);
		
		sellerBaseResponseModel = new SellerDashboardService().fetchDashboardData(baseRequestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
		
		
	}
}
