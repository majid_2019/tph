/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.constants.ConstantKeys;
import com.tph.request.model.SellerBaseRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestFetchActivePost {
	public static void main(String[] args) {
		
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();

		SellerBaseRequestModel dashboardRequestModel = new SellerBaseRequestModel();
		dashboardRequestModel.setUserId(467);
		dashboardRequestModel.setServiceType(ConstantKeys.INDUSTRIAL_PLOT);

		sellerBaseResponseModel = new SellerDashboardService().fetchActivePost(dashboardRequestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}
