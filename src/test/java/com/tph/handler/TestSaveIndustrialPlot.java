/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestSaveIndustrialPlot {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		SellerAddNewPostRequestModel sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
		
		sellerAddNewPostRequestModel.setUserId(13);
		sellerAddNewPostRequestModel.setCategoryName("Industrial Plots");
		sellerAddNewPostRequestModel.setIndustrialTypeId(1);
		sellerAddNewPostRequestModel.setProductTitle("CNC Turning Machine");
		sellerAddNewPostRequestModel.setProductDetails("CNC Turning Machine, Model No. DX 200 3A, Chuck Dia 200mm, Between Centre 300mm");
		sellerAddNewPostRequestModel.setAvailableQuantity(1);
		sellerAddNewPostRequestModel.setUomId(1);
		sellerAddNewPostRequestModel.setPricePerUnit(1000.00);
		sellerAddNewPostRequestModel.setPurchaseYear("2019");
		sellerAddNewPostRequestModel.setIpNegotiable("Yes");
		sellerAddNewPostRequestModel.setResellerName("sellerAddNewPostRequestModel");
		sellerAddNewPostRequestModel.setCountryId(1);
		sellerAddNewPostRequestModel.setStateId(1);
		sellerAddNewPostRequestModel.setCityId(1);
		sellerAddNewPostRequestModel.setProductLocation("Pune");
		sellerAddNewPostRequestModel.setContactPerson("Majid Khan");
		sellerAddNewPostRequestModel.setMobileNumber("8109536446");
		sellerAddNewPostRequestModel.setAlterMobileNumber("8109536446");
		sellerAddNewPostRequestModel.setRemark("Nothing");
		
		System.out.println("Request -- "+new Gson().toJson(sellerAddNewPostRequestModel));
		
		sellerBaseResponseModel = new SellerDashboardService().saveNewindustrialProductList(sellerAddNewPostRequestModel, sellerBaseResponseModel);
		
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));

	}

}
