package com.tph.handler;

import com.google.gson.Gson;
import com.tph.constants.Password;
import com.tph.request.model.PasswordRequest;

public class TestPassword {

	public static void main(String[] args) {
		PasswordRequest request = new PasswordRequest();
		
		request.setUserId(298);
		request.setCurrentPw("cdef");
		request.setNewPw("9960643675");
		request.setAction(String.valueOf(Password.FORGOT_PASSWORD));
		
		//BaseResponseModel res = new PasswordService().changePassword(request);
		System.out.println("-- "+new Gson().toJson(request));
	}
}
