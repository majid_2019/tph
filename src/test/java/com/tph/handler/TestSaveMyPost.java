/**
 * 
 */
package com.tph.handler;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.tph.constants.ConstantKeys;
import com.tph.request.model.SellerAddNewPostRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.response.model.PhotoVideoPath;
import com.tph.services.SellerDashboardService;

/**
 * @author majidkhan
 *
 */
public class TestSaveMyPost {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		SellerAddNewPostRequestModel sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
		List<PhotoVideoPath> phEs =  new ArrayList<PhotoVideoPath>(); 
		
		sellerAddNewPostRequestModel.setUserId(1);
		sellerAddNewPostRequestModel.setIndustrialTypeId(101);
		sellerAddNewPostRequestModel.setCatTypeId(101);
		sellerAddNewPostRequestModel.setSubCatTypeId(0);
		sellerAddNewPostRequestModel.setProductTitle("CNC Turning Machine");
		sellerAddNewPostRequestModel.setProductDetails("CNC Turning Machine, Model No. DX 200 3A, Chuck Dia 200mm, Between Centre 300mm");
		sellerAddNewPostRequestModel.setPurchaseYear("2019");
		sellerAddNewPostRequestModel.setUomId(1);
		sellerAddNewPostRequestModel.setUomAcronym("Ltr");
		sellerAddNewPostRequestModel.setBrand("LG");
		sellerAddNewPostRequestModel.setAvailableQuantity(500);
		sellerAddNewPostRequestModel.setStateId(1);
		sellerAddNewPostRequestModel.setPricePerUnit(4000.00);
		sellerAddNewPostRequestModel.setPriceType(1);
		sellerAddNewPostRequestModel.setProductLocation("Pune");
		sellerAddNewPostRequestModel.setCityId(1);
		sellerAddNewPostRequestModel.setProductStatusId(1);
		
		sellerAddNewPostRequestModel.setPhotoVideoPathList(phEs);

		sellerBaseResponseModel = new SellerDashboardService().saveNewPost(sellerAddNewPostRequestModel, sellerBaseResponseModel);
		
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));

	}

}
