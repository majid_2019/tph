/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.request.model.SellerDashboardRequestModel;
import com.tph.response.model.BaseResponseModel;
import com.tph.services.TphCommonService;

/**
 * @author majidkhan
 *
 */
public class TestFetchState {
	public static void main(String[] args) {
		BaseResponseModel sellerBaseResponseModel = new BaseResponseModel();
		
		SellerDashboardRequestModel dashboardRequestModel = new SellerDashboardRequestModel();
		dashboardRequestModel.setCountryId(1);;
		
		sellerBaseResponseModel = new TphCommonService().fetchState(dashboardRequestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));

	}
}
